fn main() {
	let bindings = bindgen::Builder::default();
	#[cfg(windows)]
	let bindings = bindings.clang_arg("-IC:/VulkanSDK/1.2.135.0/Include");
	let bindings = bindings.clang_arg("-v");
	let bindings = bindings.header("src/vma.h")
		.parse_callbacks(Box::new(bindgen::CargoCallbacks))
		.generate()
		.expect("Unable to generate bindings");

	bindings.write_to_file("src/bindings.rs")
		.expect("Couldn't write bindings!");
		
	let mut build = cc::Build::new();
	#[cfg(windows)]
	build.include("C:/VulkanSDK/1.2.135.0/Include");
	build.file("src/vma.cpp")
		.static_flag(true)
		.cpp(true)
		.compile("vma_sys");
}
