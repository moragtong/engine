pub trait ConstDefault {
	const DEFAULT: Self;
}

macro_rules! array_impls {
	($($N:expr)+) => {
		$(
			impl<T: ConstDefault + Copy> ConstDefault for [T; $N] {
				const DEFAULT: Self = [ConstDefault::DEFAULT; $N];
			}
		)+
	}
}

array_impls! {
	 0  1  2  3  4  5  6  7  8  9
	10 11 12 13 14 15 16 17 18 19
	20 21 22 23 24 25 26 27 28 29
	30 31 32 33 34 35 36 37 38 39
	40 41 42 43 44 45 46 47 48 49
	50 51 52 53 54 55 56 57 58 59
	60 61 62 63 64 256
}

impl ConstDefault for u8 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for u16 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for u32 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for u64 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for i8 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for i16 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for i32 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for i64 {
	const DEFAULT: Self = 0;
}

impl ConstDefault for usize {
	const DEFAULT: Self = 0;
}

impl ConstDefault for isize {
	const DEFAULT: Self = 0;
}

impl ConstDefault for f32 {
	const DEFAULT: Self = 0.;
}

impl ConstDefault for f64 {
	const DEFAULT: Self = 0.;
}

impl<T> ConstDefault for Option<T> {
	const DEFAULT: Self = Option::None;
}