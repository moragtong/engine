pub struct Move {
	pub eye_delta:	nalgebra::Vector3<f32>,
	delta:			nalgebra::Vector2<f64>,
}

impl Default for Move {
	fn default() -> Self {
		Self {
			eye_delta:	Default::default(),
			delta:		Default::default(),
		}
	}
}

impl Move {
	pub fn cursor(&mut self, delta: [f64; 2]) {
		self.delta -= nalgebra::Vector2::from(delta);
	}

	pub fn flat_delta(&self, time_delta: std::time::Duration) -> f64 {
		(self.delta.x + self.delta.y) * time_delta.as_secs_f64()
	}

	pub fn delta(&self) -> nalgebra::Vector2<f64> {
		self.delta
	}
}
