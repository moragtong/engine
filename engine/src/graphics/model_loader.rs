use super::*;
use async_std::io::prelude::ReadExt;

#[repr(C)]
struct Header {
	vert_size:		u64,
	radius:			f32,
}

#[derive(Debug)]
pub struct ModelLoader {
	pub file:			async_std::fs::File,
	pub radius:			f32,
	pub size:			crate::vk::DeviceSize,
	pub vertices_size:	crate::vk::DeviceSize,
	pub vertex_count:	u32,
	pub index_size:		crate::vk::DeviceSize,
	pub index_count:	u32,
}

impl ModelLoader {
	pub async fn new(name: impl std::convert::AsRef<async_std::path::Path>) -> std::io::Result<Self> {
		let mut file = async_std::fs::File::open(name).await?;
		let mut header = std::mem::MaybeUninit::<Header>::uninit();
		let hs = unsafe { std::slice::from_raw_parts_mut(header.as_mut_ptr() as _, std::mem::size_of::<Header>()) };
		file.read_exact(hs).await?;
		let header = unsafe { header.assume_init() };

		let size = file.metadata().await?.len() - std::mem::size_of::<Header>() as crate::vk::DeviceSize;
		let index_size = size - header.vert_size;
		
		Ok(Self {
			radius:			header.radius,
			vertices_size:	header.vert_size,
			index_count:	(index_size / std::mem::size_of::<u32>() as crate::vk::DeviceSize) as u32,
			vertex_count:	(header.vert_size / std::mem::size_of::<per_vertex::PerVertex>() as crate::vk::DeviceSize) as u32,
			index_size, size, file
		})
	}
}
