pub fn align_offset(offset: crate::vk::DeviceSize, align: crate::vk::DeviceSize) -> crate::vk::DeviceSize {
	(offset & !(align - 1)) + if offset & (align - 1) == 0 { 0 } else { align }
}

/*pub fn align_size(size: crate::vk::DeviceSize, align: crate::vk::DeviceSize) -> crate::vk::DeviceSize {
	size & !(align - 1)
}*/

#[derive(Debug, Default, Clone, Copy)]
#[repr(C, align(16))]
pub struct Vec3<S>(pub [S; 3]);

impl<S: 'static + std::fmt::Debug + std::marker::Copy + num_traits::Num> From<[S; 3]> for Vec3<S> {
	fn from(obj: [S; 3]) -> Self {
		Vec3(obj)
	}
}

impl<S: nalgebra::RealField> From<nalgebra::Vector3<S>> for Vec3<S> {
	fn from(obj: nalgebra::Vector3<S>) -> Self {
		Vec3(obj.into())
	}
}

#[derive(Debug, Default, Clone, Copy)]
#[repr(C, align(16))]
pub struct Vec4<S>(pub [S; 4]);

impl<S: 'static + std::fmt::Debug + std::marker::Copy + num_traits::Num> From<[S; 4]> for Vec4<S> {
	fn from(obj: [S; 4]) -> Self {
		Vec4(obj)
	}
}

impl<S: nalgebra::RealField> From<nalgebra::Vector4<S>> for Vec4<S> {
	fn from(obj: nalgebra::Vector4<S>) -> Self {
		Vec4(obj.into())
	}
}

pub trait BincodeSerialize {
	fn serialize_into(&self, _: impl std::io::Write) -> bincode::Result<()>;
}

impl<T: BincodeSerialize> BincodeSerialize for Vec<T> {
	fn serialize_into(&self, mut ret: impl std::io::Write) -> bincode::Result<()> {
		bincode::serialize_into(&mut ret, &(self.len() as u64))?;
		for elem in self {
			elem.serialize_into(&mut ret)?;
		}
		Ok(())
	}
}

pub trait BincodeDeserialize: Sized {
	fn deserialize_from(_: impl std::io::Write + std::io::Read) -> bincode::Result<Self>;
}

impl<T: BincodeDeserialize> BincodeDeserialize for Vec<T> {
	fn deserialize_from(mut input: impl std::io::Write + std::io::Read) -> bincode::Result<Self> {
		let mut ret = Self::with_capacity(bincode::deserialize_from::<_, u64>(&mut input)? as _);
		for _ in 0..ret.capacity() {
			ret.push(BincodeDeserialize::deserialize_from(&mut input)?);
		}
		Ok(ret)
	}
} 
