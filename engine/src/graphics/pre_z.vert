#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in mat4 view_model;
layout(location = 4) in mat4 proj_view_model;

layout(location = 8) in vec3 position;

layout(location = 0) out vec3 out_position;

void main() {
	out_position = vec3(view_model * vec4(position, 1.0));
	gl_Position = proj_view_model * vec4(position, 1.0);
}