#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform UniformData {
	vec4	light;
} uniform_data;

layout(binding = 0) uniform sampler2DArray image;

layout(location = 0) in vec2 uv;

layout(location = 0) out vec4 out_color;

void main() {
	float depth = texture(image, vec3(uv, uniform_data.light.w)).r;
	out_color = vec4(depth, depth, depth, 1.0);
}