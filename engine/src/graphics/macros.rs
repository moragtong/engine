#[macro_export]
macro_rules! vk_try {
	($vk_func_call:expr) => {
		{
			let res = { $vk_func_call };
			if res != crate::vk::Result::SUCCESS {
				//println!("{}, {}", file!(), line!());
				std::panic!("{:?}", res);
				//Err(res)?
			}
		}
	}
} 
