use super::*;

#[derive(Default)]
pub struct UploadInfo {
	pub barriers:	Vec<crate::vk::ImageMemoryBarrier>,
}

unsafe impl Send for UploadInfo { }

pub struct UploadRange<T> {
	pub offset:	crate::vk::DeviceSize,
	pub data:	*mut T, // mapped memory, safe to pass between threads
	pub size:	usize,
}

pub struct UploadSource<T> {
	pub offset:	crate::vk::DeviceSize,
	pub size:	crate::vk::DeviceSize,
	marker:		std::marker::PhantomData<T>,
}

unsafe impl<T> Sync for UploadRange<T> { }
unsafe impl<T> Send for UploadRange<T> { }

impl<T> UploadRange<T> {
	/*pub fn start(&self) -> crate::vk::DeviceSize {
		self.offset
	}

	pub fn data(&mut self) -> &mut [T] {
		unsafe { std::slice::from_raw_parts_mut(self.data, self.size) }

	}*/

	pub fn raw_data(&mut self) -> &mut [u8] {
		unsafe { std::slice::from_raw_parts_mut(self.data as _, self.size * std::mem::size_of::<T>()) }
	}

	pub fn source(&self) -> UploadSource<T> {
		UploadSource {
			offset:	self.offset,
			size:	self.size as _,
			marker:	Default::default(),
		}
	}
}

#[derive(Default)]
pub struct Upload {
	allocated:				crate::vk::DeviceSize,
	buffer_image_copies:	std::collections::HashMap<crate::vk::Image, Vec<crate::vk::BufferImageCopy>>,
	barriers:				Vec<crate::vk::ImageMemoryBarrier>,
	buffer_copies:			std::collections::HashMap<crate::vk::Buffer, Vec<crate::vk::BufferCopy>>,
}

unsafe impl Send for Upload { }

/*crate::vk::MappedMemoryRange {
				memory:	self.allocation_info.deviceMemory,
				offset:	util::align_offset(self.allocation_info.offset + offset, self.device.non_coherent_atom_size),
				size:	util::align_size(size, self.device.non_coherent_atom_size),
				..ConstDefault::DEFAULT
			}*/

impl Upload {
	pub fn upload_buffer<T>(&mut self, src: &UploadSource<T>, buffer: &device::buffer::Buffer<T>, subbuffer: &device::buffer::SubBuffer<T>) {
		self.buffer_copies.entry(buffer.buffer).or_default().push(crate::vk::BufferCopy {
			src_offset:	src.offset,
			dst_offset:	subbuffer.bytes_range().start,
			size:		subbuffer.bytes_len(),
		})
	}

	pub fn upload_texture(&mut self, upload_info: TextureUploadInfo, src: &UploadSource<u8>, texture: &device::TextureAllocation) {
		let buffer_image_copies = self.buffer_image_copies.entry(texture.image).or_default();
		buffer_image_copies.reserve(upload_info.level_count as _);
		let mut buffer_offset = src.offset;
		let mut width = upload_info.create_info.extent.width;
		let mut height = upload_info.create_info.extent.height;
		buffer_image_copies.extend((0..upload_info.level_count as _).map(move |mip_level| {
			let ret = crate::vk::BufferImageCopy {
				image_extent:		crate::vk::Extent3D {
					width,
					height,
					depth:	1
				},
				image_subresource:	crate::vk::ImageSubresourceLayers {
					aspect_mask:		crate::vk::ImageAspectFlags::COLOR,
					mip_level:			mip_level as _,
					base_array_layer:	texture.range.start,
					layer_count:		texture.range.end - texture.range.start,
				},
				buffer_offset, ..ConstDefault::DEFAULT
			};
			buffer_offset += width as crate::vk::DeviceSize * height as crate::vk::DeviceSize / 16 * upload_info.ratio;
			width = width.max(upload_info.ratio as u32) / 2;
			height = height.max(upload_info.ratio as u32) / 2;
			ret
		}));
		self.barriers.push(crate::vk::ImageMemoryBarrier {
			dst_access_mask:		crate::vk::AccessFlags::TRANSFER_WRITE,
			old_layout:				crate::vk::ImageLayout::UNDEFINED,
			new_layout:				crate::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
			src_queue_family_index:	crate::vk::QUEUE_FAMILY_IGNORED,
			dst_queue_family_index:	crate::vk::QUEUE_FAMILY_IGNORED,
			image:					texture.image,
			subresource_range:		crate::vk::ImageSubresourceRange {
				aspect_mask:		crate::vk::ImageAspectFlags::COLOR,
				level_count:		crate::vk::REMAINING_MIP_LEVELS,
				base_array_layer:	texture.range.start,
				layer_count:		texture.range.end - texture.range.start,
				..ConstDefault::DEFAULT
			},
			..ConstDefault::DEFAULT
		});
	}
}

pub struct Uploader {
	pub allocator:			device::Allocator,
	pub fence:				crate::vk::Fence,
	semaphore:				crate::vk::Semaphore,
	cmd_pool:				crate::vk::CommandPool,
	cmd_buffer:				crate::vk::CommandBuffer,
}

impl std::convert::TryFrom<device::Allocator> for Uploader {
	type Error = crate::vk::Result;
	fn try_from(allocator: device::Allocator) -> VkResult<Self> {
		unsafe {
		let mut fence = std::mem::MaybeUninit::uninit();
		vk_try!(allocator.device.procs.create_fence(allocator.device.device, &ConstDefault::DEFAULT, 0 as _, fence.as_mut_ptr()));

		let mut semaphore = std::mem::MaybeUninit::uninit();
		vk_try!(allocator.device.procs.create_semaphore(allocator.device.device, &ConstDefault::DEFAULT, 0 as _, semaphore.as_mut_ptr()));

		let mut cmd_pool = std::mem::MaybeUninit::uninit();
		vk_try!(allocator.device.procs.create_command_pool(allocator.device.device, &crate::vk::CommandPoolCreateInfo {
			flags:				crate::vk::CommandPoolCreateFlags::TRANSIENT | crate::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
			queue_family_index:	allocator.device.transfer_index,
			..ConstDefault::DEFAULT
		}, 0 as _, cmd_pool.as_mut_ptr()));

		let cmd_pool = cmd_pool.assume_init();

		let mut cmd_buffer = std::mem::MaybeUninit::uninit();
		vk_try!(allocator.device.procs.allocate_command_buffers(allocator.device.device, &crate::vk::CommandBufferAllocateInfo {
			command_pool:			cmd_pool,
			command_buffer_count:	1,
			..ConstDefault::DEFAULT
		}, cmd_buffer.as_mut_ptr()));
		Ok(Self {
			semaphore:	semaphore.assume_init(),
			fence:		fence.assume_init(),
			cmd_buffer:	cmd_buffer.assume_init(),
			allocator, cmd_pool })
		}
	}
}

impl Uploader {
	pub fn allocate_upload_range_bytes(&mut self, upload: &mut Upload, size: crate::vk::DeviceSize, align: crate::vk::DeviceSize) -> Result<UploadRange<u8>, (Upload, UploadRange<u8>)> {
		let offset = util::align_offset(upload.allocated, align);
		//let align_size = util::align_size(size, align);
		if offset + size > self.allocator.allocation_info.size {
			eprintln!("---UPLOAD BUFFER FULL---");
			upload.allocated = self.allocator.allocation_info.size;
			Err((Upload {
				allocated:	size,
				..Default::default()
			}, UploadRange {
				offset:	0,
				data:	self.allocator.allocation_info.pMappedData as *mut u8,
				size:	size as _,
			}))
		} else {
			eprintln!("---UPLOAD BUFFER ALLOCATION---");
			upload.allocated = offset + size;
			Ok(UploadRange { offset,
				data:	unsafe { (self.allocator.allocation_info.pMappedData as *mut u8).add(offset as _) },
				size:	size as _
			})
		}
	}

	/*pub fn allocate_upload_range<T>(&mut self, upload: &mut Upload, size: crate::vk::DeviceSize) -> Result<UploadRange<T>, (Upload, UploadRange<T>)> {
		match self.allocate_upload_range_bytes(upload, size * std::mem::size_of::<T>() as crate::vk::DeviceSize, std::mem::align_of::<T>() as _) {
			Ok(raw) => Ok(UploadRange {
				offset:	raw.offset,
				data:	raw.data as _,
				size:	size as _,
			}),
			Err((upload, raw)) => Err((upload, UploadRange {
				offset:	raw.offset,
				data:	raw.data as _,
				size:	size as _,
			}))
		}
	}*/

	pub async fn submit(&mut self, mut upload: Upload, semaphore: crate::vk::Semaphore) -> VkResult<UploadInfo> {
		#[cfg(debug_assertions)]
		{
			if upload.barriers.is_empty() & upload.buffer_copies.is_empty() & upload.buffer_image_copies.is_empty() {
				return Ok(Default::default())
			}
		}

		eprintln!("---UPLOAD BEGIN---");

		unsafe {
		vma::vmaFlushAllocation(self.allocator.allocator.allocator, self.allocator.allocation, 0, upload.allocated);
		vk_try!(self.allocator.device.procs.begin_command_buffer(self.cmd_buffer, &crate::vk::CommandBufferBeginInfo {
			flags:	crate::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
			..ConstDefault::DEFAULT
		}));

			self.allocator.device.procs.cmd_pipeline_barrier(self.cmd_buffer, crate::vk::PipelineStageFlags::TOP_OF_PIPE,
				crate::vk::PipelineStageFlags::TRANSFER,
				crate::vk::DependencyFlags::BY_REGION, 0, 0 as _, 0, 0 as _,
				upload.barriers.len() as _, upload.barriers.as_ptr());

			for (buffer, buffer_copies) in upload.buffer_copies.drain() {
				self.allocator.device.procs.cmd_copy_buffer(self.cmd_buffer, self.allocator.upload_buffer(),
					buffer, buffer_copies.len() as _, buffer_copies.as_ptr());
			}

			for (image, buffer_image_copies) in upload.buffer_image_copies.drain() {
				self.allocator.device.procs.cmd_copy_buffer_to_image(self.cmd_buffer, self.allocator.upload_buffer(), image,
					crate::vk::ImageLayout::TRANSFER_DST_OPTIMAL, buffer_image_copies.len() as _, buffer_image_copies.as_ptr());
			}

			for barrier in &mut *upload.barriers {
				barrier.src_access_mask =			crate::vk::AccessFlags::TRANSFER_WRITE;
				barrier.dst_access_mask =			crate::vk::AccessFlags::SHADER_READ;
				barrier.src_queue_family_index =	self.allocator.device.transfer_index;
				barrier.dst_queue_family_index =	self.allocator.device.graphics_index;
				barrier.old_layout =				crate::vk::ImageLayout::TRANSFER_DST_OPTIMAL;
				barrier.new_layout =				crate::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL;
			}

			self.allocator.device.procs.cmd_pipeline_barrier(self.cmd_buffer, crate::vk::PipelineStageFlags::TRANSFER,
				crate::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
				crate::vk::DependencyFlags::BY_REGION, 0, 0 as _, 0, 0 as _,
				upload.barriers.len() as _, upload.barriers.as_ptr());

		vk_try!(self.allocator.device.procs.end_command_buffer(self.cmd_buffer));

		vk_try!(self.allocator.device.procs.queue_submit(self.allocator.device.transfer_queue, 1, &crate::vk::SubmitInfo {
			command_buffer_count:	1,
			p_command_buffers:		&self.cmd_buffer,
			signal_semaphore_count:	if semaphore != ConstDefault::DEFAULT { 1 } else { 0 },
			p_signal_semaphores:	if semaphore != ConstDefault::DEFAULT { &semaphore } else { 0 as _ },
			..ConstDefault::DEFAULT
		}, self.fence));

		self.allocator.device.poll_fence(self.fence).await?;
		eprintln!("---UPLOAD END---");
		Ok(UploadInfo { barriers: upload.barriers })
		}
	}
}

impl Drop for Uploader {
	fn drop(&mut self) {
		unsafe {
			self.allocator.device.procs.destroy_command_pool(self.allocator.device.device, self.cmd_pool, 0 as _);
			self.allocator.device.procs.destroy_semaphore(self.allocator.device.device, self.semaphore, 0 as _);
			self.allocator.device.procs.destroy_fence(self.allocator.device.device, self.fence, 0 as _);
		}
	}
}
