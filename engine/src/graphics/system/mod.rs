mod submit;
use super::*;

use std::convert::TryFrom;
use std::iter::FromIterator;

#[derive(Debug)]
pub struct Component {
	pub 					map_object:			MapObject,
	pub(in crate::graphics) draw_cmd:			crate::vk::DrawIndexedIndirectCommand,
	pub(in crate::graphics) texture_layers:		util::Vec3<f32>,
	pub(in crate::graphics) descriptor_indices:	util::Vec3<u32>,
	pub 					radius:				f32,
}

pub type Index = typed_generational_arena::Index<Component>;

pub struct Frame {
	cmd_buffers:			[crate::vk::CommandBuffer; RENDER_PASS_COUNT as _],
	fence:					crate::vk::Fence,
	semaphores:				[crate::vk::Semaphore; (OPERATION_COUNT + 1) as _],
	renderer_frame:			RendererFrame,
	shadow_frame:			ShadowRendererFrame,
}

impl Frame {
	pub fn destroy(&mut self, allocator: &mut device::Allocator) {
		unsafe {
			allocator.device.procs.destroy_fence(allocator.device.device, self.fence, 0 as _);
			let separate_compute_graphics = usize::from(self.semaphores[0] == crate::vk::Semaphore::null());
			for semaphore in self.semaphores.get_unchecked(separate_compute_graphics..OPERATION_COUNT as usize + 1) {
					allocator.device.procs.destroy_semaphore(allocator.device.device, *semaphore, 0 as _);
			}
		}
		self.shadow_frame.destroy(&allocator.device);
		self.renderer_frame.destroy(allocator);
	}
}

pub struct System {
	pub device:				std::sync::Arc<device::Device>,
	resource_manager:		std::sync::Arc<async_std::sync::Mutex<ResourceManager>>,
	render_resources:		RenderResources,
	extent:					crate::vk::Extent2D,
	window:					winit::window::Window,
	surface:				crate::vk::SurfaceKHR,
	graphics_cmd_pool:		crate::vk::CommandPool,
	swapchain:				crate::vk::SwapchainKHR,
	renderer:				Renderer,
	shadow_renderer:		ShadowRenderer,
	current:				u8,
	frames:					[Frame; SWAPCHAIN_IMAGE_COUNT as _],
	per_instance_buffer:	Vec<per_vertex::PerInstance>,

	barriers:				Vec<crate::vk::ImageMemoryBarrier>,
	unloadeds:				[Vec<Component>; SWAPCHAIN_IMAGE_COUNT as _],

	pub arena:				typed_generational_arena::Arena<Component, usize, u64>,
}

impl System {
	pub fn new(event_loop: &winit::event_loop::EventLoop<EngineEvent>) -> Result<Self, EngineError> {
		unsafe {
			let window = winit::window::Window::new(event_loop).unwrap();
				//.ok().ok_or(crate::vk::Result::ERROR_SURFACE_LOST_KHR)?;
			let instance = instance::Instance::new(event_loop)?;
			let surface = instance.create_surface(&window)?;
			let device = std::sync::Arc::from(device::Device::new(instance, surface)?);

			let mut capabilities = std::mem::MaybeUninit::uninit();
			vk_try!(device.instance.surface_procs.get_physical_device_surface_capabilities_khr(device.physical, surface, capabilities.as_mut_ptr()));
			let capabilities = capabilities.assume_init();

			let extent = {
				let winit::dpi::PhysicalSize { width, height } = window.inner_size();

				if capabilities.current_extent.width == !0 {
					crate::vk::Extent2D {
						width:	std::cmp::max(capabilities.min_image_extent.width, std::cmp::min(capabilities.max_image_extent.width, width)),
						height:	std::cmp::max(capabilities.min_image_extent.height, std::cmp::min(capabilities.max_image_extent.height, height)),
					}
				} else {
					capabilities.current_extent
				}
			};

			let format = {
				let mut formats = std::mem::MaybeUninit::<[crate::vk::SurfaceFormatKHR; 64]>::uninit();

				let mut count = 64;
				
				vk_try!(device.instance.surface_procs.get_physical_device_surface_formats_khr(device.physical, surface, &mut count, formats.as_mut_ptr() as _));
				let formats = formats.assume_init();

				if count == 1 && formats[1].format == crate::vk::Format::UNDEFINED {
					crate::vk::SurfaceFormatKHR {
						format:			crate::vk::Format::B8G8R8A8_UNORM,
						color_space:	crate::vk::ColorSpaceKHR::SRGB_NONLINEAR,
					}
				} else {
					*formats.iter().find(|format|
						format.format == crate::vk::Format::B8G8R8A8_UNORM && format.color_space == crate::vk::ColorSpaceKHR::SRGB_NONLINEAR)
					.unwrap_or_else(|| formats.get_unchecked(0))
				}
			};

			let mut renderer =			Renderer::new(&device, extent, format.format,
				&[renderer::PostProcess {
					vertex:					PRE_Z_VERT,
					geometry:				Some(OUTLINE_GEOM),
					fragment:				Some(OUTLINE_FRAG),
					depth_stencil_state:	crate::vk::PipelineDepthStencilStateCreateInfo {
						depth_test_enable:	crate::vk::TRUE,
						depth_compare_op:	crate::vk::CompareOp::LESS_OR_EQUAL,
						..ConstDefault::DEFAULT
					},
				}]
			)?;

			let mut allocator =			device::BasicAllocator::try_from(device.clone())?.into();

			let shadow_renderer =	ShadowRenderer::new(&mut allocator, 1024)?;

			let fences = {
				let mut fences = std::mem::MaybeUninit::<[vk::Fence; SWAPCHAIN_IMAGE_COUNT as _]>::uninit();
				let mut fence_ptr = fences.as_mut_ptr() as *mut vk::Fence;

				for _ in 0..SWAPCHAIN_IMAGE_COUNT {
					vk_try!(device.procs.create_fence(device.device, &crate::vk::FenceCreateInfo {
						flags:	crate::vk::FenceCreateFlags::SIGNALED,
						..ConstDefault::DEFAULT
					}, 0 as _, fence_ptr));

					fence_ptr = fence_ptr.add(1);
				}

				fences.assume_init()
			};

			
			
			let mut resource_manager =	ResourceManager::new(Loader::new(Uploader::try_from(allocator)?), renderer.set_layout)?;
			let render_resources =		resource_manager.render_resources();

			let mut swapchain = std::mem::MaybeUninit::uninit();
			vk_try!(device.swapchain_procs.create_swapchain_khr(device.device, &crate::vk::SwapchainCreateInfoKHR {
				min_image_count:
					dbg!(if capabilities.max_image_count > 0 && capabilities.min_image_count == capabilities.max_image_count {
						capabilities.max_image_count
					} else {
						capabilities.min_image_count + 1
					}),
				image_format:		format.format,
				image_color_space:	format.color_space,
				image_extent:		extent,
				image_usage:		crate::vk::ImageUsageFlags::COLOR_ATTACHMENT,
				image_array_layers:	1,
				image_sharing_mode:	crate::vk::SharingMode::EXCLUSIVE,
				pre_transform:		capabilities.current_transform,
				composite_alpha:	crate::vk::CompositeAlphaFlagsKHR::OPAQUE,
				present_mode:		crate::vk::PresentModeKHR::IMMEDIATE,
				clipped:			crate::vk::TRUE,
				surface, ..ConstDefault::DEFAULT
			}, 0 as _, swapchain.as_mut_ptr()));

			let swapchain = swapchain.assume_init();

			let images = {
				let mut images = std::mem::MaybeUninit::<[crate::vk::Image; SWAPCHAIN_IMAGE_COUNT as _]>::uninit();
				let mut count = 0;
				vk_try!(device.swapchain_procs.get_swapchain_images_khr(device.device, swapchain, &mut count, 0 as _));
				vk_try!(device.swapchain_procs.get_swapchain_images_khr(device.device, swapchain, &mut count, images.as_mut_ptr() as _));
				images.assume_init()
			};

			let mut graphics_cmd_pool = std::mem::MaybeUninit::uninit();
			vk_try!(device.procs.create_command_pool(device.device, &crate::vk::CommandPoolCreateInfo {
				flags:				crate::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
				queue_family_index:	device.graphics_index,
				..ConstDefault::DEFAULT
			}, 0 as _, graphics_cmd_pool.as_mut_ptr()));
			let graphics_cmd_pool = graphics_cmd_pool.assume_init();

			let mut cmd_buffers = std::mem::MaybeUninit::<[[_; RENDER_PASS_COUNT as _]; SWAPCHAIN_IMAGE_COUNT as _]>::uninit();
			vk_try!(device.procs.allocate_command_buffers(device.device, &crate::vk::CommandBufferAllocateInfo {
				command_pool:			graphics_cmd_pool,
				command_buffer_count:	(SWAPCHAIN_IMAGE_COUNT * RENDER_PASS_COUNT).into(),
				level:					crate::vk::CommandBufferLevel::PRIMARY,
				..ConstDefault::DEFAULT
			}, cmd_buffers.as_mut_ptr() as _));
			let cmd_buffers = cmd_buffers.assume_init();

			let mut frames = std::mem::MaybeUninit::<[Frame; SWAPCHAIN_IMAGE_COUNT as _]>::uninit();

			let mut frame_ptr = frames.as_mut_ptr() as *mut Frame;

			for (i, ((swapchain_image, cmd_buffers), fence)) in images.iter().zip(&cmd_buffers).zip(&fences).enumerate() {

				frame_ptr.write(Frame {
					semaphores:			array_init::from_iter((0..usize::from(OPERATION_COUNT + 1)).map(|_| {
						let mut semaphore = std::mem::MaybeUninit::uninit();
						vk_try!(device.procs.create_semaphore(device.device, &ConstDefault::DEFAULT, 0 as _, semaphore.as_mut_ptr()));
						semaphore.assume_init()
					})).unwrap(),
					renderer_frame:		renderer.create_frame(&mut resource_manager.loader.uploader.allocator, *swapchain_image, format.format)?,
					shadow_frame:		shadow_renderer.create_frame(&resource_manager.loader.uploader.allocator.device, i as _)?,
					cmd_buffers:		*cmd_buffers,
					fence:				*fence,
				});

				frame_ptr = frame_ptr.add(1);
			}

			Ok(Self {
				current:				ConstDefault::DEFAULT,
				per_instance_buffer:	Default::default(),
				resource_manager:		std::sync::Arc::from(async_std::sync::Mutex::from(resource_manager)),
				frames:					frames.assume_init(),
				arena:					typed_generational_arena::Arena::new(),
				barriers:				Default::default(),
				unloadeds:				Default::default(),
				render_resources,
				renderer, shadow_renderer,
				extent, swapchain, graphics_cmd_pool,
				window, surface, device,

			})
		}
	}

	pub async fn save(&self, filename: impl AsRef<std::path::Path> + std::convert::AsRef<async_std::path::Path> + std::marker::Send + 'static) -> Result<(), EngineError> {
		use async_std::io::prelude::WriteExt;

		let mut buffer = Vec::default();
		serde_json::to_writer(&mut buffer, &Vec::from_iter(self.arena.iter().map(|(_, object)| object.map_object.clone())))?;
		async_std::fs::File::create(filename).await?.write(&buffer).await?;

		Ok(())
	}

	pub fn load_payload(&mut self, payload: Payload) -> Vec<Index> {
		self.render_resources.descriptor_set = payload.descriptor_set;
		self.barriers.extend_from_slice(&payload.barriers);
		let mut objects = Vec::with_capacity(payload.components.len());
		
		for component in payload.components {
			objects.push(self.arena.insert(component));
		}

		objects
	}

	pub fn unload_object(&mut self, object: Index) {
		let current: usize = self.current.into();
		unsafe { self.unloadeds.get_unchecked_mut(current) }
			.push(self.arena.remove(object).unwrap())
	}

	pub fn new_camera(&self) -> Camera {
		Camera::new(self.extent)
	}

	pub fn window(&mut self) -> &mut winit::window::Window {
		&mut self.window
	}

	pub fn resource_manager(&self) -> &std::sync::Arc<async_std::sync::Mutex<ResourceManager>> {
		&self.resource_manager
	}

	pub fn wait_for_idle(&self) {
		unsafe {
			self.device.procs.device_wait_idle(self.device.device);
		}
	}
}

impl Drop for System {
	fn drop(&mut self) {
		unsafe {
			async_std::task::block_on(async {
				let mut guard = self.resource_manager.lock().await;
				for frame in &mut self.frames {
					frame.destroy(&mut guard.loader.uploader.allocator)
				}
				self.renderer.destroy(&mut guard.loader.uploader.allocator);
				self.shadow_renderer.destroy(&mut guard.loader.uploader.allocator);
			});
			self.device.procs.destroy_command_pool(self.device.device, self.graphics_cmd_pool, 0 as _);

			self.device.swapchain_procs.destroy_swapchain_khr(self.device.device, self.swapchain, 0 as _);

			self.device.instance.surface_procs.destroy_surface_khr(self.device.instance.instance, self.surface, 0 as _);
		}
	}
}
