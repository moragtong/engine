use super::*;

/*const BIAS: nalgebra::Matrix4<f32> = nalgebra::Matrix4 {
	x:	nalgebra::Vector4 { x: 0.5, y: 0.0, z: 0.0, w: 0.0 },
	y:	nalgebra::Vector4 { x: 0.0, y: 0.5, z: 0.0, w: 0.0 },
	z:	nalgebra::Vector4 { x: 0.0, y: 0.0, z: 1.0, w: 0.0 },
	w:	nalgebra::Vector4 { x: 0.5, y: 0.5, z: 0.0, w: 1.0 },
};*/

const CMD_BUFFER_BEGIN_INFO: crate::vk::CommandBufferBeginInfo = crate::vk::CommandBufferBeginInfo {
	flags:				crate::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
	..ConstDefault::DEFAULT
};

impl System {
	pub fn submit(&mut self,
		render_data:	&RenderData,
		ranges:			&[(crate::vk::DeviceSize, u32)]
	) -> Result<(), EngineError> {
		let shadow_matrix = render_data.shadow_matrix(&self.arena);
		let proj_view = render_data.camera.proj * render_data.camera.view;
		let view = render_data.camera.view;
		{
			let Self { per_instance_buffer, arena, .. } = self;
			per_instance_buffer.extend(arena.iter().enumerate().map(|(i, (_, object))| {
				let model = nalgebra::Matrix4::from(&object.map_object.transform);
				PerInstance {
					draw_cmd:			crate::vk::DrawIndexedIndirectCommand {
						first_instance:	i as _,
						instance_count:	1,
						..object.draw_cmd
					}.into(),
					texture_layers:		object.texture_layers,
					descriptor_indices:	object.descriptor_indices,
					view_model:			(view * model).into(),
					proj_view_model:	(proj_view * model).into(),
					light_model:		(shadow_matrix * model).into(),
				}
			}));
		}

		unsafe {

		let frame = &self.frames[usize::from(self.current)];

		vk_try!(self.device.procs.wait_for_fences(self.device.device, 1, &frame.fence, crate::vk::TRUE, !0));
		vk_try!(self.device.procs.reset_fences(self.device.device, 1, &frame.fence));

		let mut index = std::mem::MaybeUninit::uninit();

		vk_try!(self.device.swapchain_procs.acquire_next_image_khr(self.device.device, self.swapchain, !0,
			frame.semaphores[0], ConstDefault::DEFAULT, index.as_mut_ptr()));

		let index = index.assume_init();

		debug_assert!(index == u32::from(self.current));

		let per_instance_offset = crate::vk::DeviceSize::from(self.current) * std::mem::size_of::<PerInstance>() as crate::vk::DeviceSize * INSTANCE_COUNT as crate::vk::DeviceSize;
		vk_try!(self.device.procs.begin_command_buffer(frame.cmd_buffers[0], &CMD_BUFFER_BEGIN_INFO));
			if !self.arena.is_empty() {
				self.device.procs.cmd_update_buffer(frame.cmd_buffers[0], self.render_resources.per_instance_buffer, per_instance_offset,
					(self.per_instance_buffer.len() * std::mem::size_of::<per_vertex::PerInstance>()) as _, self.per_instance_buffer.as_ptr() as _);

				self.device.procs.cmd_pipeline_barrier(frame.cmd_buffers[0], crate::vk::PipelineStageFlags::TRANSFER,
					crate::vk::PipelineStageFlags::DRAW_INDIRECT | crate::vk::PipelineStageFlags::VERTEX_SHADER, crate::vk::DependencyFlags::BY_REGION,
					0, 0 as _, 1, &crate::vk::BufferMemoryBarrier {
						src_access_mask:		crate::vk::AccessFlags::TRANSFER_WRITE,
						dst_access_mask:		crate::vk::AccessFlags::INDIRECT_COMMAND_READ |
												crate::vk::AccessFlags::SHADER_READ,
						src_queue_family_index:	crate::vk::QUEUE_FAMILY_IGNORED,
						dst_queue_family_index:	crate::vk::QUEUE_FAMILY_IGNORED,
						buffer:					self.render_resources.per_instance_buffer,
						size:					std::mem::size_of::<PerInstance>() as crate::vk::DeviceSize * INSTANCE_COUNT as crate::vk::DeviceSize,
						offset:					per_instance_offset,
						..ConstDefault::DEFAULT
					}, 0, 0 as _);

			}

			self.shadow_renderer.render(&self.device, frame.cmd_buffers[0], &frame.shadow_frame, self.current.into(), &self.render_resources, self.arena.len());

		vk_try!(self.device.procs.end_command_buffer(frame.cmd_buffers[0]));
		
		vk_try!(self.device.procs.begin_command_buffer(frame.cmd_buffers[1], &CMD_BUFFER_BEGIN_INFO));
			if !self.barriers.is_empty() { 
				self.device.procs.cmd_pipeline_barrier(frame.cmd_buffers[1], crate::vk::PipelineStageFlags::TRANSFER,
					crate::vk::PipelineStageFlags::FRAGMENT_SHADER,
					crate::vk::DependencyFlags::BY_REGION, 0, 0 as _, 0, 0 as _,
					self.barriers.len() as _, self.barriers.as_ptr());

				self.barriers.clear();
			}
			self.renderer.render(&self.device, frame.cmd_buffers[1], &frame.renderer_frame, self.current.into(), render_data, &self.render_resources, self.arena.len(), ranges)?;

		vk_try!(self.device.procs.end_command_buffer(frame.cmd_buffers[1]));

		self.per_instance_buffer.clear();

		vk_try!(self.device.procs.queue_submit(self.device.graphics_queue, 1, &crate::vk::SubmitInfo {
			command_buffer_count:	1,
			p_command_buffers:		&frame.cmd_buffers[0],
			wait_semaphore_count:	1,
			p_wait_semaphores:		&frame.semaphores[0],
			p_wait_dst_stage_mask:	&crate::vk::PipelineStageFlags::TOP_OF_PIPE,
			signal_semaphore_count:	1,
			p_signal_semaphores:	&frame.semaphores[1],
			..ConstDefault::DEFAULT
		}, ConstDefault::DEFAULT));

		vk_try!(self.device.procs.queue_submit(self.device.graphics_queue, 1, &crate::vk::SubmitInfo {
			command_buffer_count:	1,
			p_command_buffers:		&frame.cmd_buffers[1],
			wait_semaphore_count:	1,
			p_wait_semaphores:		&frame.semaphores[1],
			p_wait_dst_stage_mask:	&crate::vk::PipelineStageFlags::TOP_OF_PIPE,
			signal_semaphore_count:	1,
			p_signal_semaphores:	&frame.semaphores[OPERATION_COUNT as usize],
			..ConstDefault::DEFAULT
		}, frame.fence));
		
		vk_try!(self.device.swapchain_procs.queue_present_khr(self.device.present_queue, &crate::vk::PresentInfoKHR {
			wait_semaphore_count:	1,
			p_wait_semaphores:		&frame.semaphores[OPERATION_COUNT as usize],
			swapchain_count:		1,
			p_swapchains:			&self.swapchain,
			p_image_indices:		&index,
			..ConstDefault::DEFAULT
		}));
		}

		self.current = (self.current + 1) % SWAPCHAIN_IMAGE_COUNT;

		//gc
		let resource_manager = self.resource_manager.clone();

		let unloaded = std::mem::take(&mut self.unloadeds[usize::from(self.current)]);

		if !unloaded.is_empty() {
			println!("{}", self.arena.len());
			async_std::task::spawn(async move {
				let mut guard = resource_manager.lock().await;
				for component in unloaded {
					guard.unload_object(component).unwrap()
				}
			});
		}

		Ok(())
	}
}
