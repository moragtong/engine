use super::*;
mod cull;
pub use self::cull::*;

use std::convert::{ TryFrom, TryInto };

#[repr(C, align(16))]
#[derive(Clone, Copy)]
pub struct CullPushConstant {
	pub proj_view:	[[f32; 4]; 4],
	pub view:		[[f32; 4]; 4],
	pub light:		[[f32; 4]; 4],
	pub max:		u32,
}

pub struct Cull {
	cull_comp:			crate::vk::ShaderModule,
	set_layout:			crate::vk::DescriptorSetLayout,
	pipeline_layout:	crate::vk::PipelineLayout,
	pipeline:			crate::vk::Pipeline,
	descriptor_pool:	crate::vk::DescriptorPool,
	descriptor_set:		crate::vk::DescriptorSet,
}

impl Cull {
	pub fn new(device: &device::Device, render_resources: &RenderResources) -> VkResult<Self> {
		let binding_count = if render_resources.cull_buffer == crate::vk::Buffer::null() {
			1
		} else {
			2
		};
		unsafe {
		let mut set_layout = std::mem::MaybeUninit::uninit();
		{
			const STORAGE_BINDINGS: [crate::vk::DescriptorSetLayoutBinding; 2] = [
				crate::vk::DescriptorSetLayoutBinding {
					binding:			0,
					descriptor_type:	crate::vk::DescriptorType::STORAGE_BUFFER_DYNAMIC,
					descriptor_count:	1,
					stage_flags:		crate::vk::ShaderStageFlags::COMPUTE,
					..ConstDefault::DEFAULT
				},
				crate::vk::DescriptorSetLayoutBinding {
					binding:			1,
					descriptor_type:	crate::vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
					descriptor_count:	1,
					stage_flags:		crate::vk::ShaderStageFlags::COMPUTE,
					..ConstDefault::DEFAULT
				},
			];


			vk_try!(device.procs.create_descriptor_set_layout(device.device, &crate::vk::DescriptorSetLayoutCreateInfo {
				p_bindings:		STORAGE_BINDINGS.as_ptr(),
				binding_count, ..ConstDefault::DEFAULT
			}, 0 as _, set_layout.as_mut_ptr()));
		}
		let set_layout = set_layout.assume_init();

		let mut pipeline_layout = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_pipeline_layout(device.device, &crate::vk::PipelineLayoutCreateInfo {
			set_layout_count:			1,
			p_set_layouts:				&set_layout,
			push_constant_range_count:	u32::from(render_resources.cull_buffer == crate::vk::Buffer::null()),
			p_push_constant_ranges:	if render_resources.cull_buffer == crate::vk::Buffer::null() {
				&crate::vk::PushConstantRange {
					stage_flags:	crate::vk::ShaderStageFlags::COMPUTE,
					offset:			0,
					size:			std::mem::size_of::<CullPushConstant>().try_into().unwrap(),
				}
			} else {
				0 as _
			},
			..ConstDefault::DEFAULT
		}, 0 as _, pipeline_layout.as_mut_ptr()));

		let pipeline_layout = pipeline_layout.assume_init();

		let cull_comp = device.create_shader_module(if render_resources.cull_buffer == crate::vk::Buffer::null() {
			CULL_COMP
		} else {
			CULL_COMP_NPS
		})?;

		let mut pipeline = std::mem::MaybeUninit::uninit();
		device.procs.create_compute_pipelines(device.device, ConstDefault::DEFAULT, 1, &crate::vk::ComputePipelineCreateInfo {
			stage:	crate::vk::PipelineShaderStageCreateInfo {
				stage:	crate::vk::ShaderStageFlags::COMPUTE,
				module:	cull_comp,
				p_name:	cstr!("main"),
				..ConstDefault::DEFAULT
			},
			layout:	pipeline_layout,
			..ConstDefault::DEFAULT
		}, 0 as _, pipeline.as_mut_ptr());

		let pipeline = pipeline.assume_init();

		const POOL_SIZES: [crate::vk::DescriptorPoolSize; 2] = [
			crate::vk::DescriptorPoolSize {
				ty:					crate::vk::DescriptorType::STORAGE_BUFFER_DYNAMIC,
				descriptor_count:	1,
			},
			crate::vk::DescriptorPoolSize {
				ty:					crate::vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
				descriptor_count:	1,
			},
		];


		let mut descriptor_pool = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_descriptor_pool(device.device, 
		&crate::vk::DescriptorPoolCreateInfo {
			max_sets:			1,
			pool_size_count:	binding_count,
			p_pool_sizes:		POOL_SIZES.as_ptr(),
			..ConstDefault::DEFAULT
		}, 0 as _, descriptor_pool.as_mut_ptr()));

		let descriptor_pool = descriptor_pool.assume_init();
		let mut descriptor_set = std::mem::MaybeUninit::<crate::vk::DescriptorSet>::uninit();
		vk_try!(device.procs.allocate_descriptor_sets(device.device, 
			&crate::vk::DescriptorSetAllocateInfo {
				descriptor_set_count:	1,
				p_set_layouts:			&set_layout,
				descriptor_pool, ..ConstDefault::DEFAULT
			}, descriptor_set.as_mut_ptr()));

		let descriptor_set = descriptor_set.assume_init();

		let buffer_infos = [
			crate::vk::DescriptorBufferInfo {
				buffer:	render_resources.per_instance_buffer,
				range:	vk::DeviceSize::try_from(std::mem::size_of::<PerInstance>()).unwrap() * vk::DeviceSize::from(INSTANCE_COUNT),
				..ConstDefault::DEFAULT
			},
			crate::vk::DescriptorBufferInfo {
				buffer:	render_resources.cull_buffer,
				range:	vk::DeviceSize::try_from(std::mem::size_of::<CullPushConstant>()).unwrap(),
				..ConstDefault::DEFAULT
			},
		];

		let write_descriptor_sets = [
			crate::vk::WriteDescriptorSet {
				dst_set:			descriptor_set,
				descriptor_count:	1,
				descriptor_type:	crate::vk::DescriptorType::STORAGE_BUFFER_DYNAMIC,
				p_buffer_info:		&buffer_infos[0],
				..ConstDefault::DEFAULT
			},
			crate::vk::WriteDescriptorSet {
				dst_set:			descriptor_set,
				dst_binding:		1,
				descriptor_count:	1,
				descriptor_type:	crate::vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
				p_buffer_info:		&buffer_infos[1],
				..ConstDefault::DEFAULT
			},
		];

		device.procs.update_descriptor_sets(device.device, binding_count, write_descriptor_sets.as_ptr(), 0, 0 as _);
		

		Ok(Self { cull_comp, set_layout, pipeline_layout, pipeline, descriptor_pool, descriptor_set })
		}
	}

	pub fn destroy(&mut self, device: &device::Device) {
		unsafe {
			device.procs.destroy_descriptor_pool(device.device, self.descriptor_pool, 0 as _);
			device.procs.destroy_pipeline(device.device, self.pipeline, 0 as _);
			device.procs.destroy_pipeline_layout(device.device, self.pipeline_layout, 0 as _);
			device.procs.destroy_descriptor_set_layout(device.device, self.set_layout, 0 as _);
			device.procs.destroy_shader_module(device.device, self.cull_comp, 0 as _);
		}
	}
}
