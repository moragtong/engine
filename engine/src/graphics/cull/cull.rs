use super::*;

use std::convert::{ TryFrom, TryInto };

impl Cull {	
	pub fn cull(&self,
		device:					&device::Device,
		cmd_buffer:				crate::vk::CommandBuffer,
		i:						usize,
		cull_push_constant:		Option<CullPushConstant>,
	) {
		unsafe {
			let count = if let Some(cull_push_constant) = cull_push_constant {
				device.procs.cmd_push_constants(cmd_buffer, self.pipeline_layout, crate::vk::ShaderStageFlags::COMPUTE,
					0, std::mem::size_of::<CullPushConstant>().try_into().unwrap(), &cull_push_constant as *const _ as _);
				1
			} else {
				2
			};

			let offsets = [
				u32::try_from(i * std::mem::size_of::<PerInstance>()).unwrap() * u32::from(INSTANCE_COUNT),
				(i * std::mem::size_of::<cull::CullPushConstant>()).try_into().unwrap(),
			];
			
			device.procs.cmd_bind_descriptor_sets(cmd_buffer, crate::vk::PipelineBindPoint::COMPUTE,
				self.pipeline_layout, 0, 1, &self.descriptor_set, count, offsets.as_ptr());

			device.procs.cmd_bind_pipeline(cmd_buffer, crate::vk::PipelineBindPoint::COMPUTE, self.pipeline);
			device.procs.cmd_dispatch(cmd_buffer, 1, 1, 1);
		}
	}
}