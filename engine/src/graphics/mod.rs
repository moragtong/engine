pub type VkResult<T> = Result<T, crate::vk::Result>;

static MAIN_VERT: &[u32] = include_glsl!("src/graphics/main.vert", target: vulkan1_1, optimize: zero);
static MAIN_FRAG: &[u32] = include_glsl!("src/graphics/main.frag", target: vulkan1_1, optimize: zero);

static QUAD_VERT: &[u32] = include_glsl!("src/graphics/quad.vert", target: vulkan1_1, optimize: zero);
static QUAD_FRAG: &[u32] = include_glsl!("src/graphics/quad.frag", target: vulkan1_1, optimize: zero);

static PRE_Z_VERT: &[u32] = include_glsl!("src/graphics/pre_z.vert", target: vulkan1_1, optimize: zero);
static OUTLINE_GEOM: &[u32] = include_glsl!("src/graphics/outline.geom", target: vulkan1_1, optimize: zero);
static OUTLINE_FRAG: &[u32] = include_glsl!("src/graphics/outline.frag", target: vulkan1_1, optimize: zero);

//static CULL_COMP: &[u32] = include_glsl!("src/graphics/cull.comp", target: vulkan1_1, optimize: zero);
//static CULL_COMP_NPS: &[u32] = include_glsl!("src/graphics/cull_nps.comp", target: vulkan1_1, optimize: zero);

const SWAPCHAIN_IMAGE_COUNT: u8 = 3;
const TEXTURE_COUNT: u8 = 3;
const ATTACHMENT_COUNT: u8 = 2;
const DEPTH_FORMAT: crate::vk::Format = crate::vk::Format::D24_UNORM_S8_UINT;
const SUBPASS_COUNT: u8 = 2;
const RENDER_PASS_COUNT: u8 = 2;
const OPERATION_COUNT: u8 = 2;


pub mod render_data;
mod instance;
mod surface;
mod device;
mod model_loader;
mod texture_loader;
mod renderer;
mod shadow_renderer;
//mod cull;
mod upload;
mod load;
mod per_vertex;
mod system;
mod resource_manager;
mod texture;
mod macros;
mod util;

use crate::map_editor::EngineError as EngineError;
use crate::map_editor::EngineEvent as EngineEvent;

use const_default::ConstDefault;
use self::shadow_renderer::*;
use self::renderer::*;
use self::upload::*;
use self::upload::UploadInfo;
use self::load::*;
pub use self::render_data::*;
pub use self::resource_manager::*;
pub use self::per_vertex::*;
pub use self::model_loader::*;
use self::texture::*;
pub use system::*;
use crate::{ vk_try, include_glsl };
