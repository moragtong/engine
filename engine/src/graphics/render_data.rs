use super::*;

const NEAR: f32 = 0.1;
const FAR: f32 = 100.;
const FOVY: f32 = std::f32::consts::PI / 2.;

/*const CLIP: nalgebra::Matrix4<f32> = nalgebra::Matrix4 {
	x:	nalgebra::Vector4 { x: 1., y: 0., z: 0., w: 0. },
	y:	nalgebra::Vector4 { x: 0., y: -1., z: 0., w: 0. },
	z:	nalgebra::Vector4 { x: 0., y: 0., z: 0.5, w: 0. },
	w:	nalgebra::Vector4 { x: 0., y: 0., z: 0.5, w: 1. }
};*/

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Default)]
pub struct Transform {
	pub location:	[f32; 3],
	pub rotation:	[f32; 3],
	pub scale:		f32,
}

impl ConstDefault for Transform {
	const DEFAULT: Self = Self {
		location:	[ConstDefault::DEFAULT; 3],
		rotation:	[ConstDefault::DEFAULT; 3],
		scale:		ConstDefault::DEFAULT,
	};
}

impl From<&Transform> for nalgebra::Matrix4<f32> {
	fn from(transform: &Transform) -> Self {
		(&crate::physics::Transform::from(transform)).into()
	}
}

impl From<&crate::physics::Transform> for Transform {
	fn from(transform: &crate::physics::Transform) -> Self {
		Self {
			location:	transform.position.into(),
			rotation:	transform.rotation.into(),
			scale:		transform.scale
		}
	}
}

#[derive(Debug)]
pub struct Camera {
	pub proj:		nalgebra::Matrix4<f32>,
	pub view:		nalgebra::Matrix4<f32>,
	pub position:	nalgebra::Point3<f32>,
}

impl Camera {
	pub fn new(extent: crate::vk::Extent2D) -> Self {
		Self {
			proj:		nalgebra::Matrix4::new_perspective(extent.width as f32 / extent.height as f32, FOVY, NEAR, FAR),
			view:		Default::default(),
			position:	nalgebra::Vector3::default().into(),
		}
	}

	pub fn update(&mut self, physics_system: &crate::physics::System, camera_physics: &crate::camera::CameraPhysics) {
		self.view = camera_physics.view(physics_system);
		self.position = camera_physics.position(physics_system);
	}
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct Light {
	pub direction:	nalgebra::Vector4<f32>,
}

pub struct RenderData {
	pub camera:			Camera,
	pub light:			Light,
}

impl RenderData {
	pub fn shadow_matrix(&self, arena: &typed_generational_arena::Arena<Component, usize, u64>) -> nalgebra::Matrix4<f32> {
		let light_space = nalgebra::Rotation::look_at_rh(&-self.light.direction.xyz(), &-nalgebra::Vector3::y());
		let mut min_max_o = [(f32::MAX, f32::MIN); 3];
		for (_, object) in arena.iter() {
			let origin = light_space * nalgebra::Vector3::from(object.map_object.transform.location);
			let radius = object.map_object.transform.scale * object.radius;

			let min_max = [(origin.x - radius, origin.x + radius), (origin.y - radius, origin.y + radius), (origin.z - radius, origin.z + radius)];
			for ((min, max), (o_min, o_max)) in min_max.iter().zip(min_max_o.iter_mut()) {
				if min < o_min {
					*o_min = *min;
				}
				if max > o_max {
					*o_max = *max;
				}
			}
		}

		let inverse_view = (self.camera.proj * self.camera.view).try_inverse().unwrap();
		let frustum = [ // These are in clip space, which means they have to be transformed back to world space using the inverse of the projection view matrix.
			nalgebra::Vector4::new(1., 1., 0., 1.),
			nalgebra::Vector4::new(-1., -1., 0., 1.),
			nalgebra::Vector4::new(1., -1., 0., 1.),
			nalgebra::Vector4::new(-1., 1., 0., 1.),
			nalgebra::Vector4::new(1., 1., 1., 1.),
			nalgebra::Vector4::new(-1., -1., 1., 1.),
			nalgebra::Vector4::new(1., -1., 1., 1.),
			nalgebra::Vector4::new(-1., 1., 1., 1.),
		];
		let mut min_max = [(std::f32::MAX, std::f32::MIN); 2];
		for point in &frustum {
			let point = inverse_view * point;
			let point: [_; 2] = (light_space * (point / point.w).xyz()).xy().into();
			for ((min, max), n) in min_max.iter_mut().zip(&point) {
				if n < min {
					*min = *n;
				}
				if n > max {
					*max = *n;
				}
			}
		}

		let (near, far) = if arena.is_empty() {
			(0., 1.)
		} else {
			min_max_o[2]
		};

		let left = min_max[0].0.max(min_max_o[0].0);
		let right = min_max[0].1.min(min_max_o[0].1);
		let bottom = min_max[1].0.max(min_max_o[1].0);
		let top = min_max[1].1.min(min_max_o[1].1);

		let delta = far - near;

		nalgebra::Matrix4::new_orthographic(
			/*left:*/	left,
			/*right:*/	right,
			/*bottom:*/	bottom,
			/*top:*/	top,
			/*near:*/	-delta,
			/*far:*/	delta,
		) * nalgebra::Matrix4::from(nalgebra::Translation::from(nalgebra::Vector3::new(0., 0., near))) *
			nalgebra::Matrix4::from(light_space)
	}
}
