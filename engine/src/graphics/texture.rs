use super::*;
pub struct Texture {
	pub image:		crate::vk::Image,
	pub image_view:	crate::vk::ImageView,
	pub allocation:	device::Allocation,
}

unsafe impl Send for Texture { }

impl Texture {
	pub fn new(allocator: &mut device::BasicAllocator, create_info: &crate::vk::ImageCreateInfo) -> VkResult<Self> {
		unsafe {
			let (image, allocation) = allocator.create_image(create_info)?;
			let mut image_view = std::mem::MaybeUninit::uninit();
			vk_try!(allocator.device.procs.create_image_view(allocator.device.device, &crate::vk::ImageViewCreateInfo {
				view_type:			crate::vk::ImageViewType::from_raw(match create_info.image_type.as_raw() {
					ty @ 0 | ty @ 1 =>
						ty + if create_info.array_layers == 1 { 0 } else { 4 },
					2 => 2,
					_ => if create_info.array_layers == 1 { 3 } else { 6 }
				}),
				format:				create_info.format,
				subresource_range:	crate::vk::ImageSubresourceRange {
					level_count:	create_info.mip_levels,
					layer_count:	create_info.array_layers,
					aspect_mask:	if let 124..=127 | 128..=130 = create_info.format.as_raw() {
						crate::vk::ImageAspectFlags::DEPTH
					} else {
						crate::vk::ImageAspectFlags::COLOR
					} |
					if let 127..=130 = create_info.format.as_raw() {
						crate::vk::ImageAspectFlags::STENCIL
					} else {
						ConstDefault::DEFAULT
					},
					..ConstDefault::DEFAULT
				},
				image, ..ConstDefault::DEFAULT
			}, 0 as _, image_view.as_mut_ptr()));
			Ok(Self { image, image_view: image_view.assume_init(), allocation })
		}
	}

	pub fn depth(allocator: &mut device::BasicAllocator, extent: crate::vk::Extent2D) -> VkResult<Self> {
		Self::new(allocator, &crate::vk::ImageCreateInfo {
			image_type:		crate::vk::ImageType::TYPE_2D,
			format:			DEPTH_FORMAT,
			mip_levels:		1,
			array_layers:	1,
			samples:		crate::vk::SampleCountFlags::TYPE_1,
			initial_layout:	crate::vk::ImageLayout::UNDEFINED,
			usage:			crate::vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
			sharing_mode:	crate::vk::SharingMode::EXCLUSIVE,
			extent:			crate::vk::Extent3D {
				width:	extent.width,
				height:	extent.height,
				depth:	1,
			},
			..ConstDefault::DEFAULT
		})
	}

	pub fn destroy(&mut self, allocator: &mut device::BasicAllocator) {
		unsafe {
			allocator.device.procs.destroy_image_view(allocator.device.device, self.image_view, 0 as _);
			allocator.device.procs.destroy_image(allocator.device.device, self.image, 0 as _);
			allocator.free(std::slice::from_mut(&mut self.allocation));
		}
	}
}
