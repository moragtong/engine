use super::*;

impl instance::Instance {
	pub fn create_surface(&self, window: &winit::window::Window) -> VkResult<crate::vk::SurfaceKHR> {
		unsafe {
			let mut surface = std::mem::MaybeUninit::uninit();

			#[cfg(unix)]
			{
				use winit::platform::unix::WindowExtUnix;
				vk_try!(if let (Some(display), Some(wsurface)) =
				(window.wayland_display(), window.wayland_surface()) {
					self.wayland_procs.create_wayland_surface_khr(self.instance, &crate::vk::WaylandSurfaceCreateInfoKHR {
						display:	display as _,
						surface:	wsurface as _,
						..ConstDefault::DEFAULT
					}, 0 as _, surface.as_mut_ptr())
				} else if let (Some(dpy), Some(window)) =
				(window.xlib_display(), window.xlib_window()) {
					self.xlib_procs.create_xlib_surface_khr(self.instance, &crate::vk::XlibSurfaceCreateInfoKHR {
						dpy: dpy as _, window, ..ConstDefault::DEFAULT
					}, 0 as _, surface.as_mut_ptr())
				} else {
					crate::vk::Result::ERROR_SURFACE_LOST_KHR
				})
			}

			#[cfg(windows)]
			{
				use winit::platform::windows::WindowExtWindows;
				vk_try!(self.windows_procs.create_win32_surface_khr(self.instance, &crate::vk::Win32SurfaceCreateInfoKHR {
					hinstance:	winapi::um::libloaderapi::GetModuleHandleA(0 as _) as _,
					hwnd:		window.hwnd() as _,
					..ConstDefault::DEFAULT
				}, 0 as _, surface.as_mut_ptr()));
			}

			Ok(surface.assume_init())
		}
	}
}