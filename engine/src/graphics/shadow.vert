#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 4) in vec3 position;

layout(location = 0) in mat4 proj_view_model;

void main() {
	gl_Position = proj_view_model * vec4(position, 1.0);
}