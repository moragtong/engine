use super::*;

#[derive(Debug, Copy, Clone, Default)]
#[repr(C, align(16))]
pub struct DrawCommand {
	inner:	crate::vk::DrawIndexedIndirectCommand,
	_pad:	[u32; 3],
}

impl From<crate::vk::DrawIndexedIndirectCommand> for DrawCommand {
	fn from(inner: crate::vk::DrawIndexedIndirectCommand) -> Self {
		Self { inner, ..Default::default() }
	}
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(C, align(16))]
pub struct PerInstance {
	pub draw_cmd:			DrawCommand,
	pub view_model:			[[f32; 4]; 4],
	pub proj_view_model:	[[f32; 4]; 4],
	pub light_model:		[[f32; 4]; 4],
	pub texture_layers:		util::Vec3<f32>,
	pub descriptor_indices:	util::Vec3<u32>,
}

#[derive(Debug)]
#[repr(C)]
pub struct PerVertex {
	pub position:	[f32; 3],
	pub normal:		nalgebra::Vector3<f32>,
	pub tangent:	nalgebra::Vector3<f32>,
	pub uv:			[f32; 2],
}

pub const VERTEX_BINDING_DESCRIPTIONS: [crate::vk::VertexInputBindingDescription; 2] = [
	crate::vk::VertexInputBindingDescription {
		binding:	0,
		stride:		std::mem::size_of::<PerInstance>() as _,
		input_rate:	crate::vk::VertexInputRate::INSTANCE,
	},
	crate::vk::VertexInputBindingDescription {
		binding:	1,
		stride:		std::mem::size_of::<PerVertex>() as _,
		input_rate:	crate::vk::VertexInputRate::VERTEX,
	}
];

const ATTRIBUTE_DESCRIPTIONS: [crate::vk::VertexInputAttributeDescription; 18] = [
	//model
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	0,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		std::mem::size_of::<DrawCommand>() as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	1,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 1]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	2,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 2]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	3,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 3]>()) as _,
		},

	//proj_view_model
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	4,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 4]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	5,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 5]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	6,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 6]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	7,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 7]>()) as _,
		},

	//light_model
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	8,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 8]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	9,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 9]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	10,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 10]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	11,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 11]>()) as _,
		},
	//texture_layers
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	12,
			format:		crate::vk::Format::R32G32B32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 12]>()) as _,
		},

	//descriptor_indices
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	13,
			format:		crate::vk::Format::R32G32B32_UINT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 13]>()) as _,
		},

	crate::vk::VertexInputAttributeDescription {
		binding:	1,
		location:	14,
		format:		crate::vk::Format::R32G32B32_SFLOAT,
		offset:		0,
	},
	crate::vk::VertexInputAttributeDescription {
		binding:	1,
		location:	15,
		format:		crate::vk::Format::R32G32B32_SFLOAT,
		offset:		std::mem::size_of::<[[f32; 3]; 1]>() as _,
	},
	crate::vk::VertexInputAttributeDescription {
		binding:	1,
		location:	16,
		format:		crate::vk::Format::R32G32B32_SFLOAT,
		offset:		std::mem::size_of::<[[f32; 3]; 2]>() as _,
	},
	crate::vk::VertexInputAttributeDescription {
		binding:	1,
		location:	17,
		format:		crate::vk::Format::R32G32_SFLOAT,
		offset:		std::mem::size_of::<[[f32; 3]; 3]>() as _,
	},
];

pub const VERTEX_INPUT_STATE: crate::vk::PipelineVertexInputStateCreateInfo = crate::vk::PipelineVertexInputStateCreateInfo {
	vertex_attribute_description_count:	18,
	p_vertex_attribute_descriptions:	&ATTRIBUTE_DESCRIPTIONS[0],
	vertex_binding_description_count:	2,
	p_vertex_binding_descriptions:		&VERTEX_BINDING_DESCRIPTIONS[0],
	..ConstDefault::DEFAULT
};

const BASIC_ATTRIBUTE_DESCRIPTIONS: [crate::vk::VertexInputAttributeDescription; 9] = [
	//model
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	0,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		std::mem::size_of::<DrawCommand>() as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	1,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 1]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	2,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 2]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	3,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 3]>()) as _,
		},

	//proj_view_model
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	4,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 4]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	5,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 5]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	6,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 6]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	7,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 7]>()) as _,
		},

	crate::vk::VertexInputAttributeDescription {
		binding:	1,
		location:	8,
		format:		crate::vk::Format::R32G32B32_SFLOAT,
		offset:		0,
	},
];


pub const BASIC_VERTEX_INPUT_STATE: crate::vk::PipelineVertexInputStateCreateInfo = crate::vk::PipelineVertexInputStateCreateInfo {
	vertex_attribute_description_count:	9,
	p_vertex_attribute_descriptions:	&BASIC_ATTRIBUTE_DESCRIPTIONS[0],
	vertex_binding_description_count:	2,
	p_vertex_binding_descriptions:		&VERTEX_BINDING_DESCRIPTIONS[0],
	..ConstDefault::DEFAULT
};

const SHADOW_ATTRIBUTE_DESCRIPTIONS: [crate::vk::VertexInputAttributeDescription; 5] = [
	//light_model
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	0,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 8]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	1,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 9]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	2,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 10]>()) as _,
		},
		crate::vk::VertexInputAttributeDescription {
			binding:	0,
			location:	3,
			format:		crate::vk::Format::R32G32B32A32_SFLOAT,
			offset:		(std::mem::size_of::<DrawCommand>() + std::mem::size_of::<[[f32; 4]; 11]>()) as _,
		},

	crate::vk::VertexInputAttributeDescription {
		binding:	1,
		location:	4,
		format:		crate::vk::Format::R32G32B32_SFLOAT,
		offset:		0,
	},
];


pub const SHADOW_VERTEX_INPUT_STATE: crate::vk::PipelineVertexInputStateCreateInfo = crate::vk::PipelineVertexInputStateCreateInfo {
	vertex_attribute_description_count:	5,
	p_vertex_attribute_descriptions:	&SHADOW_ATTRIBUTE_DESCRIPTIONS[0],
	vertex_binding_description_count:	2,
	p_vertex_binding_descriptions:		&VERTEX_BINDING_DESCRIPTIONS[0],
	..ConstDefault::DEFAULT
};

pub const QUAD_VERTEX_INPUT_STATE: crate::vk::PipelineVertexInputStateCreateInfo = crate::vk::PipelineVertexInputStateCreateInfo {
	/*vertex_attribute_description_count:	1,
	p_vertex_attribute_descriptions:	&QUAD_ATTRIBUTE_DESCRIPTIONS[0],
	vertex_binding_description_count:	1,
	p_vertex_binding_descriptions:		&QUAD_BINDING_DESCRIPTIONS[0],*/
	..ConstDefault::DEFAULT
};
