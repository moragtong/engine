use super::*;

pub const INSTANCE_COUNT: u8 = 40;
const VERTEX_COUNT: crate::vk::DeviceSize = INSTANCE_COUNT as crate::vk::DeviceSize * 1024 * 128;
const INDEX_COUNT: crate::vk::DeviceSize = VERTEX_COUNT * 3;
pub const SET_COUNT: u8 = SWAPCHAIN_IMAGE_COUNT;
pub const MAX_TEXTURE_COUNT: u8 = 16;

pub struct ModelData {
	pub vertex:		device::buffer::SubBuffer<per_vertex::PerVertex>,
	pub index:		device::buffer::SubBuffer<u32>,
	pub radius:		f32,
}

pub struct TextureUploadInfo {
	pub format:			crate::vk::Format,
	pub ratio:			crate::vk::DeviceSize,
	pub size:			crate::vk::DeviceSize,
	pub extent:			crate::vk::Extent3D,
	pub create_info:	device::TextureCreateInfo,
	pub level_count:	u32,
	pub layer_count:	u32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MapObject {
	pub model:			String,
	pub textures:		[String; TEXTURE_COUNT as _],
	pub transform:		render_data::Transform,
	pub static_level:	u8,
}

pub struct ResourceManager {
	pub(in crate::graphics)		loader:					Loader,

	texture_map:				std::collections::HashMap<String, (usize, device::TextureAllocation)>,
	model_map:					std::collections::HashMap<String, (usize, ModelData)>,
	index_buffer:				device::buffer::Buffer<u32>,

	vertex_buffer:				device::buffer::Buffer<per_vertex::PerVertex>,

	per_instance_buffer:		crate::vk::Buffer,
	per_instance_allocation:	device::Allocation,

	descriptor_pool:			crate::vk::DescriptorPool,
	descriptor_sets:			[crate::vk::DescriptorSet; SET_COUNT as _],
	needs_update:				bool,
	descriptor_set_index:		usize,
}

unsafe impl std::marker::Send for ResourceManager {}

impl ResourceManager {
	pub(in crate::graphics) fn new(mut loader: Loader, set_layout: crate::vk::DescriptorSetLayout) -> VkResult<Self> {
		let (per_instance_buffer, per_instance_allocation) = loader.uploader.allocator.create_buffer(&crate::vk::BufferCreateInfo {
			size:			INSTANCE_COUNT as u64 * SWAPCHAIN_IMAGE_COUNT as u64 * std::mem::size_of::<PerInstance>() as u64,
			usage:			crate::vk::BufferUsageFlags::VERTEX_BUFFER |
							crate::vk::BufferUsageFlags::INDIRECT_BUFFER |
							crate::vk::BufferUsageFlags::STORAGE_BUFFER |
							crate::vk::BufferUsageFlags::TRANSFER_DST,
			sharing_mode:	crate::vk::SharingMode::EXCLUSIVE,
			..ConstDefault::DEFAULT
		})?;

		let mut descriptor_pool = std::mem::MaybeUninit::uninit();
		vk_try!(unsafe { loader.uploader.allocator.device.procs.create_descriptor_pool(loader.uploader.allocator.device.device, 
			&crate::vk::DescriptorPoolCreateInfo {
				max_sets:			SET_COUNT.into(),
				pool_size_count:	1,
				p_pool_sizes:		&crate::vk::DescriptorPoolSize {
					ty:					crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
					descriptor_count:	(MAX_TEXTURE_COUNT * SET_COUNT) as _,
				},
				..ConstDefault::DEFAULT
			}, 0 as _, descriptor_pool.as_mut_ptr()) });

		let descriptor_pool = unsafe { descriptor_pool.assume_init() };
		let descriptor_set_layouts = [set_layout; SET_COUNT as _];
		let mut descriptor_sets = std::mem::MaybeUninit::<[crate::vk::DescriptorSet; SET_COUNT as _]>::uninit();

		vk_try!(unsafe { loader.uploader.allocator.device.procs.allocate_descriptor_sets(loader.uploader.allocator.device.device, 
			&crate::vk::DescriptorSetAllocateInfo {
				descriptor_set_count:	SET_COUNT.into(),
				p_set_layouts:			descriptor_set_layouts.as_ptr(),
				descriptor_pool, ..ConstDefault::DEFAULT
			}, descriptor_sets.as_mut_ptr() as _) });

		let descriptor_sets = unsafe { descriptor_sets.assume_init() };
		loader.uploader.allocator.update_descriptor_set(descriptor_sets[0]);

		Ok(Self {
			model_map:				Default::default(),
			texture_map:			Default::default(),
			vertex_buffer:			device::buffer::Buffer::new(&mut loader.uploader.allocator, crate::vk::BufferCreateInfo {
				size:			VERTEX_COUNT,
				usage:			crate::vk::BufferUsageFlags::VERTEX_BUFFER | crate::vk::BufferUsageFlags::TRANSFER_DST,
				sharing_mode:	crate::vk::SharingMode::EXCLUSIVE,
				..ConstDefault::DEFAULT
			})?,
			index_buffer:			device::buffer::Buffer::new(&mut loader.uploader.allocator, crate::vk::BufferCreateInfo {
				size:			INDEX_COUNT,
				usage:			crate::vk::BufferUsageFlags::INDEX_BUFFER | crate::vk::BufferUsageFlags::TRANSFER_DST,
				sharing_mode:	crate::vk::SharingMode::EXCLUSIVE,
				..ConstDefault::DEFAULT
			})?,
			descriptor_set_index:	0,
			needs_update:			Default::default(),
			descriptor_pool, per_instance_buffer, per_instance_allocation,
			loader, descriptor_sets
		})
	}
}

pub struct RenderResources {
	pub index_buffer:			crate::vk::Buffer,

	pub per_instance_buffer:	crate::vk::Buffer,
	pub vertex_buffer:			crate::vk::Buffer,

	pub descriptor_set:			crate::vk::DescriptorSet,
}

#[derive(Debug)]
pub struct Payload {
	pub(in crate::graphics) descriptor_set:		crate::vk::DescriptorSet,

	pub(in crate::graphics) barriers:			Vec<crate::vk::ImageMemoryBarrier>,

	pub(in crate::graphics) components:			Vec<Component>,
}

unsafe impl std::marker::Send for Payload {}

impl ResourceManager {
	pub fn render_resources(&self) -> RenderResources {
		let index = self.descriptor_set_index;
		RenderResources {
			index_buffer:			self.index_buffer.buffer,
			per_instance_buffer:	self.per_instance_buffer,
			vertex_buffer:			self.vertex_buffer.buffer,
			descriptor_set:			self.descriptor_sets[(index + SET_COUNT as usize - 1) % SET_COUNT as usize],
		}
	}

	pub async fn load_model(&mut self, name: String) -> Result<&ModelData, EngineError> {
		use async_std::io::prelude::ReadExt;
		Ok(match self.model_map.entry(name.clone()) {
			std::collections::hash_map::Entry::Occupied(occupied) => {
				println!("-----Occupied------");
				let occupied = occupied.into_mut();
				occupied.0 += 1;
				&occupied.1
			},
			std::collections::hash_map::Entry::Vacant(vacant) => {
				println!("----{:?}----", name);
				let mut model_loader = model_loader::ModelLoader::new(&name).await?;

				let vertex = self.vertex_buffer.allocate(model_loader.vertex_count.into()).unwrap(); //.ok_or(crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY)?;
				let index = self.index_buffer.allocate(model_loader.index_count.into()).unwrap(); //.ok_or(crate::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY)?;
				{
					let mut vertex_src = self.loader.allocate_upload_range::<per_vertex::PerVertex>(model_loader.vertex_count.into(), ConstDefault::DEFAULT).await?;
					model_loader.file.read_exact(vertex_src.raw_data()).await?;
					self.loader.upload.upload_buffer(&vertex_src.source(), &self.vertex_buffer, &vertex);
				}
				{
					let mut index_src = self.loader.allocate_upload_range::<u32>(model_loader.index_count.into(), ConstDefault::DEFAULT).await?;
					model_loader.file.read_exact(index_src.raw_data()).await?;
					self.loader.upload.upload_buffer(&index_src.source(), &self.index_buffer, &index);
				}
				&vacant.insert((1, ModelData { vertex, index, radius: model_loader.radius })).1
			}
		})
	}

	fn unload_model(&mut self, name: String) -> Result<(), EngineError> {
		if let std::collections::hash_map::Entry::Occupied(mut occupied) = self.model_map.entry(name) {
			occupied.get_mut().0 -= 1;
			if occupied.get_mut().0 == 0 {
				let (_, (_, model_data)) = occupied.remove_entry();
				self.vertex_buffer.deallocate(model_data.vertex);
				self.index_buffer.deallocate(model_data.index);
			}
			Ok(())
		} else {
			Err(EngineError::ResourceNotFound)
		}
	}

	pub async fn load_texture(&mut self, name: String) -> Result<&device::TextureAllocation, EngineError> {
		use async_std::io::prelude::ReadExt;
		Ok(match self.texture_map.entry(name.clone()) {
			std::collections::hash_map::Entry::Occupied(occupied) => {
				println!("-----Occupied------");
				let occupied = occupied.into_mut();
				occupied.0 += 1;
				&occupied.1
			}
			std::collections::hash_map::Entry::Vacant(vacant) => {
				println!("Vacant----{:?}----", name);
				let mut texture_loader = texture_loader::TextureLoader::new(name).await?;
				let (texture, needs_update) = self.loader.uploader.allocator.allocate_texture(&texture_loader.upload_info.create_info.into_image_create_info(), false)?;
				let mut src = self.loader.allocate_upload_range_bytes(texture_loader.upload_info.size,
					self.loader.uploader.allocator.device.granularity.max(texture_loader.upload_info.ratio),
					ConstDefault::DEFAULT).await?;
				texture_loader.file.read(src.raw_data()).await?;
				self.loader.upload.upload_texture(texture_loader.upload_info, &src.source(), &texture);
				self.needs_update |= needs_update;
				&vacant.insert((1, texture)).1	
			}
		})
	}

	fn unload_texture(&mut self, name: String) -> Result<(), EngineError> {
		if let std::collections::hash_map::Entry::Occupied(mut occupied) = self.texture_map.entry(name) {
			occupied.get_mut().0 -= 1;
			if occupied.get_mut().0 == 0 {
				let (_, (_, texture_alloc)) = occupied.remove_entry();
				self.loader.uploader.allocator.deallocate_texture(texture_alloc);
			}
			Ok(())
		} else {
			Err(EngineError::ResourceNotFound)
		}
	}

	pub async fn load_objects(&mut self, map_objects: &[MapObject]) -> Result<Payload, EngineError> {
		let mut components = Vec::default();
		println!("{:?}", map_objects);
		for map_object in map_objects {
			let (index_count, first_index, vertex_offset, radius) = {
				let model_data = self.load_model(map_object.model.clone()).await?;
				(model_data.index.len() as _, model_data.index.range().start as _, model_data.vertex.range().start as _, model_data.radius)
			};
			let mut texture_layers = [0.; TEXTURE_COUNT as _];
			let mut descriptor_indices = [0; TEXTURE_COUNT as _];
			for ((texture, texture_layer), descriptor_index) in map_object.textures.iter().zip(&mut texture_layers).zip(&mut descriptor_indices) {
				let texture_alloc = self.load_texture(texture.clone()).await?;
				*descriptor_index = texture_alloc.descriptor_index;
				*texture_layer = texture_alloc.range.start as _;
			}

			println!("object");

			components.push(Component {
				draw_cmd:	crate::vk::DrawIndexedIndirectCommand {
					index_count, first_index, vertex_offset,
					instance_count:	1,
					..ConstDefault::DEFAULT
				},
				texture_layers:	texture_layers.into(),
				descriptor_indices: descriptor_indices.into(),
				map_object:			map_object.clone(),
				radius
			});
		}

		Ok(Payload {
			descriptor_set:			*if self.needs_update {
				self.descriptor_set_index = (self.descriptor_set_index + 1) % SET_COUNT as usize;
				let descriptor_set = unsafe { self.descriptor_sets.get_unchecked(self.descriptor_set_index) };
				self.loader.uploader.allocator.update_descriptor_set(*descriptor_set);
				self.needs_update = false;
				descriptor_set
			} else {
				unsafe { self.descriptor_sets.get_unchecked(self.descriptor_set_index) }
			},
			barriers:		self.loader.submit(ConstDefault::DEFAULT).await?.barriers,
			components
		})
	}

	pub(in crate::graphics) fn unload_object(&mut self, component: Component) -> Result<(), EngineError> {
		let Component {
			map_object: MapObject { model, mut textures, .. },
		.. } = component;

		textures.reverse();

		for texture in textures {
			self.unload_texture(texture)?
		}

		self.unload_model(model)
	}
}

impl Drop for ResourceManager {
	fn drop(&mut self) {
		self.loader.uploader.allocator.free(&mut [self.per_instance_allocation]);
		unsafe {
			self.loader.uploader.allocator.device.procs.destroy_buffer(self.loader.uploader.allocator.device.device, self.per_instance_buffer, 0 as _);
			self.loader.uploader.allocator.device.procs.destroy_descriptor_pool(self.loader.uploader.allocator.device.device, self.descriptor_pool, 0 as _);
		}
		self.index_buffer.destroy(&mut self.loader.uploader.allocator);
		self.vertex_buffer.destroy(&mut self.loader.uploader.allocator);
	}
}
