use super::*;
mod allocate;

pub use self::allocate::*;

use std::convert::{ TryFrom, TryInto };

const EXTENSIONS: [*const std::os::raw::c_char; 4] = [
	cstr!("VK_KHR_swapchain"),
	cstr!("VK_KHR_get_memory_requirements2"),
	cstr!("VK_KHR_dedicated_allocation"),
	cstr!("VK_KHR_maintenance1"),
];

pub struct Device {
	pub instance:				instance::Instance,
	pub physical:				crate::vk::PhysicalDevice,
	pub device:					crate::vk::Device,
	pub procs:					crate::vk::DeviceFnV1_0,
	pub procs2:					crate::vk::DeviceFnV1_1,
	pub swapchain_procs:		crate::vk::KhrSwapchainFn,
	//#[cfg(debug_assertions)]
	//pub debug_procs:			crate::vk::extensions::ExtDebugUtilsFn,
	pub compute_queue:			crate::vk::Queue,
	pub compute_index:			u32,
	pub graphics_queue:			crate::vk::Queue,
	pub graphics_index:			u32,
	pub present_queue:			crate::vk::Queue,
	pub transfer_queue:			crate::vk::Queue,
	pub transfer_index:			u32,
	pub granularity:			crate::vk::DeviceSize,
	pub transfer_granularity:	crate::vk::Extent3D,
	pub uniform_align:			crate::vk::DeviceSize,
	pub storage_align:			crate::vk::DeviceSize,
	pub non_coherent_atom_size:	crate::vk::DeviceSize,
	pub max_push_constant_size:	u32,
	pub memory_properties:		crate::vk::PhysicalDeviceMemoryProperties,
}

impl Device {
	pub unsafe fn new(instance: instance::Instance, surface: crate::vk::SurfaceKHR) -> VkResult<Self> {
		let mut physicals = std::mem::MaybeUninit::<[_; 8]>::uninit();
		let mut count = 8;
		vk_try!(instance.procs.enumerate_physical_devices(instance.instance, &mut count, physicals.as_mut_ptr() as _));

		let physicals = physicals.assume_init();

		for &physical in physicals.get_unchecked(..count.try_into().unwrap()) {
			count = 9;
			vk_try!(instance.surface_procs.get_physical_device_surface_formats_khr(physical, surface, &mut count, 0 as _));

			if count == 0 {
				continue
			}

			count = 8;
			let mut present_modes = std::mem::MaybeUninit::<[crate::vk::PresentModeKHR; 8]>::uninit();
			vk_try!(instance.surface_procs.get_physical_device_surface_present_modes_khr(physical, surface, &mut count, present_modes.as_mut_ptr() as _));

			if count == 0 {
				continue
			}

			let present_modes = present_modes.assume_init();

			if !present_modes.get_unchecked(..count.try_into().unwrap()).iter().any(|&mode| mode == crate::vk::PresentModeKHR::IMMEDIATE) {
				continue
			}

			let mut properties = std::mem::MaybeUninit::<[crate::vk::ExtensionProperties; 128]>::uninit();
			count = 128;
			vk_try!(instance.procs.enumerate_device_extension_properties(physical, 0 as _, &mut count, properties.as_mut_ptr() as _));

			if count == 0 {
				continue
			}

			let properties = properties.assume_init();

			let properties = properties.get_unchecked(..count.try_into().unwrap());

			if EXTENSIONS.iter().any(|extension|
				!properties.iter().any(|property|
					libc::strncmp(property.extension_name.as_ptr(), *extension, property.extension_name.len()) == 0
			)) {
				continue
			}

			count = 32;
			let mut family_properties = std::mem::MaybeUninit::<[crate::vk::QueueFamilyProperties; 32]>::uninit();
			instance.procs.get_physical_device_queue_family_properties(physical, &mut count, family_properties.as_mut_ptr() as _);

			let family_properties = family_properties.assume_init();
			let family_properties = family_properties.get_unchecked(..count.try_into().unwrap());

			let mut graphics_index = None;
			let mut presentation_index = None;
			let mut transfer_index = None;
			let mut compute_index = None;

			for (i, family) in family_properties.iter().enumerate() {
				if (family.queue_flags & crate::vk::QueueFlags::GRAPHICS).is_empty() && !(family.queue_flags & crate::vk::QueueFlags::COMPUTE).is_empty() {
					println!("Found compute queue");
					compute_index = Some(u32::try_from(i).unwrap());
				} else if (family.queue_flags & crate::vk::QueueFlags::GRAPHICS).is_empty() && !(family.queue_flags & crate::vk::QueueFlags::TRANSFER).is_empty()  {
					println!("Found transfer queue");
					transfer_index = Some(u32::try_from(i).unwrap());
				} else if !(family.queue_flags & crate::vk::QueueFlags::GRAPHICS).is_empty() {
					graphics_index = Some(u32::try_from(i).unwrap());

					vk_try!(instance.surface_procs.get_physical_device_surface_support_khr(physical, i.try_into().unwrap(), surface, &mut count));

					if count != 0 {
						presentation_index = Some(u32::try_from(i).unwrap());
					}
				} else if presentation_index.is_none() {
					vk_try!(instance.surface_procs.get_physical_device_surface_support_khr(physical, i.try_into().unwrap(), surface, &mut count));

					if count != 0 {
						presentation_index = Some(u32::try_from(i).unwrap());
					}
				}
			}

			if let (Some(graphics_index), Some(present_i)) = (graphics_index, presentation_index) {
				let mut device = std::mem::MaybeUninit::uninit();
				let transfer_i = transfer_index.unwrap_or(graphics_index);
				let compute_i = compute_index.unwrap_or(graphics_index);
				let transfer_queue_i = transfer_index.unwrap_or(present_i);

				let infos = [
					crate::vk::DeviceQueueCreateInfo {
						queue_family_index:	graphics_index,
						queue_count:		1,
						p_queue_priorities:	&1.0,
						..ConstDefault::DEFAULT
					},
					crate::vk::DeviceQueueCreateInfo {
						queue_family_index:	compute_index.unwrap_or(transfer_queue_i),
						queue_count:		1,
						p_queue_priorities:	&1.0,
						..ConstDefault::DEFAULT
					},
					crate::vk::DeviceQueueCreateInfo {
						queue_family_index:	transfer_queue_i,
						queue_count:		1,
						p_queue_priorities:	&1.0,
						..ConstDefault::DEFAULT
					},
					crate::vk::DeviceQueueCreateInfo {
						queue_family_index:	present_i,
						queue_count:		1,
						p_queue_priorities:	&1.0,
						..ConstDefault::DEFAULT
					}
				];

				vk_try!(instance.procs.create_device(physical, &crate::vk::DeviceCreateInfo {
					queue_create_info_count:	1 + u32::from(compute_index.is_some()) +
												u32::from(graphics_index != present_i) +
												u32::from(transfer_index.is_some()),
					p_queue_create_infos:		infos.as_ptr(),
					enabled_extension_count:	EXTENSIONS.len().try_into().unwrap(),
					pp_enabled_extension_names:	EXTENSIONS.as_ptr(),
					p_next:						&crate::vk::PhysicalDeviceFeatures2 {
						features:	crate::vk::PhysicalDeviceFeatures {
							geometry_shader:				crate::vk::TRUE,
							sampler_anisotropy:				crate::vk::TRUE,
							texture_compression_bc:			crate::vk::TRUE,
							multi_draw_indirect:			crate::vk::TRUE,
							draw_indirect_first_instance:	crate::vk::TRUE,
							depth_clamp:					crate::vk::TRUE,
							..ConstDefault::DEFAULT
						},
						p_next:		&crate::vk::PhysicalDeviceDescriptorIndexingFeaturesEXT {
							shader_sampled_image_array_non_uniform_indexing:	crate::vk::TRUE,
							//descriptor_binding_variable_descriptor_count:		crate::vk::TRUE,
							runtime_descriptor_array:							crate::vk::TRUE,
							descriptor_binding_partially_bound:					crate::vk::TRUE,
							/*p_next:												&crate::vk::PhysicalDeviceTimelineSemaphoreFeaturesKHR {
								timeline_semaphore:	crate::vk::TRUE,
								..ConstDefault::DEFAULT
							} as *const _ as _,*/
							..ConstDefault::DEFAULT
						} as *const _ as _,
						..ConstDefault::DEFAULT
					} as *const _ as _,
					..ConstDefault::DEFAULT
				}, 0 as _, device.as_mut_ptr()));

				let device = device.assume_init();

				/*#[cfg(debug_assertions)]
				let debug_procs;*/

				let (procs, procs2, swapchain_procs) = {
					let load_fn = |function: &std::ffi::CStr| std::mem::transmute(instance.procs.get_device_proc_addr(device, function.as_ptr()));
					/*#[cfg(debug_assertions)]
					{
						debug_procs = crate::vk::extensions::ExtDebugUtilsFn::load(load_fn);
					}*/
					(crate::vk::DeviceFnV1_0::load(load_fn),
					crate::vk::DeviceFnV1_1::load(load_fn),
					crate::vk::KhrSwapchainFn::load(load_fn),
						)
				};
						
				//use crate::vk::Handle;
				let mut transfer_queue = std::mem::MaybeUninit::uninit();
				procs.get_device_queue(device, transfer_i, 0, transfer_queue.as_mut_ptr());
				/*#[cfg(debug_assertions)]
				vk_try!(debug_procs.set_debug_utils_object_name_ext(device, &crate::vk::DebugUtilsObjectNameInfoEXT {
					object_type:	crate::vk::Queue::TYPE,
					object_handle:	transfer_queue.as_raw(),
					p_object_name:	cstr!("transfer_queue"),
					..ConstDefault::DEFAULT
				}));*/

				let mut compute_queue = std::mem::MaybeUninit::uninit();
				procs.get_device_queue(device, compute_i, 0, compute_queue.as_mut_ptr());
				/*#[cfg(debug_assertions)]
				vk_try!(debug_procs.set_debug_utils_object_name_ext(device, &crate::vk::DebugUtilsObjectNameInfoEXT {
					object_type:	crate::vk::Queue::TYPE,
					object_handle:	compute_queue.as_raw(),
					p_object_name:	cstr!("compute_queue"),
					..ConstDefault::DEFAULT
				}));*/

				let mut present_queue = std::mem::MaybeUninit::uninit();
				procs.get_device_queue(device, present_i, 0, present_queue.as_mut_ptr());
				/*#[cfg(debug_assertions)]
				vk_try!(debug_procs.set_debug_utils_object_name_ext(device, &crate::vk::DebugUtilsObjectNameInfoEXT {
					object_type:	crate::vk::Queue::TYPE,
					object_handle:	present_queue.as_raw(),
					p_object_name:	cstr!("present_queue"),
					..ConstDefault::DEFAULT
				}));*/

				let mut graphics_queue = std::mem::MaybeUninit::uninit();
				procs.get_device_queue(device, graphics_index, 0, graphics_queue.as_mut_ptr());
				/*#[cfg(debug_assertions)]
				vk_try!(debug_procs.set_debug_utils_object_name_ext(device, &crate::vk::DebugUtilsObjectNameInfoEXT {
					object_type:	crate::vk::Queue::TYPE,
					object_handle:	graphics_queue.as_raw(),
					p_object_name:	cstr!("graphics_queue"),
					..ConstDefault::DEFAULT
				}));*/

				let mut memory_properties = std::mem::MaybeUninit::uninit();
				instance.procs.get_physical_device_memory_properties(physical, memory_properties.as_mut_ptr());

				let mut properties = std::mem::MaybeUninit::uninit();
				instance.procs.get_physical_device_properties(physical, properties.as_mut_ptr());
				let properties = properties.assume_init();

				return Ok(Self {
					transfer_granularity:	family_properties.get_unchecked(usize::try_from(transfer_i).unwrap()).min_image_transfer_granularity,
					granularity:			properties.limits.buffer_image_granularity,
					uniform_align:			properties.limits.min_uniform_buffer_offset_alignment,
					storage_align:			properties.limits.min_storage_buffer_offset_alignment,
					non_coherent_atom_size:	properties.limits.non_coherent_atom_size,
					max_push_constant_size:	properties.limits.max_push_constants_size,
					memory_properties:		memory_properties.assume_init(),
					transfer_index:			transfer_i,
					compute_index:			compute_i,
					compute_queue:			compute_queue.assume_init(),
					graphics_queue:			graphics_queue.assume_init(),
					present_queue:			present_queue.assume_init(),
					transfer_queue:			transfer_queue.assume_init(),
					instance, physical, device, procs, graphics_index, procs2, swapchain_procs,
					/*#[cfg(debug_assertions)]
					debug_procs*/
				})
			}
		}
		panic!("shhhit");
		//Err(crate::vk::Result::ERROR_INCOMPATIBLE_DRIVER)
	}

	pub unsafe fn create_shader_module(&self, code: &'static [u32]) -> VkResult<crate::vk::ShaderModule> {
		let mut shader_module = std::mem::MaybeUninit::uninit();
		vk_try!(self.procs.create_shader_module(self.device, &crate::vk::ShaderModuleCreateInfo {
			code_size:	(code.len() * std::mem::size_of::<u32>()).try_into().unwrap(),
			p_code:		code.as_ptr(),
			..ConstDefault::DEFAULT
		}, 0 as _, shader_module.as_mut_ptr()));
		Ok(shader_module.assume_init())
	}

	pub fn find_index(&self, property_flags: crate::vk::MemoryPropertyFlags) -> Option<(u32, crate::vk::DeviceSize)> {
		unsafe { self.memory_properties.memory_types.get_unchecked(..self.memory_properties.memory_type_count.try_into().unwrap())
		.iter().enumerate().find_map(|(i, memory_type)|
			if !(memory_type.property_flags & property_flags).is_empty()  {
				Some((i.try_into().unwrap(), self.memory_properties.memory_heaps.get_unchecked(usize::try_from(memory_type.heap_index).unwrap()).size))
			} else {
				None
			}
		) }
	}

	pub async fn poll_fence(&self, fence: crate::vk::Fence) -> VkResult<()> {
		async_std::future::poll_fn(|_| unsafe {
			match self.procs.wait_for_fences(self.device, 1, &fence, 1, std::time::Duration::from_millis(15).as_nanos().try_into().unwrap()) {
				crate::vk::Result::SUCCESS => {
					vk_try!(self.procs.reset_fences(self.device, 1, &fence));
					async_std::task::Poll::Ready(Ok(()))
				}
				crate::vk::Result::NOT_READY => async_std::task::Poll::Pending,
				res => async_std::task::Poll::Ready(Err(res)),
			}
		}).await
	}
}

impl Drop for Device {
	fn drop(&mut self) {
		unsafe {
			self.procs.destroy_device(self.device, 0 as _);
		}
	}
}
