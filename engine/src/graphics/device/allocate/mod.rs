use super::*;
pub mod buffer;
use vma as vk_mem;

use std::convert::{ TryInto, TryFrom };

pub type Allocation = vk_mem::VmaAllocation;

pub struct BasicAllocator {
	pub device:				std::sync::Arc<device::Device>,
	pub allocator:			vk_mem::VmaAllocator,
	gpu_pool:				vk_mem::VmaPool,
	cpu_pool:				vk_mem::VmaPool,
	buffer:					crate::vk::Buffer,
	pub allocation:			vk_mem::VmaAllocation,
	pub allocation_info:	vk_mem::VmaAllocationInfo,
}

impl std::convert::TryFrom<std::sync::Arc<device::Device>> for BasicAllocator {
	type Error = crate::vk::Result;
	fn try_from(device:	std::sync::Arc<device::Device>) -> VkResult<Self> {
		unsafe {
			let vulkan_functions = vk_mem::VmaVulkanFunctions {
				vkGetPhysicalDeviceProperties: Some(std::mem::transmute(device.instance.procs.get_physical_device_properties)),
				vkGetPhysicalDeviceMemoryProperties: Some(std::mem::transmute(device.instance.procs.get_physical_device_memory_properties)),
				vkAllocateMemory: Some(std::mem::transmute(device.procs.allocate_memory)),
				vkFreeMemory: Some(std::mem::transmute(device.procs.free_memory)),
				vkMapMemory: Some(std::mem::transmute(device.procs.map_memory)),
				vkUnmapMemory: Some(std::mem::transmute(device.procs.unmap_memory)),
				vkFlushMappedMemoryRanges: Some(std::mem::transmute(device.procs.flush_mapped_memory_ranges)),
				vkInvalidateMappedMemoryRanges: Some(std::mem::transmute(device.procs.invalidate_mapped_memory_ranges)),
				vkBindBufferMemory: Some(std::mem::transmute(device.procs.bind_buffer_memory)),
				vkBindImageMemory: Some(std::mem::transmute(device.procs.bind_image_memory)),
				vkGetBufferMemoryRequirements: Some(std::mem::transmute(device.procs.get_buffer_memory_requirements)),
				vkGetImageMemoryRequirements: Some(std::mem::transmute(device.procs.get_image_memory_requirements)),
				vkCreateBuffer: Some(std::mem::transmute(device.procs.create_buffer)),
				vkDestroyBuffer: Some(std::mem::transmute(device.procs.destroy_buffer)),
				vkCreateImage: Some(std::mem::transmute(device.procs.create_image)),
				vkDestroyImage: Some(std::mem::transmute(device.procs.destroy_image)),
				vkCmdCopyBuffer: Some(std::mem::transmute(device.procs.cmd_copy_buffer)),
				vkGetBufferMemoryRequirements2KHR: Some(std::mem::transmute(device.procs2.get_buffer_memory_requirements2)),
				vkGetImageMemoryRequirements2KHR: Some(std::mem::transmute(device.procs2.get_image_memory_requirements2)),
				vkBindBufferMemory2KHR:	Some(std::mem::transmute(device.procs2.bind_buffer_memory2)),
				vkBindImageMemory2KHR:	Some(std::mem::transmute(device.procs2.bind_image_memory2)),
				vkGetPhysicalDeviceMemoryProperties2KHR: Some(std::mem::transmute(device.instance.procs2.get_physical_device_memory_properties2)),
			};

			let mut allocator = std::mem::MaybeUninit::uninit();
			vk_try!(crate::vk::Result::from_raw(vk_mem::vmaCreateAllocator(&vk_mem::VmaAllocatorCreateInfo {
				flags:					vma::VmaAllocatorCreateFlagBits_VMA_ALLOCATOR_CREATE_EXTERNALLY_SYNCHRONIZED_BIT |
					vma::VmaAllocatorCreateFlagBits_VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT,
				physicalDevice:		std::mem::transmute(device.physical),
				device:					std::mem::transmute(device.device),
				frameInUseCount:	(SWAPCHAIN_IMAGE_COUNT - 1).into(),
				pVulkanFunctions:	&vulkan_functions,
				..std::mem::zeroed()
			}, allocator.as_mut_ptr())));

			let allocator = allocator.assume_init();

			let mut gpu_index = std::mem::MaybeUninit::uninit();
			vk_mem::vmaFindMemoryTypeIndex(allocator, std::u32::MAX, &vk_mem::VmaAllocationCreateInfo {
				usage:	vk_mem::VmaMemoryUsage_VMA_MEMORY_USAGE_GPU_ONLY,
				..std::mem::zeroed()
			}, gpu_index.as_mut_ptr());

			let gpu_index = gpu_index.assume_init();

			let mut cpu_index = std::mem::MaybeUninit::uninit();
			vk_mem::vmaFindMemoryTypeIndex(allocator, std::u32::MAX, &vk_mem::VmaAllocationCreateInfo {
				usage:	vk_mem::VmaMemoryUsage_VMA_MEMORY_USAGE_CPU_ONLY,
				..std::mem::zeroed()
			}, cpu_index.as_mut_ptr());

			let cpu_index = cpu_index.assume_init();

			let mut gpu_pool = std::mem::MaybeUninit::uninit();
			vk_try!(crate::vk::Result::from_raw(vk_mem::vmaCreatePool(allocator, &vk_mem::VmaPoolCreateInfo {
				memoryTypeIndex:	gpu_index,
				frameInUseCount:	(SWAPCHAIN_IMAGE_COUNT - 1).into(),
				..std::mem::zeroed()
			}, gpu_pool.as_mut_ptr())));

			let gpu_pool = gpu_pool.assume_init();

			let mut cpu_pool = std::mem::MaybeUninit::uninit();

			vk_try!(crate::vk::Result::from_raw(vk_mem::vmaCreatePool(allocator, &vk_mem::VmaPoolCreateInfo {
				memoryTypeIndex:	cpu_index,
				flags:				vma::VmaPoolCreateFlagBits_VMA_POOL_CREATE_LINEAR_ALGORITHM_BIT,
				frameInUseCount:	(SWAPCHAIN_IMAGE_COUNT - 1).into(),
				..std::mem::zeroed()
			}, cpu_pool.as_mut_ptr())));

			let cpu_pool = cpu_pool.assume_init();

			let mut buffer = std::mem::MaybeUninit::uninit();
			let mut allocation = std::mem::MaybeUninit::uninit();
			let mut allocation_info = std::mem::MaybeUninit::uninit();

			vk_try!(crate::vk::Result::from_raw(vk_mem::vmaCreateBuffer(allocator, &crate::vk::BufferCreateInfo {
				sharing_mode:	crate::vk::SharingMode::EXCLUSIVE,
				usage:			crate::vk::BufferUsageFlags::TRANSFER_SRC,
				size:			device.find_index(crate::vk::MemoryPropertyFlags::HOST_VISIBLE)
									.ok_or(crate::vk::Result::ERROR_FEATURE_NOT_PRESENT)?.1 / 64,
				..ConstDefault::DEFAULT
			} as *const _ as _,
			&vk_mem::VmaAllocationCreateInfo {
				flags:	vma::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_MAPPED_BIT,
				pool:	cpu_pool,
				..std::mem::zeroed()
			}, buffer.as_mut_ptr(), allocation.as_mut_ptr(), allocation_info.as_mut_ptr())));

			Ok(Self {
				buffer: 		std::mem::transmute(buffer.assume_init()),
				allocation: 		allocation.assume_init(), 
				allocation_info:	allocation_info.assume_init(),
				device, allocator, gpu_pool, cpu_pool
			})
		}
	}
}

impl Drop for BasicAllocator {
	fn drop(&mut self) {
		unsafe {
			vk_mem::vmaDestroyBuffer(self.allocator, std::mem::transmute(self.buffer), self.allocation);
			vk_mem::vmaDestroyPool(self.allocator, self.cpu_pool);
			vk_mem::vmaDestroyPool(self.allocator, self.gpu_pool);
			vk_mem::vmaDestroyAllocator(self.allocator)
		}
	}
}

unsafe impl Send for BasicAllocator { }

impl BasicAllocator {
	pub fn create_image(&self, create_info: &crate::vk::ImageCreateInfo) -> VkResult<(crate::vk::Image, Allocation)> {
		unsafe {
		let mut image = std::mem::MaybeUninit::uninit();
		let mut allocation = std::mem::MaybeUninit::uninit();
		vk_try!(crate::vk::Result::from_raw(vk_mem::vmaCreateImage(self.allocator, create_info as *const _ as _, &vk_mem::VmaAllocationCreateInfo {
			pool:	self.gpu_pool,
			..std::mem::zeroed()
		}, image.as_mut_ptr(), allocation.as_mut_ptr(), 0 as _)));
		Ok((std::mem::transmute(image.assume_init()), allocation.assume_init()))
		}
	}

	pub fn create_buffer(&self, create_info: &crate::vk::BufferCreateInfo) -> VkResult<(crate::vk::Buffer, Allocation)> {
		println!("create_buffer {:?}", create_info.size);
		unsafe {
		let mut buffer = std::mem::MaybeUninit::uninit();
		let mut allocation = std::mem::MaybeUninit::uninit();
		vk_try!(crate::vk::Result::from_raw(vk_mem::vmaCreateBuffer(self.allocator, create_info as *const _ as _, &vk_mem::VmaAllocationCreateInfo {
			pool:	self.gpu_pool,
			..std::mem::zeroed()
		}, buffer.as_mut_ptr(), allocation.as_mut_ptr(), 0 as _)));
		Ok((std::mem::transmute(buffer.assume_init()), allocation.assume_init()))
		}
	}

	pub fn upload_buffer(&self) -> crate::vk::Buffer {
		self.buffer
	}

/*
	vk_mem::vmaFlushAllocation(self.allocator, allocation, 0, crate::vk::WHOLE_SIZE);
	self.device.procs.cmd_copy_buffer(cmd_buffer, buffer, gpu_buffer, 1, &crate::vk::BufferCopy {
		size: create_info.size,
		..ConstDefault::DEFAULT
	});
*/
	/*pub fn flush(&mut self) -> VkResult<()> {
		unsafe {
		vk_try!(self.device.procs.flush_mapped_memory_ranges(self.device.device,
			self.mapped_ranges.len().try_into().unwrap?(), self.mapped_ranges.as_ptr()));
		}
		self.mapped_ranges.clear();
		self.ring = xalloc::ring::Ring::new(self.allocation_info.size);
		Ok(())
	}*/

	pub fn free(&mut self, allocations: &mut [Allocation]) {
		unsafe { vk_mem::vmaFreeMemoryPages(self.allocator, allocations.len().try_into().unwrap(), allocations.as_mut_ptr()) }
	}
}

struct ImageEntry {
	texture:	Texture,
	allocator:	range_alloc::RangeAllocator<u32>,
}

impl ImageEntry {
	fn new(allocator: &mut BasicAllocator, create_info: &crate::vk::ImageCreateInfo) -> VkResult<Self> {
		Ok(Self {
			texture:	Texture::new(allocator, create_info)?,
			allocator:	range_alloc::RangeAllocator::new(0..create_info.array_layers),
		})
	}
}

const ARRAY_LAYERS: u8 = 4;

#[derive(Debug, Copy, Clone)]
pub struct TextureCreateInfo {
	pub format:	crate::vk::Format,
	pub extent:	crate::vk::Extent2D,
}

impl TextureCreateInfo {
	pub fn into_image_create_info(&self) -> crate::vk::ImageCreateInfo {
		crate::vk::ImageCreateInfo {
			image_type:		crate::vk::ImageType::TYPE_2D,
			format:			self.format,
			extent:			crate::vk::Extent3D {
				width:	self.extent.width,
				height:	self.extent.height,
				depth:	1
			},
			mip_levels:		((self.extent.width as f32).log2().floor() as u32 + 1)
								.min((self.extent.height as f32).log2().floor() as u32 + 1),
			array_layers:	1,
			samples:		crate::vk::SampleCountFlags::TYPE_1,
			usage:			crate::vk::ImageUsageFlags::TRANSFER_DST | crate::vk::ImageUsageFlags::SAMPLED,
			..ConstDefault::DEFAULT
		}
	}
}

#[derive(Debug)]
pub struct TextureAllocation {
	pub image:				crate::vk::Image,
	pub image_view:			crate::vk::ImageView,
	pub key:				ImageKey,
	pub descriptor_index:	u32,
	pub range:				std::ops::Range<u32>,
}

unsafe impl Send for TextureAllocation { }

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub struct ImageKey {
	flags:		crate::vk::ImageCreateFlags,
	format:		crate::vk::Format,
	extent:		crate::vk::Extent3D,
	mip_levels:	u32,
	usage:		crate::vk::ImageUsageFlags,
}

impl From<&crate::vk::ImageCreateInfo> for ImageKey {
	fn from(image_info: &crate::vk::ImageCreateInfo) -> Self {
		Self {
			flags:		image_info.flags,
			format:		image_info.format,
			extent:		image_info.extent,
			mip_levels:	image_info.mip_levels,
			usage:		image_info.usage,
		}
	}
}

pub struct Allocator {
	pub allocator:			BasicAllocator,
	image_map:				std::collections::HashMap<ImageKey, std::collections::HashSet<usize>>,
	descriptor_index_pool:	index_pool::IndexPool,
	descriptor_mirror:		Vec<ImageEntry>,
}

impl From<BasicAllocator> for Allocator {
	fn from(allocator: BasicAllocator) -> Self {
		Self {
			image_map: 				Default::default(),
			descriptor_mirror: 		Default::default(),
			descriptor_index_pool:	Default::default(),
			allocator
		}
	}
}

impl Drop for Allocator {
	fn drop(&mut self) {
		for index in self.descriptor_index_pool.all_indices() {
			let image_entry = unsafe { self.descriptor_mirror.get_unchecked_mut(index) };
			image_entry.texture.destroy(&mut self.allocator)
		}
	}
}

impl std::ops::Deref for Allocator {
	type Target = BasicAllocator;

	fn deref(&self) -> &Self::Target {
		&self.allocator
	}
}

impl std::ops::DerefMut for Allocator {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.allocator
	}
}

impl Allocator {
	pub fn allocate_texture(&mut self, create_info: &crate::vk::ImageCreateInfo, is_dedicated: bool) -> VkResult<(TextureAllocation, bool)> {
		let entry = self.image_map.entry(create_info.into()).or_insert_with(Default::default);
		for index in entry.iter() {
			let image_entry = unsafe { self.descriptor_mirror.get_unchecked_mut(*index) };
			if let Ok(range) = image_entry.allocator.allocate_range(create_info.array_layers) {
				return Ok((TextureAllocation {
					image:				image_entry.texture.image,
					image_view:			image_entry.texture.image_view,
					key:				create_info.into(),
					descriptor_index:	(*index).try_into().unwrap(),
					range,
				}, false))
			}
		}
		let descriptor_index = self.descriptor_index_pool.new_id();
		entry.insert(descriptor_index);

		let mut image_entry = if !is_dedicated && create_info.array_layers < u32::from(ARRAY_LAYERS) {
			let mut create_info_copy = *create_info;
			create_info_copy.array_layers = ARRAY_LAYERS.into();
			ImageEntry::new(&mut self.allocator, &create_info_copy)?
		} else {
			ImageEntry::new(&mut self.allocator, &create_info)?
		};

		image_entry.allocator.allocate_range(create_info.array_layers).unwrap();
		let image = image_entry.texture.image;
		let image_view = image_entry.texture.image_view;
		if let Some(descriptor) = self.descriptor_mirror.get_mut(descriptor_index) {
			*descriptor = image_entry
		} else {
			debug_assert!(self.descriptor_mirror.len() == descriptor_index);
			self.descriptor_mirror.push(image_entry)
		}
		Ok((TextureAllocation {
			key:				create_info.into(),
			range:				0..create_info.array_layers,
			descriptor_index:	descriptor_index.try_into().unwrap(),
			image, image_view
		}, true))
	}

	pub fn deallocate_texture(&mut self, allocation: TextureAllocation) {
		let image_entry = unsafe { self.descriptor_mirror.get_unchecked_mut(usize::try_from(allocation.descriptor_index).unwrap()) };
		image_entry.allocator.free_range(allocation.range);
		if image_entry.allocator.is_empty() {
			println!("Image Entry {} Destroyed", allocation.descriptor_index);
			self.descriptor_index_pool.return_id((allocation.descriptor_index).try_into().unwrap()).unwrap();
			image_entry.texture.destroy(&mut self.allocator);
			if self.descriptor_mirror.len() == usize::try_from(allocation.descriptor_index).unwrap() {
				self.descriptor_mirror.pop();
			}
			let format_entry = self.image_map.get_mut(&allocation.key).unwrap();
			format_entry.remove(&(allocation.descriptor_index).try_into().unwrap());
		}
	}

	pub fn update_descriptor_set(&mut self, descriptor_set: crate::vk::DescriptorSet) {
		let mut image_infos = Vec::new();
		let mut descriptor_set_writes = Vec::new();
		for index in self.descriptor_index_pool.all_indices() {
			let image_entry = unsafe { self.descriptor_mirror.get_unchecked(index) };
			image_infos.push(crate::vk::DescriptorImageInfo {
				image_layout:	crate::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
				image_view:		image_entry.texture.image_view,
				..ConstDefault::DEFAULT
			});
		}
		for (index, image_info) in self.descriptor_index_pool.all_indices().zip(&image_infos) {
			descriptor_set_writes.push(crate::vk::WriteDescriptorSet {
				dst_set:			descriptor_set,
				dst_array_element:	index.try_into().unwrap(),
				descriptor_count:	1,
				descriptor_type:	crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
				p_image_info:		image_info,
				..ConstDefault::DEFAULT
			})
		}
		unsafe { self.device.procs.update_descriptor_sets(self.device.device,
				descriptor_set_writes.len().try_into().unwrap(), descriptor_set_writes.as_ptr(), 0, 0 as _) };
			
	}
}
