use core::ops::Range;
use super::*;

#[derive(Debug)]
pub struct SubBuffer<T> {
	range:		Range<crate::vk::DeviceSize>,
	phantom:	std::marker::PhantomData<T>,
}

impl<T> SubBuffer<T> {
	pub fn len(&self) -> crate::vk::DeviceSize {
		self.range.end - self.range.start
	}

	pub fn bytes_len(&self) -> crate::vk::DeviceSize {
		self.len() * crate::vk::DeviceSize::try_from(std::mem::size_of::<T>()).unwrap()
	}

	pub fn range(&self) -> Range<crate::vk::DeviceSize> {
		println!("{:?}", self.range);
		self.range.clone()
	}

	pub fn bytes_range(&self) -> Range<crate::vk::DeviceSize> {
		self.range.start * crate::vk::DeviceSize::try_from(std::mem::size_of::<T>()).unwrap()..
		self.range.end * crate::vk::DeviceSize::try_from(std::mem::size_of::<T>()).unwrap()
	}
}

pub struct Buffer<T> {
	pub buffer:			crate::vk::Buffer,
	allocation:			Allocation,
	range_allocator:	range_alloc::RangeAllocator<crate::vk::DeviceSize>,
	phantom:			std::marker::PhantomData<T>,
}

impl<T> Buffer<T> {
	pub fn new(allocator: &mut BasicAllocator, mut create_info: crate::vk::BufferCreateInfo) -> VkResult<Self> {
		let range_allocator = range_alloc::RangeAllocator::new(0..create_info.size);
		create_info.size *= crate::vk::DeviceSize::try_from(std::mem::size_of::<T>()).unwrap();
		let (buffer, allocation) = allocator.create_buffer(&create_info)?;
		Ok(Self { buffer, allocation, range_allocator, phantom: Default::default() })
	}

	pub fn allocate(&mut self, size: crate::vk::DeviceSize) -> Option<SubBuffer<T>> {
		Some(SubBuffer {
			range:		self.range_allocator.allocate_range(size).ok()?,
			phantom:	Default::default(),
		})
	}

	pub fn deallocate(&mut self, subbuffer: SubBuffer<T>) {
		self.range_allocator.free_range(subbuffer.range)
	}

	pub fn destroy(&mut self, allocator: &mut BasicAllocator) {
		allocator.free(std::slice::from_mut(&mut self.allocation));
		unsafe { allocator.device.procs.destroy_buffer(allocator.device.device, self.buffer, 0 as _) };
	}
}

impl<T> Drop for Buffer<T> {
	fn drop(&mut self) {
		if !self.range_allocator.is_empty() {
			println!("Buffer not empty!");
		}
	}
}

unsafe impl<T> Send for Buffer<T> {}