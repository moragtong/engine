#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec2 out_uv;

void main() {
	out_uv = vec2(gl_VertexIndex & 1, gl_VertexIndex >> 1);
	gl_Position = vec4(out_uv.x-1.0, out_uv.y-1.0, 0.0, 1.0);
}