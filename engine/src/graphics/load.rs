use super::*;

use std::convert::{ TryFrom, TryInto };

pub(in crate::graphics) struct Loader {
	pub uploader:			Uploader,
	pub upload:				Upload,
}

impl Loader {
	pub fn new(uploader: Uploader) -> Self {
		Self {
			upload:	Default::default(),
			uploader
		}
	}

	pub async fn submit(&mut self, semaphore: crate::vk::Semaphore) -> VkResult<UploadInfo> {
		self.uploader.submit(std::mem::take(&mut self.upload), semaphore).await
	}

	pub async fn allocate_upload_range_bytes(&mut self, size: crate::vk::DeviceSize, align: crate::vk::DeviceSize, semaphore: crate::vk::Semaphore) -> VkResult<UploadRange<u8>> {
		match self.uploader.allocate_upload_range_bytes(&mut self.upload, size, align) {
			Err((upload, range)) => {
				self.submit(semaphore).await?;
				self.upload = upload;
				Ok(range)
			}
			Ok(ret) => Ok(ret)
		}
	}

	pub async fn allocate_upload_range<T>(&mut self, size: crate::vk::DeviceSize, semaphore: crate::vk::Semaphore) -> VkResult<UploadRange<T>> {
		let raw = self.allocate_upload_range_bytes(size * crate::vk::DeviceSize::try_from(std::mem::size_of::<T>()).unwrap(),
			std::mem::align_of::<T>().try_into().unwrap(), semaphore).await?;
		Ok(UploadRange {
			offset:	raw.offset,
			data:	raw.data as _,
			size:	size.try_into().unwrap(),
		})
	}
}