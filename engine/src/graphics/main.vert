#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0)	in	mat4 view_model;
layout(location = 4)	in	mat4 proj_view_model;
layout(location = 8)	in	mat4 light_model;
layout(location = 12)	in	vec3 texture_layers;
layout(location = 13)	in	uvec3 descriptor_indices;

layout(location = 14)	in	vec3 position;
layout(location = 15)	in	vec3 normal;
layout(location = 16)	in	vec3 tangent;
layout(location = 17)	in	vec2 uv;

layout(location = 0)	out	vec3 out_position;
layout(location = 1)	out	vec3 out_normal;
layout(location = 2)	out	vec3 out_tangent;
layout(location = 3)	out	vec2 out_uv;
layout(location = 4)	out	flat vec3 out_texture_layers;
layout(location = 5)	out	flat uvec3 out_descriptor_indices;
layout(location = 6)	out	vec3 out_shadow_position;

void main() {
	out_position = (view_model * vec4(position, 1.0)).xyz;
	out_normal = mat3(view_model) * normal;
	out_tangent = mat3(view_model) * tangent;
	out_uv = uv;
	out_texture_layers = texture_layers;
	out_descriptor_indices = descriptor_indices;
	vec4 shadow_position = light_model * vec4(position, 1.0);

	out_shadow_position = vec3(shadow_position / shadow_position.w);
	gl_Position = proj_view_model * vec4(position, 1.0);
}