use super::*;
static SHADOW_VERT: &[u32] = include_glsl!("src/graphics/shadow.vert");
const SHADOW_FORMAT: crate::vk::Format = crate::vk::Format::D32_SFLOAT;

pub struct ShadowRenderer {
	resolution:			u32,
	shadow_vert:		crate::vk::ShaderModule,
	allocation:			device::TextureAllocation,
	pipeline_layout:	crate::vk::PipelineLayout,
	render_pass:		crate::vk::RenderPass,
	pipeline:			crate::vk::Pipeline,
}

pub struct ShadowRendererFrame {
	depth_view:		crate::vk::ImageView,
	framebuffer:	crate::vk::Framebuffer,
}

impl ShadowRendererFrame {
	pub fn destroy(&mut self, device: &device::Device) {
		unsafe {
			device.procs.destroy_framebuffer(device.device, self.framebuffer, 0 as _);
			device.procs.destroy_image_view(device.device, self.depth_view, 0 as _);
		}
	}
}

impl ShadowRenderer {
	pub fn new(
		allocator:	&mut device::Allocator,
		resolution:	u32,	
	) -> VkResult<Self> {
		let extent = crate::vk::Extent2D { width: resolution, height: resolution };
		let device = allocator.device.clone();
		unsafe {
		let shadow_vert = device.create_shader_module(SHADOW_VERT)?;
		let allocation = allocator.allocate_texture(&crate::vk::ImageCreateInfo {
			image_type:		crate::vk::ImageType::TYPE_2D,
			format:			SHADOW_FORMAT,
			mip_levels:		1,
			array_layers:	SWAPCHAIN_IMAGE_COUNT as _,
			samples:		crate::vk::SampleCountFlags::TYPE_1,
			initial_layout:	crate::vk::ImageLayout::UNDEFINED,
			usage:			crate::vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT |
							crate::vk::ImageUsageFlags::SAMPLED,
			sharing_mode:	crate::vk::SharingMode::EXCLUSIVE,
			extent:			crate::vk::Extent3D {
				width:	resolution,
				height:	resolution,
				depth:	1,
			},
			..ConstDefault::DEFAULT
		}, true)?.0;
		debug_assert!(allocation.range.start == 0);

		let mut render_pass = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_render_pass(device.device, &crate::vk::RenderPassCreateInfo {
			attachment_count:	1,
			p_attachments:		&crate::vk::AttachmentDescription {
				format:				SHADOW_FORMAT,
				samples:			crate::vk::SampleCountFlags::TYPE_1,
				load_op:			crate::vk::AttachmentLoadOp::CLEAR,
				store_op:			crate::vk::AttachmentStoreOp::STORE,
				stencil_load_op:	crate::vk::AttachmentLoadOp::DONT_CARE,
				stencil_store_op:	crate::vk::AttachmentStoreOp::DONT_CARE,
				initial_layout:		crate::vk::ImageLayout::UNDEFINED,
				final_layout:		crate::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
				..ConstDefault::DEFAULT
			},
			subpass_count:		1,
			p_subpasses:		&crate::vk::SubpassDescription {
				p_depth_stencil_attachment:	&crate::vk::AttachmentReference {
					attachment:	0,
					layout:		crate::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
				},
				..ConstDefault::DEFAULT
			},
			..ConstDefault::DEFAULT
		}, 0 as _, render_pass.as_mut_ptr()));
		let render_pass = render_pass.assume_init();

		let mut pipeline_layout = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_pipeline_layout(device.device, &ConstDefault::DEFAULT, 0 as _, pipeline_layout.as_mut_ptr()));
		let pipeline_layout = pipeline_layout.assume_init();

		let mut pipeline = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_graphics_pipelines(device.device, ConstDefault::DEFAULT,
			1, &crate::vk::GraphicsPipelineCreateInfo {
			stage_count:			1,
			p_stages:				&crate::vk::PipelineShaderStageCreateInfo {
				stage:	crate::vk::ShaderStageFlags::VERTEX,
				module:	shadow_vert,
				p_name:	cstr!("main"),
				..ConstDefault::DEFAULT
			},
			layout:					pipeline_layout,
			p_vertex_input_state:	&per_vertex::SHADOW_VERTEX_INPUT_STATE,
			p_depth_stencil_state:	&crate::vk::PipelineDepthStencilStateCreateInfo {
				depth_test_enable:	crate::vk::TRUE,
				depth_write_enable:	crate::vk::TRUE,
				depth_compare_op:	crate::vk::CompareOp::LESS,
				..ConstDefault::DEFAULT
			},
			p_input_assembly_state:	&crate::vk::PipelineInputAssemblyStateCreateInfo {
				topology:	crate::vk::PrimitiveTopology::TRIANGLE_LIST_WITH_ADJACENCY,
				..ConstDefault::DEFAULT
			},
			p_viewport_state:		&crate::vk::PipelineViewportStateCreateInfo {
				viewport_count:	1,
				p_viewports:	&crate::vk::Viewport {
					width:		resolution as _,
					height:		resolution as _,
					max_depth:	1.,
					..ConstDefault::DEFAULT
				},
				scissor_count:	1,
				p_scissors:		&crate::vk::Rect2D {
					extent, ..ConstDefault::DEFAULT
				},
				..ConstDefault::DEFAULT
			},
			p_rasterization_state:	&crate::vk::PipelineRasterizationStateCreateInfo {
				//depth_clamp_enable:	1,
				polygon_mode:		crate::vk::PolygonMode::FILL,
				cull_mode:			crate::vk::CullModeFlags::FRONT,
				front_face:			crate::vk::FrontFace::CLOCKWISE,
				line_width:			1.,
				..ConstDefault::DEFAULT
			},
			p_multisample_state:	&crate::vk::PipelineMultisampleStateCreateInfo {
				rasterization_samples:	crate::vk::SampleCountFlags::TYPE_1,
				..ConstDefault::DEFAULT
			},
			render_pass, ..ConstDefault::DEFAULT
		}, 0 as _, pipeline.as_mut_ptr()));

		Ok(Self {
			pipeline:		pipeline.assume_init(),
			resolution, shadow_vert, allocation, pipeline_layout, render_pass
		})
		}
	}

	pub fn create_frame(&self, device: &device::Device, i: u8) -> Result<ShadowRendererFrame, EngineError> {
		unsafe {
			let mut depth_view = std::mem::MaybeUninit::uninit();
			vk_try!(device.procs.create_image_view(device.device, &crate::vk::ImageViewCreateInfo {
				view_type:			crate::vk::ImageViewType::TYPE_2D,
				image:				self.allocation.image,
				format:				SHADOW_FORMAT,
				subresource_range:	crate::vk::ImageSubresourceRange {
					aspect_mask:		crate::vk::ImageAspectFlags::DEPTH,
					level_count:		1,
					base_array_layer:	i.into(),
					layer_count:		1,
					..ConstDefault::DEFAULT
				},
				..ConstDefault::DEFAULT
			}, 0 as _, depth_view.as_mut_ptr()));
			let depth_view = depth_view.assume_init();

			let mut framebuffer = std::mem::MaybeUninit::uninit();
			vk_try!(device.procs.create_framebuffer(device.device, &crate::vk::FramebufferCreateInfo {	
				render_pass:		self.render_pass,
				attachment_count:	1,
				p_attachments:		&depth_view,
				width:				self.resolution,
				height:				self.resolution,
				layers:				1,
				..ConstDefault::DEFAULT
			}, 0 as _, framebuffer.as_mut_ptr()));

			Ok(ShadowRendererFrame { depth_view, framebuffer: framebuffer.assume_init() })
		}
	}

	pub fn render(&self,
		device:				&device::Device,
		cmd_buffer:			crate::vk::CommandBuffer,
		frame:				&ShadowRendererFrame,
		i:					usize,
		render_resources:	&RenderResources,
		object_count:		usize,
	) {
		unsafe {
		device.procs.cmd_begin_render_pass(cmd_buffer, &crate::vk::RenderPassBeginInfo {
			render_pass:		self.render_pass,
			framebuffer:		frame.framebuffer,
			render_area:		crate::vk::Rect2D {
				extent: crate::vk::Extent2D {
					width:	self.resolution,
					height:	self.resolution,
				},
				..ConstDefault::DEFAULT
			},
			clear_value_count:	1,
			p_clear_values:		&crate::vk::ClearValue {
				depth_stencil:	crate::vk::ClearDepthStencilValue {
					depth:		1.,
					stencil:	0,
				}
			},
			..ConstDefault::DEFAULT
		}, crate::vk::SubpassContents::INLINE);

			let buffers = [
				render_resources.per_instance_buffer,
				render_resources.vertex_buffer,
			];

			let offsets = [(i * std::mem::size_of::<PerInstance>()) as crate::vk::DeviceSize * INSTANCE_COUNT as crate::vk::DeviceSize, 0];

			device.procs.cmd_bind_pipeline(cmd_buffer, crate::vk::PipelineBindPoint::GRAPHICS, self.pipeline);
			device.procs.cmd_bind_vertex_buffers(cmd_buffer, 0, per_vertex::VERTEX_BINDING_DESCRIPTIONS.len() as _, buffers.as_ptr(), offsets.as_ptr());
			device.procs.cmd_bind_index_buffer(cmd_buffer, render_resources.index_buffer, 0, crate::vk::IndexType::UINT32);
			if object_count != 0 {
				device.procs.cmd_draw_indexed_indirect(cmd_buffer, buffers[0],
					offsets[0], object_count as _, std::mem::size_of::<PerInstance>() as _);
			}
		device.procs.cmd_end_render_pass(cmd_buffer);
		}
	}

	pub fn destroy(&mut self, allocator: &mut device::Allocator) {
		unsafe {
			allocator.device.procs.destroy_pipeline(allocator.device.device, self.pipeline, 0 as _);
			allocator.device.procs.destroy_render_pass(allocator.device.device, self.render_pass, 0 as _);
			allocator.device.procs.destroy_pipeline_layout(allocator.device.device, self.pipeline_layout, 0 as _);
			//allocator.deallocate_texture(self.allocation);
			allocator.device.procs.destroy_shader_module(allocator.device.device, self.shadow_vert, 0 as _);
		}
	}
}
