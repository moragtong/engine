#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(triangles_adjacency) in;
layout(line_strip, max_vertices = 6) out;

layout(location = 0) in vec3 position[];

float direction(uint x, uint y, uint z) {
	vec3 u = position[y] - position[x];
	vec3 v = position[z] - position[y];

	return dot(position[y], cross(u, v));
}

void draw(uint x, uint y, uint z) {
	if (direction(x, y, z) > 0.0) {
		gl_Position = gl_in[x].gl_Position;
		EmitVertex();

		gl_Position = gl_in[z].gl_Position;
		EmitVertex();

		EndPrimitive();
	}
}

void main() {
	if (direction(0, 2, 4) <= 0.0) {
		draw(0, 1, 2);
		draw(2, 3, 4);
		draw(4, 5, 0);
	}
}