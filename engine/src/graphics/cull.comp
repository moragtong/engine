#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_nonuniform_qualifier : enable

layout(local_size_x = 32, local_size_y = 1, local_size_z = 1) in;

float when_eq(float x, float y) {
  return 1.0 - abs(sign(x - y));
}

float when_neq(float x, float y) {
  return abs(sign(x - y));
}

float when_gt(float x, float y) {
  return max(sign(x - y), 0.0);
}

float when_lt(float x, float y) {
  return max(sign(y - x), 0.0);
}

float when_ge(float x, float y) {
  return 1.0 - when_lt(x, y);
}

float when_le(float x, float y) {
  return 1.0 - when_gt(x, y);
}

#define TEXTURE_COUNT 3

layout(push_constant) uniform UniformData {
	mat4	proj_view;
	mat4	view;
	mat4	light;
	uint	max;
} uniform_data;

struct PerInstance {
	uint	index_count;
	uint	instance_count;
	uint	first_index;
	int		vertex_offset;
	uint	first_instance;
	uint[3] _pad;
	
	mat4	model; // view_model
	mat4	proj_view_model;
	mat4 	light_model;
	vec4	texture_layers;
	uvec4	descriptor_indices;
};

layout(std430, binding = 0) buffer Instances {
	PerInstance[] instances;
} instances;

void main() {
	if (gl_LocalInvocationID.x >= uniform_data.max) {
		return;
	}
	mat4 proj_view_model = uniform_data.proj_view * instances.instances[gl_LocalInvocationID.x].model;

	instances.instances[gl_LocalInvocationID.x].proj_view_model = proj_view_model;
	instances.instances[gl_LocalInvocationID.x].light_model = uniform_data.light * instances.instances[gl_LocalInvocationID.x].model;
	instances.instances[gl_LocalInvocationID.x].model = uniform_data.view * instances.instances[gl_LocalInvocationID.x].model;

}
