use super::*;

use std::iter::FromIterator;

const CLEAR_VALUES: [crate::vk::ClearValue; ATTACHMENT_COUNT as _] = [
	crate::vk::ClearValue {
		color:	crate::vk::ClearColorValue {
			float32:	[0., 0., 0., 1.]
		}
	},
	crate::vk::ClearValue {
		depth_stencil:	crate::vk::ClearDepthStencilValue {
			depth:		1.,
			stencil:	0,
		}
	},
];

pub struct PostProcess {
	pub vertex:					&'static [u32],
	pub geometry:				Option<&'static [u32]>,
	pub fragment:				Option<&'static [u32]>,
	pub depth_stencil_state:	crate::vk::PipelineDepthStencilStateCreateInfo,
}

impl PostProcess {
	pub fn depth_image_layout(&self) -> crate::vk::ImageLayout {
		if self.depth_stencil_state.depth_test_enable != 0 && self.depth_stencil_state.depth_write_enable == 0 {
			crate::vk::ImageLayout::DEPTH_STENCIL_READ_ONLY_OPTIMAL
		} else {
			crate::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		}
	}

	pub fn access_flags(&self) -> crate::vk::AccessFlags {
		if self.depth_stencil_state.depth_test_enable != 0 {
			crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ | if self.depth_stencil_state.depth_write_enable == 0 {
				ConstDefault::DEFAULT
			} else {
				crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE
			}
		} else {
			ConstDefault::DEFAULT
		}
	}

	pub fn stage_flags(&self) -> crate::vk::PipelineStageFlags {
		if self.access_flags() != ConstDefault::DEFAULT {
			crate::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS
		} else {
			ConstDefault::DEFAULT
		}
	}
}

struct ShaderMap<'d> {
	device:	&'d device::Device,
	map:	std::collections::HashMap<*const u32, crate::vk::ShaderModule>
}

impl<'d> ShaderMap<'d> {
	pub fn new(device: &'d device::Device) -> Self {
		Self { device, map: Default::default() }
	}

	pub fn insert(&mut self, code: &'static [u32], stage: crate::vk::ShaderStageFlags)
	-> VkResult<crate::vk::PipelineShaderStageCreateInfo> {
		Ok(crate::vk::PipelineShaderStageCreateInfo {
			module:	*match self.map.entry(code.as_ptr()) {
				std::collections::hash_map::Entry::Occupied(entry) => entry.into_mut(),
				std::collections::hash_map::Entry::Vacant(entry) =>
					entry.insert(unsafe { self.device.create_shader_module(code) }?)
			},
			p_name:	cstr!("main"),
			stage, ..ConstDefault::DEFAULT
		})
	}
}

pub struct RendererFrame {
	swapchain_view:	crate::vk::ImageView,
	depth_texture:	Texture,
	framebuffer:	crate::vk::Framebuffer,
}

impl RendererFrame {
	pub fn destroy(&mut self, allocator: &mut device::Allocator) {
		unsafe {
			allocator.device.procs.destroy_framebuffer(allocator.device.device, self.framebuffer, 0 as _);
			allocator.device.procs.destroy_image_view(allocator.device.device, self.swapchain_view, 0 as _);
		}
		self.depth_texture.destroy(allocator);
	}
}

pub struct Renderer {
	extent:				crate::vk::Extent2D,
	shaders:			Vec<crate::vk::ShaderModule>,
	pub(in crate::graphics) set_layout:			crate::vk::DescriptorSetLayout,
	pipeline_layout:	crate::vk::PipelineLayout,
	render_pass:		crate::vk::RenderPass,
	pipelines:			Vec<crate::vk::Pipeline>,
	sampler:			crate::vk::Sampler,
	shadow_sampler:		crate::vk::Sampler,
}

impl Renderer {
	pub fn new(
		device:			&device::Device,
		extent:			crate::vk::Extent2D,
		format:			crate::vk::Format,
		post_process:	&[PostProcess],
	) -> Result<Self, EngineError> {
		println!("{:?}", std::mem::size_of::<per_vertex::PerInstance>());
		unsafe {
		let mut sampler = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_sampler(device.device, &crate::vk::SamplerCreateInfo {
			mag_filter:			crate::vk::Filter::LINEAR,
			min_filter:			crate::vk::Filter::LINEAR,
			mipmap_mode:		crate::vk::SamplerMipmapMode::LINEAR,
			anisotropy_enable:	crate::vk::TRUE,
			max_anisotropy:		16.,
			max_lod:			std::f32::MAX,
			border_color:		crate::vk::BorderColor::INT_OPAQUE_BLACK,
			..ConstDefault::DEFAULT
		}, 0 as _, sampler.as_mut_ptr()));

		let sampler = sampler.assume_init();

		let mut shadow_sampler = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_sampler(device.device, &crate::vk::SamplerCreateInfo {
			address_mode_u:				crate::vk::SamplerAddressMode::CLAMP_TO_BORDER,
			address_mode_v:				crate::vk::SamplerAddressMode::CLAMP_TO_BORDER,
			border_color:				crate::vk::BorderColor::FLOAT_OPAQUE_WHITE,
			..ConstDefault::DEFAULT
		}, 0 as _, shadow_sampler.as_mut_ptr()));

		let shadow_sampler = shadow_sampler.assume_init();

		let mut set_layout = std::mem::MaybeUninit::uninit();
		{
			let mut samplers = std::mem::MaybeUninit::<[crate::vk::Sampler; MAX_TEXTURE_COUNT as usize + 1]>::uninit();
			let mut psampler = samplers.as_mut_ptr() as *mut crate::vk::Sampler;
			for input in std::iter::once(shadow_sampler).chain(std::iter::repeat(sampler).take(MAX_TEXTURE_COUNT.into())) {
				psampler.write(input);
				psampler = psampler.add(1);
			}
			let samplers = samplers.assume_init();
			vk_try!(device.procs.create_descriptor_set_layout(device.device, &crate::vk::DescriptorSetLayoutCreateInfo {
				binding_count:	1,
				p_bindings:		&crate::vk::DescriptorSetLayoutBinding { // Textures
					binding:				0,
					descriptor_type:		crate::vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
					descriptor_count:		MAX_TEXTURE_COUNT as _,
					stage_flags:			crate::vk::ShaderStageFlags::FRAGMENT,
					p_immutable_samplers:	samplers.as_ptr(),
				},
				p_next:			&crate::vk::DescriptorSetLayoutBindingFlagsCreateInfo {
					binding_count:		1,
					p_binding_flags:	&crate::vk::DescriptorBindingFlags::PARTIALLY_BOUND,
					..ConstDefault::DEFAULT
				} as *const _ as _,
				..ConstDefault::DEFAULT
			}, 0 as _, set_layout.as_mut_ptr()))
		}

		let set_layout = set_layout.assume_init();

		const PUSH_CONSTANTS: [crate::vk::PushConstantRange; 1] = [
			crate::vk::PushConstantRange {
				stage_flags:	crate::vk::ShaderStageFlags::FRAGMENT,
				size:			std::mem::size_of::<Light>() as _,
				..ConstDefault::DEFAULT
			},
		];

		let mut pipeline_layout = std::mem::MaybeUninit::uninit();
		vk_try!(device.procs.create_pipeline_layout(device.device, &crate::vk::PipelineLayoutCreateInfo {
			set_layout_count:			1,
			p_set_layouts:				&set_layout,
			push_constant_range_count:	PUSH_CONSTANTS.len() as _,
			p_push_constant_ranges:		PUSH_CONSTANTS.as_ptr(),
			..ConstDefault::DEFAULT
		}, 0 as _, pipeline_layout.as_mut_ptr()));

		let pipeline_layout = pipeline_layout.assume_init();

		let render_pass = {
			let mut render_pass = std::mem::MaybeUninit::uninit();
			let attachments = [
				crate::vk::AttachmentDescription {
					samples:			crate::vk::SampleCountFlags::TYPE_1,
					load_op:			crate::vk::AttachmentLoadOp::CLEAR,
					store_op:			crate::vk::AttachmentStoreOp::STORE,
					stencil_load_op:	crate::vk::AttachmentLoadOp::DONT_CARE,
					stencil_store_op:	crate::vk::AttachmentStoreOp::DONT_CARE,
					initial_layout:		crate::vk::ImageLayout::UNDEFINED,
					final_layout:		crate::vk::ImageLayout::PRESENT_SRC_KHR,
					format, ..ConstDefault::DEFAULT
				},
				crate::vk::AttachmentDescription {
					format:				DEPTH_FORMAT,
					samples:			crate::vk::SampleCountFlags::TYPE_1,
					load_op:			crate::vk::AttachmentLoadOp::CLEAR,
					store_op:			crate::vk::AttachmentStoreOp::DONT_CARE,
					stencil_load_op:	crate::vk::AttachmentLoadOp::DONT_CARE,
					stencil_store_op:	crate::vk::AttachmentStoreOp::DONT_CARE,
					initial_layout:		crate::vk::ImageLayout::UNDEFINED,
					final_layout:		post_process.last().map(PostProcess::depth_image_layout)
						.unwrap_or(crate::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL),
					..ConstDefault::DEFAULT
				},
			];

			let mut dependencies = Vec::with_capacity(post_process.len() + SUBPASS_COUNT as usize - 1);

			dependencies.push(crate::vk::SubpassDependency {
				src_subpass:		0,
				dst_subpass:		1,
				src_stage_mask:		crate::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT |
										crate::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
				dst_stage_mask:		crate::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT |
										crate::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
				src_access_mask:	crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ |
									crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE |
									crate::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
				dst_access_mask:	crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ |
									crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE |
									crate::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
				dependency_flags:	crate::vk::DependencyFlags::BY_REGION,
			});

			if let Some(post_process) = post_process.first() {
				dependencies.push(crate::vk::SubpassDependency {
					src_subpass:		1,
					dst_subpass:		2,
					src_stage_mask:		crate::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT |
													crate::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
					dst_stage_mask:		crate::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT |
													post_process.stage_flags(),
					src_access_mask:	crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ |
										crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE |
										crate::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
					dst_access_mask:	post_process.access_flags() |
										crate::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
					dependency_flags:	crate::vk::DependencyFlags::BY_REGION,
				})
			}

			/*dependencies.extend((SUBPASS_COUNT as usize..dependencies.len()).zip(post_process.windows(2)).map(|(i, post_process)| {
				let post_process = &*(post_process.as_ptr() as *const [PostProcess; 2]);
				crate::vk::SubpassDependency {
					src_subpass:		i as _,
					dst_subpass:		(i + 1) as _,
					src_stage_mask:		crate::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT |
													post_process[0].stage_flags(),
					dst_stage_mask:		crate::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT |
													post_process[1].stage_flags(),
					src_access_mask:	post_process[0].access_flags() |
										crate::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
					dst_access_mask:	post_process[1].access_flags() |
										crate::vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
					dependency_flags:	crate::vk::DependencyFlags::BY_REGION,
				}
			}));*/

			const COLOR_ATTACHMENT: crate::vk::AttachmentReference = crate::vk::AttachmentReference {
				attachment:	0,
				layout:		crate::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
			};

			const DEPTH_STENCIL_ATTACHMENT_REFERENCE: crate::vk::AttachmentReference = crate::vk::AttachmentReference {
				attachment:	1,
				layout:		crate::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			};

			const DEPTH_STENCIL_READ_ONLY_REFERENCE: crate::vk::AttachmentReference = crate::vk::AttachmentReference {
				attachment:	1,
				layout:		crate::vk::ImageLayout::DEPTH_STENCIL_READ_ONLY_OPTIMAL,
			};

			const FIRST: crate::vk::SubpassDescription = crate::vk::SubpassDescription {
				p_depth_stencil_attachment:	&DEPTH_STENCIL_ATTACHMENT_REFERENCE,
				color_attachment_count:		1,
				p_color_attachments:		&COLOR_ATTACHMENT,
				..ConstDefault::DEFAULT
			};

			let mut subpasses = vec![FIRST; SUBPASS_COUNT as _];
			
			subpasses.extend(post_process.iter().map(|post_process| crate::vk::SubpassDescription {
				p_depth_stencil_attachment:	if post_process.depth_stencil_state.depth_test_enable != 0 {
					if post_process.depth_stencil_state.depth_write_enable != 0 {
						&DEPTH_STENCIL_ATTACHMENT_REFERENCE
					} else {
						&DEPTH_STENCIL_READ_ONLY_REFERENCE
					}
				} else {
					0 as _
				},
				color_attachment_count:		1,
				p_color_attachments:		&COLOR_ATTACHMENT,
				..ConstDefault::DEFAULT
			}));

			vk_try!(device.procs.create_render_pass(device.device, &crate::vk::RenderPassCreateInfo {
				attachment_count:	attachments.len() as _,
				p_attachments:		attachments.as_ptr(),
				subpass_count:		subpasses.len() as _,
				p_subpasses:		subpasses.as_ptr(),
				dependency_count:	dependencies.len() as _,
				p_dependencies:		dependencies.as_ptr(),
				..ConstDefault::DEFAULT
			}, 0 as _, render_pass.as_mut_ptr()));
			render_pass.assume_init()
		};

		let mut pipelines = Vec::with_capacity(SUBPASS_COUNT as usize + post_process.len());
		pipelines.set_len(SUBPASS_COUNT as usize + post_process.len());
		let shaders = {
			let mut shader_map = ShaderMap::new(device);
			let vert = device.create_shader_module(MAIN_VERT)?;
			let frag = device.create_shader_module(MAIN_FRAG)?;

			let quad_vert = device.create_shader_module(QUAD_VERT)?;
			let quad_frag = device.create_shader_module(QUAD_FRAG)?;

			shader_map.map.insert(MAIN_VERT.as_ptr(), vert);
			shader_map.map.insert(MAIN_FRAG.as_ptr(), frag);

			shader_map.map.insert(QUAD_VERT.as_ptr(), quad_vert);
			shader_map.map.insert(QUAD_FRAG.as_ptr(), quad_frag);
			
			let shader_stage_create_info = [
				crate::vk::PipelineShaderStageCreateInfo {
					stage:	crate::vk::ShaderStageFlags::VERTEX,
					module:	vert,
					p_name:	cstr!("main"),
					..ConstDefault::DEFAULT
				},
				crate::vk::PipelineShaderStageCreateInfo {
					stage:	crate::vk::ShaderStageFlags::FRAGMENT,
					module:	frag,
					p_name:	cstr!("main"),
					..ConstDefault::DEFAULT
				},
			];

			let quad_shader_stage_create_info = [
				crate::vk::PipelineShaderStageCreateInfo {
					stage:	crate::vk::ShaderStageFlags::VERTEX,
					module:	quad_vert,
					p_name:	cstr!("main"),
					..ConstDefault::DEFAULT
				},
				crate::vk::PipelineShaderStageCreateInfo {
					stage:	crate::vk::ShaderStageFlags::FRAGMENT,
					module:	quad_frag,
					p_name:	cstr!("main"),
					..ConstDefault::DEFAULT
				},
			];

			const INPUT_ASSEMBLY_STATE: crate::vk::PipelineInputAssemblyStateCreateInfo = crate::vk::PipelineInputAssemblyStateCreateInfo {
				topology:	crate::vk::PrimitiveTopology::TRIANGLE_LIST_WITH_ADJACENCY,
				..ConstDefault::DEFAULT
			};

			let viewport_state = crate::vk::PipelineViewportStateCreateInfo {
				viewport_count:	1,
				p_viewports:	&crate::vk::Viewport {
					width:		extent.width as _,
					height:		extent.height as _,
					max_depth:	1.,
					..ConstDefault::DEFAULT
				},
				scissor_count:	1,
				p_scissors:		&crate::vk::Rect2D {
					extent, ..ConstDefault::DEFAULT
				},
				..ConstDefault::DEFAULT
			};

			const RASTER_STATE: crate::vk::PipelineRasterizationStateCreateInfo = crate::vk::PipelineRasterizationStateCreateInfo {
				polygon_mode:	crate::vk::PolygonMode::FILL,
				cull_mode:		crate::vk::CullModeFlags::BACK,
				front_face:		crate::vk::FrontFace::CLOCKWISE,
				line_width:		1.,
				..ConstDefault::DEFAULT
			};

			const MULTISAMPLE_STATE: crate::vk::PipelineMultisampleStateCreateInfo = crate::vk::PipelineMultisampleStateCreateInfo {
				rasterization_samples:	crate::vk::SampleCountFlags::TYPE_1,
				..ConstDefault::DEFAULT
			};

			let color_blend_state = crate::vk::PipelineColorBlendStateCreateInfo {
				attachment_count:	1,
				p_attachments:		&crate::vk::PipelineColorBlendAttachmentState {
					color_write_mask:	crate::vk::ColorComponentFlags::all(),
					..ConstDefault::DEFAULT
				},
				..ConstDefault::DEFAULT
			};

			let pipeline_create_info = crate::vk::GraphicsPipelineCreateInfo {
				stage_count:			shader_stage_create_info.len() as u32,
				p_stages:				shader_stage_create_info.as_ptr(),
				layout:					pipeline_layout,
				subpass:				1,
				p_vertex_input_state:	&per_vertex::VERTEX_INPUT_STATE,
				p_depth_stencil_state:	&crate::vk::PipelineDepthStencilStateCreateInfo {
					depth_test_enable:	crate::vk::TRUE,
					depth_write_enable:	crate::vk::TRUE,
					depth_compare_op:	crate::vk::CompareOp::LESS,
					..ConstDefault::DEFAULT
				},
				p_input_assembly_state:	&INPUT_ASSEMBLY_STATE,
				p_viewport_state:		&viewport_state,
				p_rasterization_state:	&RASTER_STATE,
				p_multisample_state:	&MULTISAMPLE_STATE,
				p_color_blend_state:	&color_blend_state,
				render_pass, ..ConstDefault::DEFAULT
			};

			let shadow_debug = crate::vk::GraphicsPipelineCreateInfo {
				stage_count:			quad_shader_stage_create_info.len() as u32,
				p_stages:				quad_shader_stage_create_info.as_ptr(),
				subpass:				0,
				p_vertex_input_state:	&per_vertex::QUAD_VERTEX_INPUT_STATE,
				p_input_assembly_state:	&crate::vk::PipelineInputAssemblyStateCreateInfo {
					topology:	crate::vk::PrimitiveTopology::TRIANGLE_STRIP,
					..ConstDefault::DEFAULT
				},
				..pipeline_create_info
			};

			let mut pipeline_create_infos = Vec::with_capacity(SUBPASS_COUNT as usize + post_process.len());
			pipeline_create_infos.push(shadow_debug);
			pipeline_create_infos.push(pipeline_create_info);

			let mut shader_stage_storage = Vec::default();

			let shader_stages: Vec<(usize, usize)> = VkResult::from_iter(post_process.iter().map(|post_process| {
				let index = shader_stage_storage.len();
				shader_stage_storage.push(shader_map.insert(post_process.vertex, crate::vk::ShaderStageFlags::VERTEX)?);
				let mut len = 1;
				if let Some(geometry) = post_process.geometry {
					shader_stage_storage.push(shader_map.insert(geometry, crate::vk::ShaderStageFlags::GEOMETRY)?);
					len += 1;
				}
				if let Some(fragment) = post_process.fragment {
					shader_stage_storage.push(shader_map.insert(fragment, crate::vk::ShaderStageFlags::FRAGMENT)?);
					len += 1;
				}
				Ok((index, len))
			}))?;

			pipeline_create_infos.extend((SUBPASS_COUNT..).zip(post_process).zip(&shader_stages)
			.map(|((i, post_process), (index, len))|
				crate::vk::GraphicsPipelineCreateInfo {
					stage_count:			*len as _,
					p_stages:				shader_stage_storage.get_unchecked(*index),
					layout:					pipeline_layout,
					subpass:				i as _,
					p_vertex_input_state:	&per_vertex::BASIC_VERTEX_INPUT_STATE,
					p_depth_stencil_state:	&post_process.depth_stencil_state,
					..pipeline_create_info
				}
			));

			vk_try!(device.procs.create_graphics_pipelines(device.device, ConstDefault::DEFAULT,
				pipeline_create_infos.len() as _, pipeline_create_infos.as_ptr(), 0 as _, pipelines.as_mut_ptr()));

			Vec::from_iter(shader_map.map.into_iter().map(|(_, value)| value))
		};

		Ok(Self {
			sampler, shadow_sampler, extent, shaders,
			render_pass, set_layout, pipeline_layout, pipelines,
		})
		}
	}

	pub fn create_frame(&mut self, allocator: &mut device::Allocator, swapchain_image: crate::vk::Image, format: crate::vk::Format) -> Result<RendererFrame, EngineError> {
		unsafe {
			let mut swapchain_view = std::mem::MaybeUninit::uninit();
			vk_try!(allocator.device.procs.create_image_view(allocator.device.device, &crate::vk::ImageViewCreateInfo {
				image:				swapchain_image,
				view_type:			crate::vk::ImageViewType::TYPE_2D,
				subresource_range:	crate::vk::ImageSubresourceRange {
					aspect_mask:	crate::vk::ImageAspectFlags::COLOR,
					level_count:	1,
					layer_count:	1,
					..ConstDefault::DEFAULT
				},
				format, ..ConstDefault::DEFAULT
			}, 0 as _, swapchain_view.as_mut_ptr()));

			let swapchain_view = swapchain_view.assume_init();

			let depth_texture = Texture::depth(allocator, self.extent)?;
			let attachments = [swapchain_view, depth_texture.image_view];
			let mut framebuffer = std::mem::MaybeUninit::uninit();
			vk_try!(allocator.device.procs.create_framebuffer(allocator.device.device, &crate::vk::FramebufferCreateInfo {	
				render_pass:		self.render_pass,
				attachment_count:	attachments.len() as _,
				p_attachments:		attachments.as_ptr(),
				width:				self.extent.width,
				height:				self.extent.height,
				layers:				1,
				..ConstDefault::DEFAULT
			}, 0 as _, framebuffer.as_mut_ptr()));
			Ok(RendererFrame { swapchain_view, depth_texture, framebuffer: framebuffer.assume_init() })
		}
	}

	pub fn render(&self,
		device:				&device::Device,
		cmd_buffer:			crate::vk::CommandBuffer,
		frame:				&RendererFrame, // On purpose
		i:					usize,
		render_data:		&RenderData,
		render_resources:	&RenderResources,
		arena_len:			usize,
		ranges:				&[(crate::vk::DeviceSize, u32)])
	-> Result<(), EngineError> {
		debug_assert!(SUBPASS_COUNT as usize + ranges.len() == self.pipelines.len());
		
		unsafe {
			let mut direction = (render_data.camera.view * render_data.light.direction).normalize();
			direction.w = i as _;
			/*device.procs.cmd_pipeline_barrier(cmd_buffer, crate::vk::PipelineStageFlags::LATE_FRAGMENT_TESTS,
				crate::vk::PipelineStageFlags::FRAGMENT_SHADER, crate::vk::DependencyFlags::BY_REGION,
				0, 0 as _, 0, 0 as _, 1, &crate::vk::ImageMemoryBarrier {
				/*src_access_mask:		crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ |
												crate::vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,*/
				dst_access_mask:		crate::vk::AccessFlags::SHADER_READ,
				//old_layout:					crate::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
				new_layout:				crate::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
				src_queue_family_index:	device.graphics_index,
				dst_queue_family_index:	device.graphics_index,
				image:					resource_manager.textures.get_unchecked(0).image,
				subresource_range:		crate::vk::ImageSubresourceRange {
					aspect_mask:		crate::vk::ImageAspectFlags::DEPTH,
					level_count:			crate::vk::REMAINING_MIP_LEVELS,
					base_array_layer:	i as _,
					layer_count:			1,
					..ConstDefault::DEFAULT
				},
				..ConstDefault::DEFAULT
			});*/
			
			device.procs.cmd_bind_descriptor_sets(cmd_buffer, crate::vk::PipelineBindPoint::GRAPHICS,
				self.pipeline_layout, 0, 1, &render_resources.descriptor_set, 0, 0 as _);

			device.procs.cmd_push_constants(cmd_buffer, self.pipeline_layout,
				crate::vk::ShaderStageFlags::FRAGMENT, 0, std::mem::size_of::<Light>() as _,
				&Light { direction } as *const _ as _);
			device.procs.cmd_begin_render_pass(cmd_buffer, &crate::vk::RenderPassBeginInfo {
				render_pass:		self.render_pass,
				framebuffer:		frame.framebuffer,
				render_area:		crate::vk::Rect2D {
					extent: self.extent, ..ConstDefault::DEFAULT
				},
				clear_value_count:	CLEAR_VALUES.len() as _,
				p_clear_values:		CLEAR_VALUES.as_ptr(),
				..ConstDefault::DEFAULT
			}, crate::vk::SubpassContents::INLINE);

				let buffers = [
					render_resources.per_instance_buffer,
					render_resources.vertex_buffer,
				];

				let offsets = [(i * std::mem::size_of::<PerInstance>()) as crate::vk::DeviceSize * INSTANCE_COUNT as crate::vk::DeviceSize, 0];

				device.procs.cmd_bind_pipeline(cmd_buffer, crate::vk::PipelineBindPoint::GRAPHICS, *self.pipelines.get_unchecked(0));
				device.procs.cmd_draw(cmd_buffer, 4, 1, 0, 0);

				device.procs.cmd_next_subpass(cmd_buffer, crate::vk::SubpassContents::INLINE);

				device.procs.cmd_bind_pipeline(cmd_buffer, crate::vk::PipelineBindPoint::GRAPHICS, *self.pipelines.get_unchecked(1));
				device.procs.cmd_bind_vertex_buffers(cmd_buffer, 0, buffers.len() as _, buffers.as_ptr(), offsets.as_ptr());
				device.procs.cmd_bind_index_buffer(cmd_buffer, render_resources.index_buffer, 0, crate::vk::IndexType::UINT32);
				if arena_len != 0 {
					device.procs.cmd_draw_indexed_indirect(cmd_buffer, render_resources.per_instance_buffer,
						offsets[0], arena_len as _, std::mem::size_of::<PerInstance>() as _);
				}

				for (pipeline, range) in self.pipelines.get_unchecked(SUBPASS_COUNT as usize..).iter().zip(ranges) {
					device.procs.cmd_next_subpass(cmd_buffer, crate::vk::SubpassContents::INLINE);
					device.procs.cmd_bind_pipeline(cmd_buffer, crate::vk::PipelineBindPoint::GRAPHICS, *pipeline);
					device.procs.cmd_bind_vertex_buffers(cmd_buffer, 0, per_vertex::VERTEX_BINDING_DESCRIPTIONS.len() as _, buffers.as_ptr(), offsets.as_ptr());
					device.procs.cmd_bind_index_buffer(cmd_buffer, render_resources.index_buffer, 0, crate::vk::IndexType::UINT32);
					if arena_len != 0 {
						device.procs.cmd_draw_indexed_indirect(cmd_buffer, render_resources.per_instance_buffer,
							offsets[0] + range.0 * std::mem::size_of::<PerInstance>() as crate::vk::DeviceSize,
							range.1, std::mem::size_of::<PerInstance>() as _);
					}
				}

			device.procs.cmd_end_render_pass(cmd_buffer);
		}
		Ok(())
	}

	pub fn destroy(&mut self, allocator: &mut device::BasicAllocator) {
		unsafe {
			for pipeline in &self.pipelines {
				allocator.device.procs.destroy_pipeline(allocator.device.device, *pipeline, 0 as _);
			}
			allocator.device.procs.destroy_render_pass(allocator.device.device, self.render_pass, 0 as _);
			allocator.device.procs.destroy_pipeline_layout(allocator.device.device, self.pipeline_layout, 0 as _);
			allocator.device.procs.destroy_descriptor_set_layout(allocator.device.device, self.set_layout, 0 as _);
			allocator.device.procs.destroy_sampler(allocator.device.device, self.sampler, 0 as _);
			allocator.device.procs.destroy_sampler(allocator.device.device, self.shadow_sampler, 0 as _);
			for shader in &self.shaders {
				allocator.device.procs.destroy_shader_module(allocator.device.device, *shader, 0 as _);
			}
		}
	}
}
