#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_nonuniform_qualifier : enable

layout(early_fragment_tests) in;

layout(push_constant) uniform UniformData {
	vec4	light;
} uniform_data;

layout(binding = 0) uniform sampler2DArray texture_sampler[];

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 surface_normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec2 uv;
layout(location = 4) in flat vec3 texture_layers;
layout(location = 5) in flat uvec3 descriptor_indices;
layout(location = 6) in vec3 shadow_map_position;

layout(location = 0) out vec4 out_color;

void main() {
	mat3 tbn = mat3(normalize(tangent), normalize(cross(surface_normal, tangent)), normalize(surface_normal));
	vec3 normal = normalize(tbn * (vec3(texture(texture_sampler[descriptor_indices[2]], vec3(uv, texture_layers[2])).xy, 1.0) * 2.0 - 1.0));
	vec3 viewdir = normalize(-position);
	vec3 halfway = normalize(uniform_data.light.xyz + viewdir);

	float visible = max(sign(texture(texture_sampler[0], vec3(shadow_map_position.xy * 0.5 + 0.5, uniform_data.light.w))[0] - shadow_map_position.z), 0);

	vec3 color = texture(texture_sampler[descriptor_indices[0]], vec3(uv, texture_layers[0])).xyz;

	vec3 diffuse = visible * max(dot(normal, uniform_data.light.xyz), 0.0) * color;
	vec3 ambient = 0.2 * color;
	vec3 specular = visible * pow(max(dot(normal, halfway), 0.0), 16.0) * texture(texture_sampler[descriptor_indices[1]], vec3(uv, texture_layers[1])).xxx;
	out_color = vec4((ambient + diffuse + specular), 1);
}
