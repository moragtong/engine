use super::*;

#[cfg(unix)]
use winit::platform::unix::EventLoopWindowTargetExtUnix;

/*
#[cfg(window)]
use winit::os::windows::WindowExt;*/

#[cfg(debug_assertions)]
use std::iter::FromIterator;

#[cfg(unix)]
const VULKAN: &str = "libvulkan.so.1";

#[cfg(windows)]
const VULKAN: &str = "vulkan-1.dll";

#[cfg(debug_assertions)]
const LAYER_COUNT: usize = 1;

#[cfg(debug_assertions)]
const LAYERS: [*const std::os::raw::c_char; LAYER_COUNT] = [cstr!("VK_LAYER_KHRONOS_validation")];

use std::convert::TryInto;

pub struct Instance {
	_lib:				libloading::Library,
	pub instance:		crate::vk::Instance,
	pub procs:			crate::vk::InstanceFnV1_0,
	pub procs2:			crate::vk::InstanceFnV1_1,
	pub surface_procs:	crate::vk::KhrSurfaceFn,

	#[cfg(unix)]
	pub wayland_procs:	crate::vk::KhrWaylandSurfaceFn,
	#[cfg(unix)]
	pub xlib_procs:		crate::vk::KhrXlibSurfaceFn,

	#[cfg(windows)]
	pub windows_procs:	crate::vk::KhrWin32SurfaceFn,
}

impl Instance {
	pub fn new<T>(event_loop: &winit::event_loop::EventLoop<T>) -> VkResult<Self> {
		#[cfg(unix)]
		let extensions = [
			cstr!("VK_KHR_surface"),
			cstr!("VK_EXT_debug_utils"),
			if event_loop.is_x11() {
				cstr!("VK_KHR_xlib_surface")
			} else {
				cstr!("VK_KHR_wayland_surface")
			}
		];

		#[cfg(windows)]
		let extensions = [cstr!("VK_KHR_surface"), cstr!("VK_EXT_debug_utils"), cstr!("VK_KHR_win32_surface")];

		let _lib = unsafe { libloading::Library::new(VULKAN) }
			//.ok().ok_or(crate::vk::Result::ERROR_INITIALIZATION_FAILED)?;
			.unwrap();
		let get_instance_proc_addr: extern "system" fn(_: crate::vk::Instance, _: *const libc::c_char) -> *const std::os::raw::c_void =
			*unsafe { _lib.get(b"vkGetInstanceProcAddr\0") }
			.unwrap();
			//.ok().ok_or(crate::vk::Result::ERROR_INITIALIZATION_FAILED)?;

		let entry_points = crate::vk::EntryFnV1_0::load(move |function| get_instance_proc_addr(ConstDefault::DEFAULT, function.as_ptr()));

		unsafe {
			#[cfg(debug_assertions)]
			let layers = {
				println!("Debug enabled");
				let mut count = 32;
				let mut layer_properties = std::mem::MaybeUninit::<[crate::vk::LayerProperties; 32]>::uninit();
				vk_try!(entry_points.enumerate_instance_layer_properties(&mut count, layer_properties.as_mut_ptr() as _));
				let layer_properties = layer_properties.assume_init();
				for property in layer_properties.get_unchecked(..count.try_into().unwrap()) {
					libc::puts(property.layer_name.as_ptr());
				}
				println!("Enabled:");
				Vec::from_iter(LAYERS.iter().filter_map(|layer| {
					layer_properties.get_unchecked(..count.try_into().unwrap()).iter().find(|property|
						libc::strncmp(*layer, property.layer_name.as_ptr(), property.layer_name.len()) == 0)?;
					libc::puts(*layer);
					Some(*layer)
				}))
			};

			#[cfg(debug_assertions)]
			if layers.is_empty() {
				panic!();
			}

			#[cfg(debug_assertions)]
			let enabled_validation_names = layers.as_ptr();

			#[cfg(debug_assertions)]
			let layer_count = layers.len().try_into().unwrap();

			#[cfg(not(debug_assertions))]
			let layer_count = 0;

			#[cfg(not(debug_assertions))]
			let enabled_validation_names = 0 as _;

			let mut instance = std::mem::MaybeUninit::uninit();
			
			vk_try!(entry_points.create_instance(&crate::vk::InstanceCreateInfo {
				p_application_info:			&crate::vk::ApplicationInfo {
					api_version:	crate::vk::make_version(1, 2, 0),
					..ConstDefault::DEFAULT
				},
				pp_enabled_extension_names:	extensions.as_ptr(),
				enabled_extension_count:	extensions.len().try_into().unwrap(),
				enabled_layer_count:		layer_count,
				pp_enabled_layer_names:		enabled_validation_names,
				..ConstDefault::DEFAULT
			}, 0 as _, instance.as_mut_ptr()));

			let instance = instance.assume_init();

			let load_fn = move |function: &std::ffi::CStr| get_instance_proc_addr(instance, function.as_ptr());

			let procs = crate::vk::InstanceFnV1_0::load(load_fn);
			let procs2 = crate::vk::InstanceFnV1_1::load(load_fn);
			let surface_procs = crate::vk::KhrSurfaceFn::load(load_fn);

			#[cfg(unix)]
			let ret = Self {
				wayland_procs:	crate::vk::KhrWaylandSurfaceFn::load(load_fn),
				xlib_procs:		crate::vk::KhrXlibSurfaceFn::load(load_fn),
				procs, procs2, surface_procs, _lib, instance,
			};

			#[cfg(windows)]
			let ret = Self {
				windows_procs:	crate::vk::extensions::KhrWin32SurfaceFn::load(load_fn),
				procs, procs2, surface_procs, _lib, instance,
			};

			Ok(ret)
		}
	}
}

impl Drop for Instance {
	fn drop(&mut self) {
		unsafe { self.procs.destroy_instance(self.instance, 0 as _) };
	}
}
