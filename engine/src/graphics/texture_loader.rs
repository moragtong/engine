use super::*;

#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct PixelFormat {
	pub size:			u32,
	pub flags:			u32,
	pub four_cc:		[u8; 4],
	pub rgb_bit_count:	u32,
	pub red_bit_mask:	u32,
	pub green_bit_mask:	u32,
	pub blue_bit_mask:	u32,
	pub alpha_bit_mask:	u32,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Header {
	pub magic:					u32,
	pub size:					u32,
	pub flags:					u32,
	pub height:					u32,
	pub width:					u32,
	pub pitch_or_linear_size:	u32,
	pub depth:					u32,
	pub mipmap_count:			u32,
	pub reserved:				[u32; 11],
	pub pixel_format:			PixelFormat,
	pub caps:					u32,
	pub caps2:					u32,
	pub caps3:					u32,
	pub caps4:					u32,
	pub reserved2:				u32,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Header10 {
	format:			u32,
	dimension:		u32,
	misc_flag:		u32,
	layer_count:	u32,
	misc_flags2:	u32,
}

pub struct TextureLoader {
	pub file:			async_std::fs::File,
	pub upload_info:	TextureUploadInfo,
}

impl TextureLoader {
	pub async fn new(name: impl AsRef<async_std::path::Path>) -> std::io::Result<Self> {
		use async_std::io::prelude::ReadExt;
		let mut file = async_std::fs::File::open(name).await?;
		let mut size = file.metadata().await?.len() - std::mem::size_of::<Header>() as crate::vk::DeviceSize;

		let mut header = std::mem::MaybeUninit::<Header>::uninit();
		let hs = unsafe { std::slice::from_raw_parts_mut(header.as_mut_ptr() as _, std::mem::size_of::<Header>()) };
		file.read_exact(hs).await?;
		let header = unsafe { header.assume_init() };

		let pixel_format = unsafe { std::str::from_utf8_unchecked(&header.pixel_format.four_cc) };

		let ((format, ratio), layer_count) = match pixel_format {
			"DXT1" => ((crate::vk::Format::BC1_RGB_UNORM_BLOCK, 8), 1),
			"ATI1" => ((crate::vk::Format::BC4_UNORM_BLOCK, 8), 1),
			"ATI2" => ((crate::vk::Format::BC5_UNORM_BLOCK, 16), 1),
			"DX10" => {
				let mut header10 = std::mem::MaybeUninit::<Header10>::uninit();
				let h10s = unsafe { std::slice::from_raw_parts_mut(header10.as_mut_ptr() as _, std::mem::size_of::<Header10>()) };
				file.read_exact(h10s).await?;
				let header10 = unsafe { header10.assume_init() };
				size -= std::mem::size_of::<Header10>() as crate::vk::DeviceSize;

				(match header10.format {
					71 => (crate::vk::Format::BC1_RGB_UNORM_BLOCK, 8),
					80 => (crate::vk::Format::BC4_UNORM_BLOCK, 8),
					83 => (crate::vk::Format::BC5_UNORM_BLOCK, 16),
					format => panic!("{:?}", format)
				}, header10.layer_count)
			}
			format => panic!("{:?}", format)
		};

		let mipmap_count = header.mipmap_count.min(((header.width / (ratio / 2)) as f32).log2().floor() as u32 + 1).min(((header.height / (ratio / 2)) as f32).log2().floor() as u32 + 1);

		println!("{:?}", (pixel_format, mipmap_count, header.pitch_or_linear_size));
		Ok(Self {
			file, upload_info: TextureUploadInfo {
				extent:			crate::vk::Extent3D {
					width:	header.width,
					height:	header.height,
					depth:	1,
				},
				create_info:	device::TextureCreateInfo {
					format, extent: crate::vk::Extent2D {
						width:	header.width,
						height:	header.height,
					}
				},
				level_count:	mipmap_count,
				layer_count, format, ratio: ratio.into(), size
			}
		})
	}
}