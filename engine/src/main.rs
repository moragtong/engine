#[macro_use]
extern crate cstr_macro;
use vk_shader_macros::include_glsl;
#[macro_use]
extern crate serde_derive;
extern crate vk;

mod map_editor;
mod console;
mod physics;
#[macro_use]
mod graphics;
mod time;
mod movement;
mod camera;
mod load;

use self::console::*;
use self::time::*;
use self::movement::*;
use self::camera::*;
use self::load::*;

fn main() {
	map_editor::map_editor().unwrap();
}
