//use super::*;
pub struct Time {
	prev:	std::time::Instant,
	next:	std::time::Instant,
	delta:	std::time::Duration,
}

impl Default for Time {
	fn default() -> Self {
		let now = std::time::Instant::now();
		Self {
			prev:	now,
			next:	now,
			delta:	std::time::Duration::default(),
		}
	}
}

impl Time {
	pub fn update(&mut self) {
		self.prev = self.next;
		self.next = std::time::Instant::now();
		self.delta = self.next.duration_since(self.prev).mul_f32(1000.);
	}

	pub fn delta(&self) -> std::time::Duration {
		self.delta
	}
}