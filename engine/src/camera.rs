use crate::physics::FloatT;
use super::*;

const CAMERA_SIZE: FloatT = 0.3;
const VELOCITY: FloatT = 0.2;

pub struct CameraPhysics {
	physics:	crate::physics::Index,
}

impl CameraPhysics {
	pub fn new(physics_system: &mut physics::System, position: nalgebra::Point3<f32>) -> Self {
		Self {
			physics:	physics_system.arena.insert(physics::Component::new(std::sync::Arc::new(physics::Model {
				radius:	CAMERA_SIZE,
				vertex: Default::default(),
				index:	Default::default(),
			}),
			physics::Transform {
				rotation:	nalgebra::Vector3::new(-180., 0., 180.),
				scale:		1.,
				position
			}, !0)),
		}
	}

	pub fn view(&self, physics_system: &physics::System) -> nalgebra::Matrix4<f32> {
		let camera_physics = physics_system.arena.get(self.physics).unwrap();
		nalgebra::Matrix4::from(camera_physics.transform.rotation_matrix().inverse())
		* nalgebra::Matrix4::from(nalgebra::Translation::from(camera_physics.transform.position).inverse())
	}

	pub fn position(&self, physics_system: &physics::System) -> nalgebra::Point3<f32> {
		let camera_physics = physics_system.arena.get(self.physics).unwrap();
		camera_physics.transform.position
	}

	pub fn apply_movement_no_physics(&self, physics_system: &mut physics::System, movement: &Move) {
		let physics = physics_system.arena.get_mut(self.physics).unwrap();

		physics.transform.rotation.x += movement.delta().y as f32;
		physics.transform.rotation.x = nalgebra::clamp(physics.transform.rotation.x, -90., 90.);
		physics.transform.rotation.y -= movement.delta().x as f32;

		physics.transform.position += physics.transform.rotation_matrix() * movement.eye_delta;
	}

	pub fn apply_movement(&self, physics_system: &mut physics::System, movement: &Move) {
		let physics = physics_system.arena.get_mut(self.physics).unwrap();

		physics.transform.rotation.x += movement.delta().y as f32;
		physics.transform.rotation.x = nalgebra::clamp(physics.transform.rotation.x, -90., 90.);
		physics.transform.rotation.y -= movement.delta().x as f32;


		physics.velocity = physics.transform.rotation_matrix() * (VELOCITY * movement.eye_delta);
	}

}
