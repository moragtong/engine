use super::*;

/*#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MapEntity {
	pub graphics:	graphics::MapObject,
	pub physics:	physics::MapObject,
}*/

#[derive(Debug)]
pub struct Payload {
	pub graphics:	graphics::Payload,
	pub physics:	Vec<physics::Component>
}

pub async fn load(physics_model_map: &mut physics::ModelMap, resource_manager: &mut graphics::ResourceManager, objects: Vec<graphics::MapObject>)
	-> Result<Payload, map_editor::EngineError> {
	let graphics = resource_manager.load_objects(&objects).await?;
	let mut physics = Vec::with_capacity(objects.len());
	for object in objects {
		physics.push(physics::Component::from_file(physics_model_map, object.model, (&object.transform).into(), object.static_level).await?)
	}
	Ok(Payload { graphics, physics })
}
