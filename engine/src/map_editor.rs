use super::*;

use std::iter::FromIterator;
use std::str::FromStr;
use futures::join;

#[derive(Debug)]
pub enum EngineEvent {
	Console(String),
	Payload(load::Payload),
}

#[derive(Debug)]
pub enum EngineError {
	Io(std::io::Error),
	Vk(crate::vk::Result),
	Bincode(bincode::Error),
	Json(serde_json::error::Error),
	EventLoopClosed(winit::event_loop::EventLoopClosed<EngineEvent>),
	ResourceNotFound,
}

impl From<std::io::Error> for EngineError {
	fn from(err: std::io::Error) -> Self {
		EngineError::Io(err)
	}
}

impl From<crate::vk::Result> for EngineError {
	fn from(err: crate::vk::Result) -> Self {
		EngineError::Vk(err)
	}
}

impl From<bincode::Error> for EngineError {
	fn from(err: bincode::Error) -> Self {
		EngineError::Bincode(err)
	}
}

impl From<serde_json::error::Error> for EngineError {
	fn from(err: serde_json::error::Error) -> Self {
		EngineError::Json(err)
	}
}

impl From<winit::event_loop::EventLoopClosed<EngineEvent>> for EngineError {
	fn from(err: winit::event_loop::EventLoopClosed<EngineEvent>) -> Self {
		EngineError::EventLoopClosed(err)
	}
}

struct Entity {
	graphics:	graphics::Index,
	physics:	physics::Index,
}

#[derive(Default)]
struct ObjectSelector {
	objects:	Vec<Entity>,
	selected:	usize,
}

impl ObjectSelector {
	fn remove_selected(&mut self) -> Option<Entity> {
		if self.objects.is_empty() {
			None
		} else {
			let selected = self.selected;
			if self.objects.len() != 1 && self.objects.len() - 1 == self.selected {
				self.selected -= 1;
			}
			Some(self.objects.remove(selected))
		}
	}

	/*fn get_selected<'p>(&self, physics_system: &'p physics::System) -> &'p physics::Component {
		physics_system.arena.get(unsafe { self.objects.get_unchecked(self.selected).physics }).unwrap()
	}*/

	fn get_selected_graphics<'p>(&self, system: &'p graphics::System) -> &'p graphics::Component {
		system.arena.get(unsafe { self.objects.get_unchecked(self.selected).graphics }).unwrap()
	}

	fn get_selected_mut<'p>(&mut self, system: &'p mut physics::System) -> &'p mut physics::Component {
		let selected = self.selected;
		system.arena.get_mut(unsafe { self.objects.get_unchecked(selected).physics }).unwrap()
	}

	fn get_selected_mut_graphics<'p>(&mut self, system: &'p mut graphics::System) -> &'p mut graphics::Component {
		let selected = self.selected;
		system.arena.get_mut(unsafe { self.objects.get_unchecked(selected).graphics }).unwrap()
	}

	fn select_next(&mut self) {
		self.selected = (self.selected + 1) % self.objects.len()
	}

	fn select_prev(&mut self) {
		self.selected = if self.selected == 0 {
			self.objects.len() - 1 
		} else {
			self.selected - 1
		}
	}
}

#[derive(Debug, PartialEq, Eq)]
enum ControlState {
	RotateX,
	RotateY,
	RotateZ,
	RotatePending,
	Scale,
	MoveX,
	MoveY,
	MoveZ,
	MovePending,
}

struct Control {
	state:		ControlState,
	transform:	graphics::Transform,
}

use winit::platform::run_return::EventLoopExtRunReturn;

pub fn map_editor() -> Result<(), EngineError> {
	let mut physics_system =	physics::System::default();
	let physics_model_map =		std::sync::Arc::from(async_std::sync::Mutex::from(physics::ModelMap::default()));

	let mut event_loop =		winit::event_loop::EventLoop::with_user_event();
	let mut graphics_system =	graphics::System::new(&event_loop)?;
	let mut render_data =		graphics::RenderData {
		light:		graphics::Light {
			direction:	nalgebra::Vector4::new(1.0, 1.0, 0.5, 0.0),
		},
		camera:		graphics_system.new_camera(),
	};
	let camera_physics = 		CameraPhysics::new(&mut physics_system, nalgebra::Point3::new(2., 2., 2.));
	let proxy = 				event_loop.create_proxy();
	let mut console = 			Console::from(proxy.clone());
	let mut object_selector =	ObjectSelector::default();
	let mut console_handle =	Some(async_std::task::spawn(async move {
		console.run().await
	}));
	let mut time =				Time::default();
	let mut movement =			Move::default();
	let mut filename =			String::default();
	let mut exit = false;
	let mut result = Result::<_, EngineError>::Ok(());

	let mut state =				Option::<Control>::None;

	let mut physics_enabled = false;

	while !exit {
		event_loop.run_return(|event, _, control_flow| {
			result = async_std::task::block_on(async {
				match event {
					winit::event::Event::UserEvent(EngineEvent::Payload(payload)) =>
						for (graphics, physics_component) in graphics_system.load_payload(payload.graphics).into_iter().zip(payload.physics) {
							object_selector.objects.push(Entity {
								physics:	physics_system.arena.insert(physics_component),
								graphics
							})
						}
					winit::event::Event::WindowEvent {
						event: winit::event::WindowEvent::KeyboardInput {
							input:	winit::event::KeyboardInput {
								state:				winit::event::ElementState::Released,
								virtual_keycode:	Some(keycode),
								..
							}, ..
						}, ..
					} if physics_enabled => match keycode {
						winit::event::VirtualKeyCode::A =>
							movement.eye_delta[0] = 0.,
						winit::event::VirtualKeyCode::D =>
							movement.eye_delta[0] = 0.,
						winit::event::VirtualKeyCode::W =>
							movement.eye_delta[2] = 0.,
						winit::event::VirtualKeyCode::S =>
							movement.eye_delta[2] = 0.,
						_ => {}
					}
					winit::event::Event::WindowEvent {
						event: winit::event::WindowEvent::KeyboardInput {
							input:	winit::event::KeyboardInput {
								state:				winit::event::ElementState::Pressed,
								virtual_keycode:	Some(keycode),
								..
							}, ..
						}, ..
					} => match keycode {
						winit::event::VirtualKeyCode::Escape => if let Some(control) = &mut state {
							if !physics_enabled {
								object_selector.get_selected_mut_graphics(&mut graphics_system).map_object.transform = control.transform;
								state = None;
							}
						}
						winit::event::VirtualKeyCode::A =>
							movement.eye_delta[0] = -1.,
						winit::event::VirtualKeyCode::D =>
							movement.eye_delta[0] = 1.,
						winit::event::VirtualKeyCode::W =>
							movement.eye_delta[2] = -1.,
						winit::event::VirtualKeyCode::S =>
							movement.eye_delta[2] = 1.,
						winit::event::VirtualKeyCode::Space =>
							movement.eye_delta[1] = -1.,
						winit::event::VirtualKeyCode::Left => if state.is_none() {
							object_selector.select_next();
						}
						winit::event::VirtualKeyCode::Right => if state.is_none() {
							object_selector.select_prev();
						}
						winit::event::VirtualKeyCode::Key1 => if state.is_none() & !physics_enabled {
							state = Some(Control {
								state:		ControlState::RotatePending,
								transform:	object_selector.get_selected_graphics(&graphics_system).map_object.transform,
							})
						}
						winit::event::VirtualKeyCode::Key2 => if state.is_none() & !physics_enabled {
							state = Some(Control {
								state:		ControlState::Scale,
								transform:	object_selector.get_selected_graphics(&graphics_system).map_object.transform,
							})
						}
						winit::event::VirtualKeyCode::Key3 => if state.is_none() & !physics_enabled {
							state = Some(Control {
								state:		ControlState::MovePending,
								transform:	object_selector.get_selected_graphics(&graphics_system).map_object.transform,
							})
						}
						winit::event::VirtualKeyCode::X => if let Some(control) = &mut state {
							match control.state {
								ControlState::RotatePending => control.state = ControlState::RotateX,
								ControlState::MovePending => control.state = ControlState::MoveX,
								_ => {}
							}
						}
						winit::event::VirtualKeyCode::Y => if let Some(control) = &mut state {
							match control.state {
								ControlState::RotatePending => control.state = ControlState::RotateY,
								ControlState::MovePending => control.state = ControlState::MoveY,
								_ => {}
							}
						}
						winit::event::VirtualKeyCode::Z => if let Some(control) = &mut state {
							match control.state {
								ControlState::RotatePending => control.state = ControlState::RotateZ,
								ControlState::MovePending => control.state = ControlState::MoveZ,
								_ => {}
							}
						}
						_ => {}
					}

					winit::event::Event::WindowEvent {
						event: winit::event::WindowEvent::CursorEntered { .. }, ..
					} => graphics_system.window().set_cursor_visible(false),

					winit::event::Event::WindowEvent {
						event: winit::event::WindowEvent::CursorLeft { .. }, ..
					} => graphics_system.window().set_cursor_visible(true),

					winit::event::Event::DeviceEvent {
						event: winit::event::DeviceEvent::MouseMotion {
							delta, ..
						}, ..
					} => movement.cursor([delta.0, delta.1]),

					winit::event::Event::WindowEvent {
						event: winit::event::WindowEvent::MouseInput {
							state:	winit::event::ElementState::Released,
							button:	winit::event::MouseButton::Left, ..
						}, ..
					} => state = None,

					winit::event::Event::MainEventsCleared =>
						graphics_system.window().request_redraw(),

					winit::event::Event::RedrawRequested(_) => {
						if let Some(control) = &state {
							let delta = movement.flat_delta(time.delta()) as f32;
							let selected = &mut object_selector.get_selected_mut_graphics(&mut graphics_system).map_object.transform;
							match &control.state {
								ControlState::RotateX => selected.rotation[0] += delta,
								ControlState::RotateY => selected.rotation[1] += delta,
								ControlState::RotateZ => selected.rotation[2] += delta,
								ControlState::MoveX => selected.location[0] += delta * 0.001,
								ControlState::MoveY => selected.location[1] += delta * 0.001,
								ControlState::MoveZ => selected.location[2] += delta * 0.001,
								ControlState::Scale => selected.scale += delta * 0.001,
								_ => {}
							}
						} else if physics_enabled {
							camera_physics.apply_movement(&mut physics_system, &movement);
							render_data.camera.update(&physics_system, &camera_physics);

							physics_system.update(time.delta());

							for object in &object_selector.objects {
								graphics_system.arena.get_mut(object.graphics).unwrap().map_object.transform =
									(&physics_system.arena.get(object.physics).unwrap().transform).into();
							}
						} else {
							camera_physics.apply_movement_no_physics(&mut physics_system, &movement);
							render_data.camera.update(&physics_system, &camera_physics);
						}

						movement = Default::default();

						graphics_system.submit(&render_data, &[(object_selector.selected as _, 1)])?;
					}
					winit::event::Event::RedrawEventsCleared => *control_flow = winit::event_loop::ControlFlow::Exit,
					winit::event::Event::UserEvent(EngineEvent::Console(buffer)) =>
						match Vec::from_iter(buffer.split_whitespace()).as_slice() {
							["load_model", model, static_level] => {
								let proxy = proxy.clone();
								let resource_manager = std::sync::Arc::clone(&graphics_system.resource_manager());
								let model_map = std::sync::Arc::clone(&physics_model_map);
								let model = model.to_string();
								if let Ok(static_level) = u8::from_str(static_level) {
									async_std::task::spawn(async move {
										let mut map_objects = Vec::with_capacity(1);
										map_objects.push(graphics::MapObject {
											textures:	["utah_diffuse.dds".into(),"utah_specular.dds".into(),"test2_normal.dds".into()],
											transform:	graphics::Transform {
												scale:	1.0,
												..Default::default()
											}, model, static_level
										});

										let (mut rm_guard, mut mm_guard) = join!(
											resource_manager.lock(),
											model_map.lock(),
										);
										let payload = EngineEvent::Payload(load(&mut mm_guard, &mut rm_guard, map_objects).await?);
										proxy.send_event(payload)?;
										Result::<(), EngineError>::Ok(())
									});
								} else {
									println!("Invalid static level.");
								}
							}
							["unload"] => {
								if let Some(unloaded) = object_selector.remove_selected() {
									graphics_system.unload_object(unloaded.graphics);
									physics_system.arena.remove(unloaded.physics);
								} else {
									println!("No objects are loaded.");
								}
							}
							["load", name] => {
								filename = (*name).into();
								let name = filename.clone();
								let resource_manager = std::sync::Arc::clone(&graphics_system.resource_manager());
								let model_map = std::sync::Arc::clone(&physics_model_map);
								let proxy = proxy.clone();

								async_std::task::spawn(async move {
									use async_std::io::prelude::ReadExt;

									let mut buffer = Vec::default();
									let mut file = async_std::fs::File::open(&name).await?;
									file.read_to_end(&mut buffer).await?;
									let objects: Vec<graphics::MapObject> = serde_json::from_slice(&buffer)?;
									let (mut rm_guard, mut mm_guard) = join!(
										resource_manager.lock(),
										model_map.lock(),
									);
									let payload = EngineEvent::Payload(load(&mut mm_guard, &mut rm_guard, objects).await?);
									proxy.send_event(payload)?;
									Result::<(), EngineError>::Ok(())
								});
							}
							["save"] => if filename.is_empty() {
									println!("Please specify a name");
								} else {
									graphics_system.save(filename.clone()).await?;
								}
							["add_velocity", v_x, v_y, v_z] => {
								let selected = object_selector.get_selected_mut(&mut physics_system);
								if let (Ok(v_x), Ok(v_y), Ok(v_z)) = (f32::from_str(*v_x), f32::from_str(*v_y), f32::from_str(*v_z)) {
									selected.velocity += nalgebra::Vector3::new(v_x, v_y, v_z);
								} else {
									println!("Incorrect velocity format.");
								}
							}
							["save", name] => {
								filename = (*name).into();
								graphics_system.save(filename.clone()).await?;
							}
							["physics_enable", enable] =>
								if let Ok(enable) = bool::from_str(enable) {
									physics_enabled = enable;
									for object in &object_selector.objects {
										physics_system.arena.get_mut(object.physics).unwrap().transform = 
											(&graphics_system.arena.get(object.graphics).unwrap().map_object.transform).into();
									}
								} else {
									println!("Incorrect boolean format.");
								}
							["exit"] => exit = true,
							_ => {
								println!("Unidentified command.");
							}
						}
					_ => {}
				}
				Ok(())
			})
		});
		result?;

		time.update();
		result = Ok(())
	}
	graphics_system.wait_for_idle();
	async_std::task::block_on(async {
		if let Some(console_handle) = console_handle.take() {
			console_handle.await?
		} else {
			unreachable!()
		}
		Ok(())
	})
}
