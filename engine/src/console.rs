pub struct Console {
	stdin:				async_std::io::Stdin,
	proxy:				winit::event_loop::EventLoopProxy<crate::map_editor::EngineEvent>,
}

impl From<winit::event_loop::EventLoopProxy<crate::map_editor::EngineEvent>> for Console {
	fn from(proxy: winit::event_loop::EventLoopProxy<crate::map_editor::EngineEvent>) -> Self {
		Self {
			stdin:				async_std::io::stdin(),
			proxy
		}
	}
}

impl Console {
	pub async fn run(&mut self) -> Result<(), crate::map_editor::EngineError> {
		loop {
			let mut buffer = String::default();
			self.stdin.read_line(&mut buffer).await?;
			if let Some("exit") = buffer.split_whitespace().next() {
				self.proxy.send_event(crate::map_editor::EngineEvent::Console(buffer))?;
				break
			}
			self.proxy.send_event(crate::map_editor::EngineEvent::Console(buffer))?;
		}
		Ok(())
	}
}
