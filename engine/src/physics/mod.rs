use super::*;

mod system;
mod component;
mod collision;

pub type FloatT = f32;

pub use self::system::*;
pub use self::component::*;

pub type ModelMap = std::collections::HashMap<String, std::sync::Weak<Model>>;

#[derive(Debug)]
pub struct Model {
	pub radius:	FloatT,
	pub vertex:	Vec<nalgebra::Point3<FloatT>>,
	pub index:	Vec<u32>,
}

impl Default for Model {
	fn default() -> Self {
		Self {
			radius:	0.,
			vertex:	Default::default(),
			index:	Default::default(),
		}
	}
}

pub fn vertex_index_intersect_sphere(vertex: &[nalgebra::Point3<FloatT>], index: &[u32],
	origin: &nalgebra::Point3<FloatT>, radius: FloatT, ret: &mut Vec<nalgebra::Vector3<FloatT>>)
{
	use std::convert::TryFrom;

	for tri in index.chunks(6) {
		#[cfg(not(debug_assertions))]
		let tri: &[u32; 6] = unsafe { std::mem::transmute(tri.as_ptr()) };
		let triangle = collision::Triangle {
			points: [vertex[usize::try_from(tri[0]).unwrap()],
					vertex[usize::try_from(tri[2]).unwrap()],
					vertex[usize::try_from(tri[4]).unwrap()]]
		};
		triangle.intersects_sphere(&origin, radius, ret);
	}
}

impl Model {
	pub async fn new(model: &str) -> Result<Self, map_editor::EngineError> {
		use async_std::io::prelude::ReadExt;
		use std::convert::TryInto;

		let mut model_loader = graphics::ModelLoader::new(model).await?;

		let vertex = unsafe {
			let len = model_loader.vertex_count.try_into().unwrap();
			let mut vertex = Vec::<graphics::PerVertex>::with_capacity(len);
			vertex.set_len(len);

			let raw = std::slice::from_raw_parts_mut(vertex.as_mut_ptr() as _, vertex.len() * std::mem::size_of::<graphics::PerVertex>());
			model_loader.file.read_exact(raw).await?;

			vertex.iter().map(|per_vertex| per_vertex.position.into()).collect()
		};
		let index = unsafe {
			let len = model_loader.index_count.try_into().unwrap();
			let mut index = Vec::<u32>::with_capacity(len);
			index.set_len(len);

			let raw = std::slice::from_raw_parts_mut(index.as_mut_ptr() as _, index.len() * std::mem::size_of::<u32>());
			model_loader.file.read_exact(raw).await?;

			index
		};

		Ok(Self { vertex, index, radius: model_loader.radius })
	}
}

#[derive(Debug, Clone, Copy)]
pub struct TransformedModel<'a> {
	pub radius:		FloatT,
	pub	position:	&'a nalgebra::Point3<FloatT>,
	pub vertex:		&'a [nalgebra::Point3<FloatT>],
	pub index:		&'a [u32],
}

impl<'a> TransformedModel<'a> {
	pub fn intersect(self, other: Self, ret: &mut Vec<nalgebra::Vector3<FloatT>>) {
		use std::convert::TryFrom;

		let spheres_delta = collision::intersect_spheres(self.position, self.radius,
			other.position, other.radius);

		if spheres_delta.norm_squared() == 0. {

		} else if self.vertex.is_empty() && other.vertex.is_empty() {
			ret.push(spheres_delta)

		} else if other.vertex.is_empty() {
			vertex_index_intersect_sphere(self.vertex, self.index, &other.position, other.radius, ret)

		} else if self.vertex.is_empty() {
			let begin = ret.len();

			vertex_index_intersect_sphere(other.vertex, other.index, &self.position, self.radius, ret);

			for v in ret.get_mut(begin..).unwrap() {
				*v = -*v;
			}
		} else {
			for tri1 in self.index.chunks(6) {
				#[cfg(not(debug_assertions))]
				let tri1: &[u32; 6] = unsafe { std::mem::transmute(tri1.as_ptr()) };
				let triangle1 = collision::Triangle {
					points: [self.vertex[usize::try_from(tri1[0]).unwrap()],
							self.vertex[usize::try_from(tri1[2]).unwrap()],
							self.vertex[usize::try_from(tri1[4]).unwrap()]]
				};
				for tri2 in other.index.chunks(6) {
					#[cfg(not(debug_assertions))]
					let tri2: &[u32; 6] = unsafe { std::mem::transmute(tri2.as_ptr()) };
					let triangle2 = collision::Triangle {
						points: [other.vertex[usize::try_from(tri2[0]).unwrap()],
								other.vertex[usize::try_from(tri2[2]).unwrap()],
								other.vertex[usize::try_from(tri2[4]).unwrap()]]
					};
					triangle1.intersects(&triangle2, ret);
				}
			}
		}
	}
}
