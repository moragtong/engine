use super::*;

//const COF: FloatT = 0.2;
pub (super) const G: FloatT = -9.80665;

#[derive(Debug, Clone)]
pub struct Transform {
	pub position:	nalgebra::Point3<FloatT>,
	pub rotation:	nalgebra::Vector3<FloatT>,
	pub scale:		FloatT,
}

impl Transform {
	pub fn copy_from(&mut self, other: &Self) {
		self.position.coords.copy_from(&other.position.coords);
		self.rotation.copy_from(&other.rotation);
		self.scale = other.scale;
	}

	pub fn transform_vertices(&self, vertices: &[nalgebra::Point3<FloatT>], ret: &mut Vec<nalgebra::Point3<FloatT>>) {
		let mtx = nalgebra::Matrix4::from(self);
		for point in vertices {
			let point4 = mtx * nalgebra::Point4::new(point.x, point.y, point.z, 1.);
			ret.push(nalgebra::Point3::new(point4.x, point4.y, point4.z));
		}
	}
}

impl From<&graphics::render_data::Transform> for Transform {
	fn from(transform: &graphics::render_data::Transform) -> Self {
		Self {
			position:	transform.location.into(),
			rotation:	transform.rotation.into(),
			scale:		transform.scale
		}
	}
}

pub fn rotation(v: &nalgebra::Vector3<FloatT>) -> nalgebra::Rotation3<FloatT> {
	nalgebra::Rotation3::new(nalgebra::Vector3::y() * v.y.to_radians()) *
	nalgebra::Rotation3::new(nalgebra::Vector3::x() * v.x.to_radians()) *
	nalgebra::Rotation3::new(nalgebra::Vector3::z() * v.z.to_radians())
}

impl Transform {
	pub fn rotation_matrix(&self) -> nalgebra::Rotation3<FloatT> {
		rotation(&self.rotation)
	}
}

impl From<&Transform> for nalgebra::Matrix4<FloatT> {
	fn from(transform: &Transform) -> Self {
		nalgebra::Matrix4::new_translation(&transform.position.coords) *
		nalgebra::Matrix4::from(transform.rotation_matrix()) *
		nalgebra::Matrix4::new_scaling(transform.scale)

	}
}

#[derive(Debug)]
pub struct Component {
	pub transform:				Transform,
	pub(super) model:			std::sync::Arc<Model>,
	pub(super) prev_transform:	Transform,
	pub velocity:				nalgebra::Vector3<FloatT>,
	pub(super) accel:			nalgebra::Vector3<FloatT>,
	pub(super) transform_vertex:Vec<nalgebra::Point3<FloatT>>,
	pub(super) static_level:	u8,
}

impl Component {
	pub async fn from_file(model_map: &mut std::collections::HashMap<String, std::sync::Weak<Model>>,
		name: String, transform: Transform, static_level: u8) -> Result<Component, map_editor::EngineError> {
		let model = match model_map.entry(name) {
			std::collections::hash_map::Entry::Occupied(mut occupied) =>
				if let Some(model) = std::sync::Weak::upgrade(occupied.get()) {
					model
				} else {
					let model = std::sync::Arc::new(Model::new(occupied.key()).await?);
					occupied.insert(std::sync::Arc::downgrade(&model));
					model
				}
			std::collections::hash_map::Entry::Vacant(vacant) => {
				let model = std::sync::Arc::new(Model::new(vacant.key()).await?);
				vacant.insert(std::sync::Arc::downgrade(&model));
				model
			}
		};
		Ok(Self::new(model, transform, static_level))
	}

	pub fn new(model: std::sync::Arc<Model>, transform: Transform, static_level: u8) -> Component {
		Component {
			velocity:			Default::default(),
			accel:				nalgebra::Vector3::new(0., G, 0.),
			prev_transform:		transform.clone(),
			transform_vertex:	Default::default(),
			model, transform, static_level
		}
	}

	pub fn resolve(&mut self, delta: &nalgebra::Vector3<FloatT>) {
		const SQRT_2_2: f32 = std::f32::consts::SQRT_2 / 2.;
		self.transform.position += delta;
		let norm_delta = delta.normalize();
		//self0.velocity = self.velocity - self.velocity.dot(&norm_delta) * norm_delta;
		let dot = self.velocity.dot(&norm_delta);
		if dot > SQRT_2_2 || dot < -SQRT_2_2 {
			self.accel = self.accel.dot(&norm_delta) * norm_delta;
		}
		self.velocity = nalgebra::zero();
	}

	pub fn simulate(&mut self, _dt: f32) {
		if self.static_level != 0 {
			let dt = 0.1;

			self.prev_transform.copy_from(&self.transform);

			self.transform.position += self.velocity * dt + 0.5 * self.accel * dt.powi(2);
			self.velocity += self.accel * dt;
		}
	}
}
