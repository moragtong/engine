use super::*;

//const CELL_SIZE: FloatT = 1.;

pub type Index = typed_generational_arena::Index<Component>;

pub struct System {
	//map:			std::collections::HashMap<[u32; 3], std::collections::HashSet<typed_generational_arena::Index<Component>>>,
	intersect_vecs:			Vec<nalgebra::Vector3<FloatT>>,
	pub arena:				typed_generational_arena::Arena<Component, usize, u64>,
}

impl Default for System {
	fn default() -> Self {
		Self {
			//map:					Default::default(),
			intersect_vecs:			Default::default(),
			arena:					typed_generational_arena::Arena::new(),
		}
	}
}

impl System {
	pub fn update(&mut self, delta_time: std::time::Duration) {
		/*for (idx, component) in self.arena.iter_mut() {
			component.simulate(delta_time);
			let points = [
				nalgebra::Point3::new(component.aabb[0].0, component.aabb[1].0, component.aabb[2].0),
				nalgebra::Point3::new(component.aabb[0].0, component.aabb[1].1, component.aabb[2].0),
				nalgebra::Point3::new(component.aabb[0].0, component.aabb[1].0, component.aabb[2].1),
				nalgebra::Point3::new(component.aabb[0].0, component.aabb[1].1, component.aabb[2].1),
				nalgebra::Point3::new(component.aabb[0].1, component.aabb[1].0, component.aabb[2].0),
				nalgebra::Point3::new(component.aabb[0].1, component.aabb[1].1, component.aabb[2].0),
				nalgebra::Point3::new(component.aabb[0].1, component.aabb[1].0, component.aabb[2].1),
				nalgebra::Point3::new(component.aabb[0].1, component.aabb[1].1, component.aabb[2].1),
			];
			for point in &points {
				let entry = self.map.entry([
					(point.x / CELL_SIZE).round() as _,
					(point.y / CELL_SIZE).round() as _,
					(point.z / CELL_SIZE).round() as _,
				]).or_insert_with(Default::default);

				entry.insert(idx);
			}
		}

		for idx_vec in self.map.values() {
			for (i, component0idx) in idx_vec.iter().enumerate() {
				for component1idx in idx_vec.iter().skip(i + 1) {
					let (component0, component1) = self.arena.get2_mut(*component0idx, *component1idx);
					let component0 = component0.unwrap();
					let component1 = component1.unwrap();

					let distance = bboxes_intersect([&component0.aabb, &component1.aabb]);

					let dist = distance.norm();
					if dist != 0. {
						component0.collide(component1);
					}
				}
			}
		}

		self.map.clear();*/

		for (_, component) in self.arena.iter_mut() {
			component.simulate(delta_time.as_secs_f32());
			component.transform.transform_vertices(&component.model.vertex, &mut component.transform_vertex);
			component.accel = nalgebra::Vector3::new(0., G, 0.);
		}

		let indices: Vec<_> = self.arena.iter().map(|(i, _)| i).collect();

		for (i, component0idx) in indices.iter().enumerate() {
			for component1idx in indices.iter().skip(i + 1) {
				let (component0, component1) = self.arena.get2_mut(*component0idx, *component1idx);
				let component0 = component0.unwrap();
				let component1 = component1.unwrap();

				(TransformedModel {
					radius:		component0.transform.scale * component0.model.radius,
					position:	&component0.transform.position,
					vertex:		&component0.transform_vertex,
					index:		&component0.model.index,
				}).intersect(TransformedModel {
					radius:		component1.transform.scale * component1.model.radius,
					position:	&component1.transform.position,
					vertex:		&component1.transform_vertex,
					index:		&component1.model.index,
				}, &mut self.intersect_vecs);

				if !self.intersect_vecs.is_empty() {
					let mut dist0 = f32::MAX;
					let mut dist1 = f32::MAX;
					let mut delta0 = nalgebra::Vector3::new(0., 0., 0.);
					let mut delta1 = nalgebra::Vector3::new(0., 0., 0.);
					let prev_d0 = component0.transform.position - component0.prev_transform.position;
					let prev_d1 = component1.transform.position - component1.prev_transform.position;
					for t_delta in &self.intersect_vecs {
						let t_dist0 = (prev_d0 - *t_delta).norm_squared();
						let t_dist1 = (prev_d1 + *t_delta).norm_squared();
						if t_dist0 < dist0 {
							delta0 = -*t_delta;
							dist0 = t_dist0;
						}
						if t_dist1 < dist1 {
							delta1 = *t_delta;
							dist1 = t_dist1;
						}
					}
					let which = if component0.static_level == component1.static_level {
						component0.velocity.norm_squared() > component1.velocity.norm_squared()
					} else {
						component0.static_level > component1.static_level
					};

					if which {
						component0.resolve(&delta0);
					} else {
						component1.resolve(&delta1);
					}

					self.intersect_vecs.clear();
				}
			}
		}

		for (_, component) in self.arena.iter_mut() {
			component.transform_vertex.clear();
		}
	}
}
