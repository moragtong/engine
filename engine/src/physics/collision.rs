use super::*;

pub fn intersect_spheres(self_origin: &nalgebra::Point3<physics::FloatT>, self_radius: FloatT,
	other_origin: &nalgebra::Point3<physics::FloatT>, other_radius: FloatT) -> nalgebra::Vector3<FloatT>
{
	let radii_sum = self_radius + other_radius;
	let delta = other_origin - self_origin;

	if radii_sum.powi(2) > delta.norm_squared() {
		delta.normalize() * radii_sum - delta
	} else {
		Default::default()
	}
}

pub struct Triangle {
	pub points: [nalgebra::Point3<physics::FloatT>; 3]
}

impl Triangle {
	fn normal(&self) -> nalgebra::Vector3<physics::FloatT> {
		let v1 = self.points[1] - self.points[0];
		let v2 = self.points[2] - self.points[0];

		v1.cross(&v2).normalize()
	}

	fn is_point_bounded(&self, test_point: &nalgebra::Point3<physics::FloatT>) -> bool {
		let mut angle = 0.;
		for (i, point) in self.points.iter().enumerate() {
			let a = point - test_point;
			let b = unsafe { self.points.get_unchecked((i + 1) % 3) } - test_point;

			angle += (a.dot(&b) / (a.norm() * b.norm())).acos();
		}

		angle >= 2. * std::f32::consts::PI * 0.99
	}

	fn intersect_once(&self, other: &Self, ret: &mut Vec<nalgebra::Vector3<FloatT>>) {
		for (i, point) in self.points.iter().enumerate() {
			let line = Line {
				points:	[
					*point,
					*unsafe { self.points.get_unchecked((i + 1) % 3) },
				]
			};
			let intersect = line.intersects(other);
			if intersect.norm_squared() != 0. {
				ret.push(intersect);
			}
		}
	}

	pub fn intersects(&self, other: &Self, ret: &mut Vec<nalgebra::Vector3<FloatT>>) {
		self.intersect_once(other, ret);
		let begin = ret.len();
		other.intersect_once(self, ret);
		for v in ret.get_mut(begin..).unwrap() {
			*v = -*v;
		}
	}

	pub fn intersects_sphere(&self, origin: &nalgebra::Point3<physics::FloatT>, radius: FloatT,
		ret: &mut Vec<nalgebra::Vector3<physics::FloatT>>) {
		let normal = self.normal();
		let dist = normal.dot(&(origin - self.points[0]).into());
		if dist.abs() < radius {
			let normal_dist = normal * dist;
			let proj_origin = nalgebra::Point3::from(origin - normal_dist);
			if self.is_point_bounded(&proj_origin) {
				ret.push(normal_dist.normalize() * radius - normal_dist);
			} else {
				for (i, point) in self.points.iter().enumerate() {
					let line = Line {
						points:	[
							*point,
							*unsafe { self.points.get_unchecked((i + 1) % 3) },
						]
					};
					let delta = line.closest_point(origin);
					if delta.norm() < radius {
						ret.push(delta.normalize() * radius - delta);
					}
				}
			}
		}
	}
}

pub struct Line {
	pub points: [nalgebra::Point3<physics::FloatT>; 2]
}

impl Line {
	pub fn closest_point(&self, origin: &nalgebra::Point3<physics::FloatT>)
		-> nalgebra::Vector3<physics::FloatT> {
		let line_dir = self.points[1] - self.points[0];
		let t = line_dir.dot(&(origin - self.points[0])) / line_dir.norm_squared();
		origin - (self.points[0] + t.clamp(0., 1.) * line_dir)
	}

	pub fn intersects(&self, other: &Triangle) -> nalgebra::Vector3<physics::FloatT> {
		let normal = other.normal();

		let sign1 = normal.dot(&(self.points[0] - other.points[0]));
		let sign2 = normal.dot(&(self.points[1] - other.points[0]));

		if sign1 * sign2 < 0. {
			let line_dir = self.points[1] - self.points[0];

			let denom = normal.dot(&line_dir);

			if denom == 0. {
				Default::default()
			} else {
				let d = sign1 / denom;
				if other.is_point_bounded(&(self.points[0] - line_dir * d)) {
					sign1.min(sign2) * normal
				} else {
					Default::default()
				}
			}
		} else {
			Default::default()
		}
	}
}
