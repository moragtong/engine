extern crate generator;
use generator::write_source_code;

fn main() {
    write_source_code(&mut std::path::Path::new(&String::from("vk.xml")), "../vk/src/");
}
